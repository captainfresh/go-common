package util

import (
	"context"
	"github.com/kataras/iris/v12"

	"bitbucket.org/captainfresh/go-common/auth"
)

func CreateContext(in iris.Context) context.Context {
	ctx := in.Request().Context()
	ctx = context.WithValue(ctx, auth.CtxValidatedData, in.Values().Get(auth.CtxValidatedData))
	return ctx
}

func GetValidatedData(ctx context.Context) *auth.ValidatedData {
	d, _ := ctx.Value(auth.CtxValidatedData).(*auth.ValidatedData)
	return d
}

func GetValidatedAccount(ctx context.Context) *auth.ValidatedAccount {
	vd := GetValidatedData(ctx)
	if vd != nil {
		return vd.Account
	}
	return &auth.ValidatedAccount{}
}
