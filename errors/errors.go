package errors

import (
	"runtime"
	"strconv"
)

const framesToSkip = 2

type BaseErr interface {
	// Error returns the top level error message
	Error() string
	// Trace returns the error messages separated by '\n' from top to bottom order of callers
	// Example:
	// ValidationErr: validation failed for namespace
	// NotFoundErr: namespace not found
	// Whoops
	Trace() string
	// Unpack returns the underlying error or cause of the current error received. If there are no
	// underlying error, Unpack will return nil.
	Unpack() BaseErr

	// StackedTrace returns a more detailed trace including file, function and line number
	// Example:
	// main.doSomethingElse   Whoops
	//        /go/go-common/main/main.go:44
	// main.doSomethingElse   NotFoundErr: namespace not found
	//        /go/go-common/main/main.go:44
	// main.doSomething       ValidationErr: validation failed for namespace
	//        /go/go-common/main/main.go:36
	StackedTrace() string
}

type frame struct {
	programCounter uintptr
	file           string
	line           int
}

type baseErr struct {
	msg   string
	err   BaseErr
	frame *frame
}

func (e *baseErr) Error() string { return e.msg }

func (e *baseErr) Trace() string {
	if e.err != nil {
		return e.msg + "\n" + e.err.Trace()
	}

	return e.msg
}

func (e *baseErr) Unpack() BaseErr {
	return e.err
}

func (e *baseErr) StackedTrace() (s string) {
	s = runtime.FuncForPC(e.frame.programCounter).Name() + "\t" + e.msg + "\n\t" + e.frame.file + ":" + strconv.Itoa(e.frame.line)
	if e.err != nil {
		s = e.err.StackedTrace() + "\n" + s
	}

	return
}

func New(m string) BaseErr {
	return &baseErr{msg: m, frame: getFrame(framesToSkip)}
}

func Append(err BaseErr, msg string) BaseErr {
	return &baseErr{err: err, msg: msg, frame: getFrame(framesToSkip)}
}

func getFrame(skip int) *frame {
	pc, file, line, _ := runtime.Caller(skip)

	return &frame{programCounter: pc, file: file, line: line}
}
