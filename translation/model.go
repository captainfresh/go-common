package translation

const (
	tagTranslate      = "translate"
	tagSeparator      = ","
	tagValueSeparator = "="
	methodTag         = "method"

	MethodExactMatch Method = "exact"

	defaultTranslateTag = true
	defaultTypeTag      = MethodExactMatch
)

type Tag struct {
	Translate bool
	Method    Method
}

type Method string

type ResolverInput struct {
	Text   string
	Method Method
}
