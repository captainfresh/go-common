package translation

import "context"

// Resolver is a simple text resolver interface.
// Open to implementation by the clients
type Resolver interface {
	Resolve(ctx context.Context, input *ResolverInput) (string, error)
}

// Translator is a simple interface that has only one method Translate.
// TODO: Add method WithFieldTranslator(fieldName string, translator Translator)
// TODO: to add field specific translator.
type Translator interface {
	// Translate takes an interface and translates its concrete values
	// returns error in case of failure
	Translate(ctx context.Context, v interface{}) error
}
