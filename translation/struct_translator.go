package translation

import (
	"context"
	"errors"
	"reflect"
)

// StructTranslator is takes a struct or a pointer to struct
// TODO: Currently only string fields are translated.
// TODO: Class can be extended to translate non-string fields by calling field specific translations.
type StructTranslator struct {
	stringResolver Resolver
}

func NewStructTranslator(stringResolver Resolver) *StructTranslator {
	return &StructTranslator{
		stringResolver: stringResolver,
	}
}

// Translate takes a pointer to struct as an input and translates its fields
// returns error if v is not a or pointer to struct type. if v is nil, returns nil
// TODO: Can add more fields to work on array, other structs as well.
func (translator *StructTranslator) Translate(ctx context.Context, v interface{}) error {
	if v == nil {
		return nil
	}

	typeOfV := reflect.TypeOf(v)
	valueOfV := reflect.ValueOf(v)
	if !isPointerToStruct(typeOfV) {
		return errors.New(reflect.ValueOf(v).String() + " is not a pointer to struct type")
	}

	typeOfV = typeOfV.Elem()
	valueOfV = valueOfV.Elem()
	for i := 0; i < typeOfV.NumField(); i++ {
		vField := typeOfV.Field(i)
		field := valueOfV.Field(i)
		tagString, ok := vField.Tag.Lookup(tagTranslate)
		if !ok {
			continue
		}

		tag, err := GetTagFromString(tagString)
		if err != nil {
			return err
		}

		if vField.Type.Kind() == reflect.Ptr {
			field = field.Elem()
		}

		switch field.Interface().(type) {
		case string:
			value, _ := field.Interface().(string)
			translatedString, err := translator.getStringTranslation(ctx, value, tag)
			if err != nil {
				return err
			}
			field.Set(reflect.ValueOf(translatedString))
		default:
			return errors.New("unsupported type for translation for: " + vField.Name)
		}
	}

	return nil
}

func (translator *StructTranslator) getStringTranslation(ctx context.Context, fieldValue string, tag *Tag) (string, error) {
	if tag.Translate {
		return translator.stringResolver.Resolve(ctx, &ResolverInput{
			Text:   fieldValue,
			Method: tag.Method,
		})
	}

	return fieldValue, nil
}

func isPointerToStruct(typeOfV reflect.Type) bool {
	return typeOfV.Kind() == reflect.Ptr && typeOfV.Elem().Kind() == reflect.Struct
}
