package translation

import (
	"reflect"
	"testing"
)

func TestGetTagFromString(t *testing.T) {
	type args struct {
		tagString string
	}
	tests := []struct {
		name    string
		args    args
		want    *Tag
		wantErr bool
	}{

		{name: "Test tag string true", args: args{tagString: "true"}, want: &Tag{Translate: true, Method: MethodExactMatch}},
		{name: "Test tag string false", args: args{tagString: "false"}, want: &Tag{Translate: false, Method: MethodExactMatch}},
		{name: "Test tag string true,type=exact", args: args{tagString: "true,type=exact"}, want: &Tag{Translate: true, Method: MethodExactMatch}},
		{name: "Test tag string type=exact", args: args{tagString: "type=exact"}, want: &Tag{Translate: true, Method: MethodExactMatch}},
		{name: "Test tag string exact,true", args: args{tagString: "exact,true"}, wantErr: true},
		{name: "Test tag string ,true", args: args{tagString: ",true"}, wantErr: true},
		{name: "Test tag with empty string", args: args{tagString: ""}, wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetTagFromString(tt.args.tagString)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetTagFromString() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetTagFromString() got = %v, want %v", got, tt.want)
			}
		})
	}
}
