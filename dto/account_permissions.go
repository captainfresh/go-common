package dto

import (
	"bitbucket.org/captainfresh/go-common/constant"
	"bitbucket.org/captainfresh/go-common/errors"
	"encoding/json"
)

type AccountPermissions struct {
	Permissions map[constant.PermissionType]bool
}

func ArrayToAccountPermissionDto(permission []constant.PermissionType) *AccountPermissions {
	var permissionsDict = make(map[constant.PermissionType]bool)
	for _, element := range permission {
		permissionsDict[element] = true
	}
	return &AccountPermissions{Permissions: permissionsDict}
}

func JsonToAccountPermissionDTO(permissions interface{}) (*AccountPermissions, errors.BaseErr) {
	var permissionsDict = make(map[constant.PermissionType]bool)

	data, err := json.Marshal(permissions)
	if err != nil {
		return nil, errors.New(err.Error())
	}
	err = json.Unmarshal(data, &permissionsDict)
	if err != nil {
		return nil, errors.New(err.Error())
	}
	return &AccountPermissions{Permissions: permissionsDict}, nil
}
