# How to use

# Initializing config
```go
package main

import (
	"bitbucket.org/captainfresh/go-common/v2/config"
	"os"
)

func main() {
	err := config.Init(os.Getenv("BASE_CONFIG_PATH"), os.Getenv("ENV"))
	if err != nil {
		panic(err)
	}
}
```