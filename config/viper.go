package config

import (
	"fmt"

	"github.com/spf13/viper"
)

var ViperConfig *viper.Viper

func Init(env string) {
	viper.SetConfigName(env)
	viper.AutomaticEnv()
	viper.SetConfigType("yaml")
	viper.AddConfigPath("./config")
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("fatal error config file: %w ", err))
	}
	ViperConfig = viper.GetViper()
}
