package auth

type ValidatedNamespace struct {
	ID   int64  `json:"id"`
	Code string `json:"code"`
	Name string `json:"name"`
}

type ValidatedUser struct {
	ID      string `json:"id"`
	Name    string `json:"name"`
	Email   string `json:"email"`
	Picture string `json:"picture"`
}

type ValidatedAccount struct {
	ID       int64  `json:"id"`
	Name     string `json:"name"`
	Metadata struct {
		FeatureConfig struct {
			Buying  bool `json:"buying"`
			Selling bool `json:"selling"`
		} `json:"featureConfig"`
	} `json:"metadata"`
}

type ValidatedData struct {
	User      *ValidatedUser      `json:"user"`
	Namespace *ValidatedNamespace `json:"namespace"`
	Account   *ValidatedAccount   `json:"account"`
}

func fillValidatedData(data *ValidatedData, user *ValidatedUser, namespace *ValidatedNamespace, account *ValidatedAccount) *ValidatedData {
	if data == nil {
		data = &ValidatedData{}
	}

	if user != nil {
		fillUser(data, user)
	}

	if namespace != nil {
		fillNamespace(data, namespace)
	}

	if account != nil {
		fillAccount(data, account)
	}

	return data
}

func fillUser(data *ValidatedData, user *ValidatedUser) {
	if data.User == nil {
		data.User = &ValidatedUser{}
	}

	if user.ID != "" {
		data.User.ID = user.ID
	}

	if user.Email != "" {
		data.User.Email = user.Email
	}

	if user.Name != "" {
		data.User.Name = user.Name
	}

	if user.Picture != "" {
		data.User.Picture = user.Picture
	}
}

func fillNamespace(data *ValidatedData, namespace *ValidatedNamespace) {
	if data.Namespace == nil {
		data.Namespace = &ValidatedNamespace{}
	}

	if namespace.ID != 0 {
		data.Namespace.ID = namespace.ID
	}

	if namespace.Name != "" {
		data.Namespace.Name = namespace.Name
	}
	if namespace.Code != "" {
		data.Namespace.Code = namespace.Code
	}
}

func fillAccount(data *ValidatedData, account *ValidatedAccount) {
	if data.Account == nil {
		data.Account = &ValidatedAccount{}
	}

	if account.ID != 0 {
		data.Account.ID = account.ID
	}

	if account.Name != "" {
		data.Account.Name = account.Name
	}

	data.Account.Metadata = account.Metadata
}
