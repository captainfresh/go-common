package auth

import (
	"context"
	"crypto/rsa"
	"github.com/lestrrat-go/jwx/jwk"
	"github.com/lestrrat-go/jwx/jwt"
	"time"
)

const verifierAutoRefreshInterval = time.Hour * 12

type Verifier interface {
	Verify(string) (map[string]interface{}, error)
}

type VerifierClient struct {
	publicKey    *rsa.PublicKey
	keySetURL    string
	parseOptions []jwt.ParseOption
	ar           *jwk.AutoRefresh
}

func NewVerifierClient(keySetURL string) *VerifierClient {
	ar := jwk.NewAutoRefresh(context.Background())
	ar.Configure(keySetURL, jwk.WithMinRefreshInterval(verifierAutoRefreshInterval))

	verifier := &VerifierClient{
		publicKey: nil,
		keySetURL: keySetURL,
		ar:        ar,
	}

	verifier.parseOptions = []jwt.ParseOption{
		jwt.WithValidate(true),
		jwt.WithKeySetProvider(jwt.KeySetProviderFunc(func(jwt.Token) (jwk.Set, error) {
			return verifier.ar.Fetch(context.Background(), verifier.keySetURL)
		})),
	}

	return verifier
}

func (auth *VerifierClient) Verify(signedString string) (map[string]interface{}, error) {
	token, err := jwt.Parse([]byte(signedString), auth.parseOptions...)
	if err != nil {
		return nil, err
	}
	claims := token.PrivateClaims()
	claims["sub"] = token.Subject()
	return claims, nil
}
