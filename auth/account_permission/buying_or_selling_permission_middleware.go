package account_permission

import (
	account_permission_constant "bitbucket.org/captainfresh/go-common/constant/account_permission"
	"bitbucket.org/captainfresh/go-common/errors"
	"bitbucket.org/captainfresh/go-common/policy/account_permission"
	"sync"
)

type BuyingOrSellingPermissionMiddleware struct {
	AccountPermissionMiddleware
}

var buyingOrSellingPermissionMiddlewareOnce sync.Once

var buyingOrSellingPermissionMiddlewareInstance *BuyingOrSellingPermissionMiddleware

func NewBuyingOrSellingPermissionMiddleware() (AccountPermissionMiddlewareInterface, errors.BaseErr) {
	policy, err := account_permission.GetAccountPermissionPolicy(account_permission_constant.BuyingOrSelling)
	if err != nil {
		return nil, errors.New(err.Error())
	}
	buyingOrSellingPermissionMiddlewareOnce.Do(func() {
		buyingOrSellingPermissionMiddlewareInstance = &BuyingOrSellingPermissionMiddleware{
			AccountPermissionMiddleware: AccountPermissionMiddleware{
				policy: policy,
			},
		}
	})
	return buyingOrSellingPermissionMiddlewareInstance, nil
}
