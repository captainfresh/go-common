package account_permission

import (
	"bitbucket.org/captainfresh/go-common/constant/account_permission"
	"bitbucket.org/captainfresh/go-common/errors"
)

func GetAccountPermissionMiddleware(permissionType account_permission.AccountPermissionRequired) (AccountPermissionMiddlewareInterface, errors.BaseErr) {
	switch permissionType {
	case account_permission.Buying:
		return NewBuyingPermissionMiddleware()
	case account_permission.Selling:
		return NewSellingPermissionMiddleware()
	case account_permission.BuyingAndSelling:
		return NewBuyingAndSellingPermissionMiddleware()
	case account_permission.BuyingOrSelling:
		return NewBuyingOrSellingPermissionMiddleware()
	}
	return nil, errors.New("Wrong permission type passed")
}
