package account_permission

import (
	account_permission_constant "bitbucket.org/captainfresh/go-common/constant/account_permission"
	"bitbucket.org/captainfresh/go-common/errors"
	"bitbucket.org/captainfresh/go-common/policy/account_permission"
	"sync"
)

type SellingPermissionMiddleware struct {
	AccountPermissionMiddleware
}

var sellingPermissionMiddlewareOnce sync.Once

var sellingPermissionMiddlewareInstance *SellingPermissionMiddleware

func NewSellingPermissionMiddleware() (AccountPermissionMiddlewareInterface, errors.BaseErr) {
	policy, err := account_permission.GetAccountPermissionPolicy(account_permission_constant.Selling)
	if err != nil {
		return nil, errors.New(err.Error())
	}
	sellingPermissionMiddlewareOnce.Do(func() {
		sellingPermissionMiddlewareInstance = &SellingPermissionMiddleware{
			AccountPermissionMiddleware: AccountPermissionMiddleware{
				policy: policy,
			},
		}
	})
	return sellingPermissionMiddlewareInstance, nil
}
