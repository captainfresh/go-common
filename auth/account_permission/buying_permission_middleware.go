package account_permission

import (
	account_permission_constant "bitbucket.org/captainfresh/go-common/constant/account_permission"
	"bitbucket.org/captainfresh/go-common/errors"
	"bitbucket.org/captainfresh/go-common/policy/account_permission"
	"sync"
)

type BuyingPermissionMiddleware struct {
	AccountPermissionMiddleware
}

var buyingPermissionMiddlewareOnce sync.Once

var buyingPermissionMiddlewareInstance *BuyingPermissionMiddleware

func NewBuyingPermissionMiddleware() (AccountPermissionMiddlewareInterface, errors.BaseErr) {
	policy, err := account_permission.GetAccountPermissionPolicy(account_permission_constant.Buying)
	if err != nil {
		return nil, errors.New(err.Error())
	}
	buyingPermissionMiddlewareOnce.Do(func() {
		buyingPermissionMiddlewareInstance = &BuyingPermissionMiddleware{
			AccountPermissionMiddleware: AccountPermissionMiddleware{
				policy: policy,
			},
		}
	})
	return buyingPermissionMiddlewareInstance, nil
}
