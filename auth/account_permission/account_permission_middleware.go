package account_permission

import (
	"bitbucket.org/captainfresh/go-common/dto"
	"bitbucket.org/captainfresh/go-common/policy/account_permission"
	"bitbucket.org/captainfresh/go-common/util"
	"github.com/kataras/iris/v12"
)

type AccountPermissionMiddleware struct {
	policy account_permission.AccountPermissionPolicyInterface
}

func (m *AccountPermissionMiddleware) Validate(ctx iris.Context) {
	permissionsAvailable := util.GetValidatedAccount(util.CreateContext(ctx)).Metadata.FeatureConfig

	permissionsAvailableDto, err := dto.JsonToAccountPermissionDTO(permissionsAvailable)

	if err != nil {
		_, _ = ctx.Problem(iris.NewProblem().Status(iris.StatusInternalServerError).Detail(err.Error()))
		ctx.StopExecution()
		return
	}

	if !m.policy.Validate(permissionsAvailableDto) {
		_, _ = ctx.Problem(iris.NewProblem().Status(iris.StatusForbidden).Detail("Account Permission Unauthorized"))
		ctx.StopExecution()
		return
	}

	ctx.Next()
}
