package account_permission

import (
	"github.com/kataras/iris/v12"
)

type AccountPermissionMiddlewareInterface interface {
	Validate(ctx iris.Context)
}
