package account_permission

import (
	account_permission_constant "bitbucket.org/captainfresh/go-common/constant/account_permission"
	"bitbucket.org/captainfresh/go-common/errors"
	"bitbucket.org/captainfresh/go-common/policy/account_permission"
	"sync"
)

type BuyingAndSellingPermissionMiddleware struct {
	AccountPermissionMiddleware
}

var buyingAndSellingPermissionMiddlewareOnce sync.Once

var buyingAndSellingPermissionMiddlewareInstance *BuyingAndSellingPermissionMiddleware

func NewBuyingAndSellingPermissionMiddleware() (AccountPermissionMiddlewareInterface, errors.BaseErr) {
	policy, err := account_permission.GetAccountPermissionPolicy(account_permission_constant.BuyingAndSelling)
	if err != nil {
		return nil, errors.New(err.Error())
	}
	buyingAndSellingPermissionMiddlewareOnce.Do(func() {
		buyingAndSellingPermissionMiddlewareInstance = &BuyingAndSellingPermissionMiddleware{
			AccountPermissionMiddleware: AccountPermissionMiddleware{
				policy: policy,
			},
		}
	})
	return buyingAndSellingPermissionMiddlewareInstance, nil
}
