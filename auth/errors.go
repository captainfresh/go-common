package auth

type (
	InvalidTokenErr  string
	InvalidKeyErr    string
	SigningErr       string
	AuthorizationErr string
)

func (e InvalidTokenErr) Error() string { return string(e) }

func (e InvalidKeyErr) Error() string { return string(e) }

func (e SigningErr) Error() string { return string(e) }

func (e AuthorizationErr) Error() string { return string(e) }
