package auth

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"path"
	"time"
)

const (
	validateNamespaceUserEndpoint    = "/auth/verify-ns-user"
	validateAccountUserEndpoint      = "/auth/verify-account-user"
	validateNamespaceAndUserEndpoint = "/auth/verify-namespace"
	validateNamespaceEndpoint        = "/auth/v2/verify-namespace"
	ctxUserID                        = "saas-user-id"
	ctxNamespace                     = "saas-namespace"
	ctxAccountID                     = "saas-account-id"
	authHeader                       = "Authorization"
)

type RequestAuthorizer interface {
	ValidateNamespaceUser(ctx context.Context) (*ValidatedData, error)
	ValidateAccountUser(ctx context.Context) (*ValidatedData, error)
	ValidateNamespaceAndUser(ctx context.Context) (*ValidatedData, error)
	ValidateNamespace(ctx context.Context) (*ValidatedData, error)
}

type RequestAuthorizerImpl struct {
	client http.Client
	apiURL string
}

func NewRequestAuthorizer(apiURL string) *RequestAuthorizerImpl {
	return &RequestAuthorizerImpl{client: http.Client{Timeout: time.Second * 5}, apiURL: apiURL}
}

func (auth *RequestAuthorizerImpl) ValidateNamespaceUser(ctx context.Context) (resp *ValidatedData, err error) {
	headers := make(map[string]string)
	keys := []string{ctxUserID, ctxNamespace, authHeader}
	for _, key := range keys {
		if headers[key], err = auth.getStrValueFromCtx(ctx, key); err != nil {
			return
		}
	}
	err = auth.makeRequest(validateNamespaceUserEndpoint, headers, &resp)

	return
}

func (auth *RequestAuthorizerImpl) ValidateAccountUser(ctx context.Context) (resp *ValidatedData, err error) {
	headers := make(map[string]string)
	keys := []string{ctxUserID, ctxNamespace, ctxAccountID, authHeader}
	for _, key := range keys {
		if headers[key], err = auth.getStrValueFromCtx(ctx, key); err != nil {
			return
		}
	}
	err = auth.makeRequest(validateAccountUserEndpoint, headers, &resp)

	return
}

func (auth *RequestAuthorizerImpl) ValidateNamespaceAndUser(ctx context.Context) (resp *ValidatedData, err error) {
	headers := make(map[string]string)
	keys := []string{ctxUserID, ctxNamespace, authHeader}
	for _, key := range keys {
		if headers[key], err = auth.getStrValueFromCtx(ctx, key); err != nil {
			return
		}
	}
	err = auth.makeRequest(validateNamespaceAndUserEndpoint, headers, &resp)

	return
}

func (auth *RequestAuthorizerImpl) ValidateNamespace(ctx context.Context) (resp *ValidatedData, err error) {
	headers := make(map[string]string)
	keys := []string{ctxNamespace, authHeader}
	for _, key := range keys {
		if headers[key], err = auth.getStrValueFromCtx(ctx, key); err != nil {
			return
		}
	}
	err = auth.makeRequest(validateNamespaceEndpoint, headers, &resp)

	return
}

func (*RequestAuthorizerImpl) getStrValueFromCtx(ctx context.Context, key string) (string, error) {
	value, ok := ctx.Value(key).(string)
	if !ok || value == "" {
		return "", AuthorizationErr(key + " not set")
	}

	return value, nil
}

func (auth *RequestAuthorizerImpl) makeRequest(urlEndpoint string, headers map[string]string, resp interface{}) (err error) {
	u, err := url.Parse(auth.apiURL)
	if err != nil {
		return
	}
	u.Path = path.Join(u.Path, urlEndpoint)
	request, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return
	}
	for k, v := range headers {
		request.Header.Set(k, v)
	}
	response, err := auth.client.Do(request)
	if err != nil {
		//TODO: Retry on failure
		return AuthorizationErr(err.Error())
	}

	defer func() { _ = response.Body.Close() }()

	if !Is2xxCode(response.StatusCode) {
		return AuthorizationErr(fmt.Sprintf("authorization failed with status code %d", response.StatusCode))
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return
	}

	return json.Unmarshal(body, &resp)
}

func Is2xxCode(statusCode int) bool {
	return statusCode >= 200 && statusCode < 300
}
