package auth

import (
	"context"
	"github.com/kataras/iris/v12"
	"strings"
)

const (
	CtxValidatedData = "validated-data"
	nameKey          = "name"
	emailKey         = "email"
	pictureKey       = "picture"
	userIdKey        = "sub"
)

type Middleware struct {
	auth RequestAuthorizer
}

func NewMiddleware(apiURL string) *Middleware {
	return &Middleware{auth: NewRequestAuthorizer(apiURL)}
}

func (m *Middleware) VerifyNamespaceUser(ctx iris.Context) {
	if resp, err := m.auth.ValidateNamespaceUser(getContext(ctx)); err != nil {
		_, _ = ctx.Problem(iris.NewProblem().Status(iris.StatusUnauthorized).Detail(err.Error()))
		ctx.StopExecution()

		return
	} else {
		m.updateValidatedData(ctx, resp)
		ctx.Next()
	}
}

func (m *Middleware) VerifyAccountUser(ctx iris.Context) {
	if resp, err := m.auth.ValidateAccountUser(getContext(ctx)); err != nil {
		_, _ = ctx.Problem(iris.NewProblem().Status(iris.StatusUnauthorized).Detail(err.Error()))
		ctx.StopExecution()

		return
	} else {
		m.updateValidatedData(ctx, resp)
		ctx.Next()
	}
}

func (m *Middleware) VerifyNamespaceAndUser(ctx iris.Context) {
	if resp, err := m.auth.ValidateNamespaceAndUser(getContext(ctx)); err != nil {
		_, _ = ctx.Problem(iris.NewProblem().Status(iris.StatusUnauthorized).Detail(err.Error()))
		ctx.StopExecution()

		return
	} else {
		m.updateValidatedData(ctx, resp)
		ctx.Next()
	}
}

func (m *Middleware) VerifyNamespace(ctx iris.Context) {
	if resp, err := m.auth.ValidateNamespace(getContext(ctx)); err != nil {
		_, _ = ctx.Problem(iris.NewProblem().Status(iris.StatusUnauthorized).Detail(err.Error()))
		ctx.StopExecution()
		return
	} else {
		m.updateValidatedData(ctx, resp)
		ctx.Next()
	}
}

func (m *Middleware) updateValidatedData(ctx iris.Context, data *ValidatedData) {
	validatedData, _ := ctx.Values().Get(CtxValidatedData).(*ValidatedData)
	validatedData = fillValidatedData(validatedData, data.User, data.Namespace, data.Account)
	ctx.Values().Set(CtxValidatedData, validatedData)
}

func getContext(in iris.Context) context.Context {
	ctx := in.Request().Context()
	ctx = context.WithValue(ctx, ctxNamespace, in.GetHeader(ctxNamespace))
	ctx = context.WithValue(ctx, ctxUserID, in.GetHeader(ctxUserID))
	ctx = context.WithValue(ctx, ctxAccountID, in.GetHeader(ctxAccountID))
	ctx = context.WithValue(ctx, authHeader, in.GetHeader(authHeader))

	return ctx
}

type TokenVerifierMiddleware struct {
	auth Verifier
}

func NewTokenVerifierMiddleware(apiURL string) *TokenVerifierMiddleware {
	return &TokenVerifierMiddleware{auth: NewVerifierClient(apiURL)}
}

func (m *TokenVerifierMiddleware) Verify(ctx iris.Context) {
	token := ctx.GetHeader(authHeader)
	tokenParts := strings.Split(token, " ")
	// tokenParts must contain a prefix such as Jwt/Bearer and then signed string
	if len(tokenParts) < 2 {
		_, _ = ctx.Problem(iris.NewProblem().Status(iris.StatusUnauthorized).Detail("invalid token"))

		return
	}

	claims, err := m.auth.Verify(tokenParts[1])
	if err != nil {
		_, _ = ctx.Problem(iris.NewProblem().Status(iris.StatusUnauthorized).Detail(err.Error()))

		return
	}
	m.updateValidatedData(ctx, claims)
	ctx.Next()
}

func (m *TokenVerifierMiddleware) updateValidatedData(ctx iris.Context, claims map[string]interface{}) {
	name, _ := claims[nameKey].(string)
	email, _ := claims[emailKey].(string)
	picture, _ := claims[pictureKey].(string)
	userId, _ := claims[userIdKey].(string)
	validatedData, _ := ctx.Values().Get(CtxValidatedData).(*ValidatedData)
	validatedData = fillValidatedData(validatedData, &ValidatedUser{ID: userId, Name: name, Email: email, Picture: picture}, nil, nil)
	ctx.Values().Set(CtxValidatedData, validatedData)
}
