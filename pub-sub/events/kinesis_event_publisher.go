package events

import (
	"context"
	"encoding/json"

	"bitbucket.org/captainfresh/go-common/pub-sub/kinesis"
	"go.uber.org/zap"
)

type kinesisPublisher struct {
	client kinesis.Producer
	logger *zap.Logger
}

func NewKinesisPublisher(client kinesis.Producer, logger *zap.Logger) Publisher {
	return &kinesisPublisher{client: client, logger: logger}
}

func (publisher *kinesisPublisher) Publish(ctx context.Context, stream string, event *Event) (err error) {
	if err = event.ValidateAndPrepare(); err != nil {
		publisher.logger.With(zap.Error(err)).Error("kinesisPublisher.Publish.error: invalid event")
		return
	}

	data, err := json.Marshal(event)
	if err != nil {
		publisher.logger.With(zap.Error(err)).Error("kinesisPublisher.Publish.error: event marshalling failed")

		return
	}
	response, err := publisher.client.PublishWithContext(ctx, &kinesis.Message{
		Data:         data,
		PartitionKey: event.PartitionKey,
		Stream:       stream,
	})

	if err != nil {
		publisher.logger.With(zap.Error(err)).Error("kinesisPublisher.Publish.error: publish failed")

		return
	}

	publisher.logger.With(
		zap.String("shard_id", *response.ShardId),
		zap.String("stream", event.Topic),
	).Debug("kinesisPublisher.Publish.success")

	return nil
}
