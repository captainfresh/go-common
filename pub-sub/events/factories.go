package events

import (
	"github.com/eapache/go-resiliency/retrier"
)

type HandlerFactory interface {
	GetHandler(topic string) (HandlerFunc, error)
}

type RetrierFactory interface {
	GetRetrier(event *Event) *retrier.Retrier
}
