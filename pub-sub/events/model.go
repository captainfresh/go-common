package events

import (
	"strings"
	"time"

	"github.com/go-playground/validator"
)

var eventValidator *validator.Validate

type EventMetadata struct {
	ServiceName string                 `json:"serviceName,omitempty"`
	NamespaceID string                 `json:"namespaceId,omitempty"`
	Custom      map[string]interface{} `json:"custom,omitempty"`
}

type Event struct {
	// PartitionKey is used to decide in which shard/partition
	// of the queue the event will be pushed to
	PartitionKey string `json:"partitionKey,omitempty" validate:"min=1"`

	// Type of event. Example, CREATE/UPDATE/DELETE
	Type string `json:"type,omitempty" validate:"min=1"`

	// Topic determines to which queue/stream the event will be pushed to
	Topic string `json:"topic,omitempty" validate:"min=1"`

	Timestamp time.Time      `json:"timestamp"`
	Data      interface{}    `json:"data,omitempty" validate:"required"`
	Metadata  *EventMetadata `json:"metadata,omitempty"`
}

func (event *Event) ValidateAndPrepare() error {
	event.PartitionKey = strings.TrimSpace(event.PartitionKey)
	event.Topic = strings.TrimSpace(event.Topic)
	event.Type = strings.TrimSpace(event.Type)
	event.Timestamp = time.Now().UTC()

	return getEventValidator().Struct(event)
}

func getEventValidator() *validator.Validate {
	if eventValidator == nil {
		eventValidator = validator.New()
	}

	return eventValidator
}
