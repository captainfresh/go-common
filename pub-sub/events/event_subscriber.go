package events

import "context"

type HandlerFunc func(context.Context, *Event) error

type Subscriber interface {
	ListenToEvents() error
}
