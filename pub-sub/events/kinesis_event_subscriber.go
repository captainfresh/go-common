package events

import (
	"bitbucket.org/captainfresh/go-common/pub-sub/kinesis"
	"context"
	"encoding/json"
	kinesis2 "github.com/aws/aws-sdk-go/service/kinesis"
	"github.com/eapache/go-resiliency/retrier"
	"go.uber.org/zap"
)

type kinesisEventSubscriber struct {
	consumer       kinesis.Consumer
	handlerFactory HandlerFactory
	retrierFactory RetrierFactory
	logger         *zap.Logger
}

func NewKinesisEventSubscriber(consumer kinesis.Consumer, logger *zap.Logger, handlerFactory HandlerFactory, retrierFactory RetrierFactory) Subscriber {
	return &kinesisEventSubscriber{consumer: consumer, logger: logger, retrierFactory: retrierFactory, handlerFactory: handlerFactory}
}

func (subscriber *kinesisEventSubscriber) ListenToEvents() error {
	err := subscriber.consumer.Consume(&kinesis.ProcessorDescription{HandlerFunc: subscriber.processKinesisRecords})
	if err != nil {
		subscriber.logger.With(zap.Error(err)).Error("kinesisEventSubscriber.ListenToEvents: unable to listen")

		return err
	}
	subscriber.logger.Info("kinesisEventSubscriber.ListenToEvents: listener started")

	return nil
}

func (subscriber *kinesisEventSubscriber) processKinesisRecords(ctx context.Context, records []*kinesis2.Record) {
	var event *Event
	for _, record := range records {
		err := json.Unmarshal(record.Data, &event)
		if err != nil {
			// TODO: Persist this
			subscriber.logger.With(zap.Error(err), zap.String("record", string(record.Data))).
				Error("kinesisEventSubscriber.ListenToEvents: unable to marshall")

			continue
		}

		subscriber.processEvent(ctx, event)
	}
}

func (subscriber *kinesisEventSubscriber) processEvent(ctx context.Context, event *Event) {
	handler, err := subscriber.handlerFactory.GetHandler(event.Topic)
	if err != nil {
		subscriber.logger.With(zap.Error(err)).Error("kinesisEventSubscriber.processEvent.error: error in getting event handler")

		return
	}

	eventRetrier := subscriber.getRetrier(event)
	if eventRetrier == nil {
		err = handler(ctx, event)
	} else {
		err = eventRetrier.Run(func() error {
			return handler(ctx, event)
		})
	}

	// TODO: handle failed cases
	if err != nil {
		subscriber.logger.With(zap.Error(err)).Error("failed to process message")
	}
}

func (subscriber *kinesisEventSubscriber) getRetrier(event *Event) *retrier.Retrier {
	if subscriber.retrierFactory == nil {
		return nil
	}

	return subscriber.retrierFactory.GetRetrier(event)
}
