package events

import (
	"github.com/eapache/go-resiliency/retrier"
	"time"
)

type RetrierPolicy int8

const (
	RetrierPolicyConstantBackOff RetrierPolicy = iota
	RetrierPolicyExponentialBackOff
)

type RetrierConfig struct {
	NumberOfRetries int
	RetrierPolicy   RetrierPolicy
	Duration        time.Duration
}

type TopicRetrierConfig struct {
	Topic string
	RetrierConfig
	DefaultConfig *RetrierConfig
}

type TopicRetrierParams struct {
	configs       []*TopicRetrierConfig
	DefaultConfig *RetrierConfig
}

type TopicAndTypeRetrierConfig struct {
	EventType string
	TopicRetrierConfig
}

type TopicAndTypeRetrierParams struct {
	configs       []*TopicAndTypeRetrierConfig
	DefaultConfig *RetrierConfig
}

type TopicRetrierFactory struct {
	retrierMap     map[string]*retrier.Retrier
	defaultRetrier *retrier.Retrier
}

func NewTopicRetrierFactory(configParams *TopicRetrierParams) *TopicRetrierFactory {
	factory := &TopicRetrierFactory{retrierMap: make(map[string]*retrier.Retrier)}
	for _, config := range configParams.configs {
		factory.retrierMap[config.Topic] = getRetrier(config.RetrierPolicy, config.NumberOfRetries, config.Duration)
	}

	if configParams.DefaultConfig != nil {
		factory.defaultRetrier = getRetrier(configParams.DefaultConfig.RetrierPolicy, configParams.DefaultConfig.NumberOfRetries, configParams.DefaultConfig.Duration)
	}

	return factory
}

func (factory *TopicRetrierFactory) GetRetrier(event *Event) *retrier.Retrier {
	if eventRetrier, ok := factory.retrierMap[event.Topic]; ok {
		return eventRetrier
	}

	return factory.defaultRetrier
}

type TopicAndTypeRetrierFactory struct {
	retrierMap     map[string]*retrier.Retrier
	defaultRetrier *retrier.Retrier
}

func NewTopicAndTypeRetrierFactory(configParams TopicAndTypeRetrierParams) *TopicAndTypeRetrierFactory {
	factory := &TopicAndTypeRetrierFactory{retrierMap: make(map[string]*retrier.Retrier)}
	for _, config := range configParams.configs {
		factory.retrierMap[factory.getMapKey(config.Topic, config.EventType)] = getRetrier(config.RetrierPolicy, config.NumberOfRetries, config.Duration)
	}

	if configParams.DefaultConfig != nil {
		factory.defaultRetrier = getRetrier(configParams.DefaultConfig.RetrierPolicy, configParams.DefaultConfig.NumberOfRetries, configParams.DefaultConfig.Duration)
	}

	return factory
}

func (factory *TopicAndTypeRetrierFactory) GetRetrier(event *Event) *retrier.Retrier {
	if eventRetrier, ok := factory.retrierMap[factory.getMapKey(event.Topic, event.Type)]; ok {
		return eventRetrier
	}

	return factory.defaultRetrier
}

func (factory *TopicAndTypeRetrierFactory) getMapKey(topic, EventType string) string {
	return topic + EventType
}

type SimpleRetrierFactory struct {
	config *RetrierConfig
}

func NewSimpleRetrierFactory(config *RetrierConfig) *SimpleRetrierFactory {
	return &SimpleRetrierFactory{config: config}
}

func (factory *SimpleRetrierFactory) GetRetrier(_ *Event) *retrier.Retrier {
	return getRetrier(factory.config.RetrierPolicy, factory.config.NumberOfRetries, factory.config.Duration)
}

func getRetrier(policy RetrierPolicy, numberOfRetries int, duration time.Duration) *retrier.Retrier {
	switch policy {
	case RetrierPolicyExponentialBackOff:
		return retrier.New(retrier.ExponentialBackoff(numberOfRetries, duration), nil)
	case RetrierPolicyConstantBackOff:
		return retrier.New(retrier.ExponentialBackoff(numberOfRetries, duration), nil)
	default:
		return retrier.New(retrier.ExponentialBackoff(numberOfRetries, duration), nil)
	}
}
