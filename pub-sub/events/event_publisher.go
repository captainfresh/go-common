package events

import (
	"context"
)

type Publisher interface {
	Publish(context context.Context, queue string, event *Event) error
}
