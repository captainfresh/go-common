package kinesis

import (
	"github.com/pkg/errors"
	"github.com/vmware/vmware-go-kcl/clientlibrary/config"
	"github.com/vmware/vmware-go-kcl/clientlibrary/worker"
	"go.uber.org/zap"
)

type IteratorType string

const (
	Latest      = "LATEST"
	AtTimestamp = "AT_TIMESTAMP"
	TrimHorizon = "TRIM_HORIZON"
)

type Consumer interface {
	Consume(description *ProcessorDescription) error
}

type consumer struct {
	Config *ConsumerConfig
	logger *zap.Logger
}

func NewConsumer(cfg *ConsumerConfig, logger *zap.Logger) Consumer {
	return &consumer{Config: cfg, logger: logger}
}

func (consumer *consumer) Consume(processorDescription *ProcessorDescription) error {
	if err := consumer.Config.Validate(); err != nil {
		return err
	}
	clientConfig := config.NewKinesisClientLibConfig(consumer.Config.AppName, consumer.Config.StreamName, consumer.Config.Region, consumer.Config.WorkerID)
	if len(consumer.Config.CheckpointDB) > 0 {
		clientConfig = clientConfig.WithDynamoDBEndpoint(consumer.Config.CheckpointDB)
	}

	if len(consumer.Config.Endpoint) > 0 {
		clientConfig = clientConfig.WithKinesisEndpoint(consumer.Config.Endpoint)
	}

	switch consumer.Config.IteratorConfig.InitialPositionPolicy {
	case Latest:
		clientConfig = clientConfig.WithInitialPositionInStream(config.LATEST)
	case AtTimestamp:
		clientConfig = clientConfig.WithInitialPositionInStream(config.AT_TIMESTAMP).
			WithTimestampAtInitialPositionInStream(consumer.Config.IteratorConfig.Timestamp)
	case TrimHorizon:
		clientConfig = clientConfig.WithInitialPositionInStream(config.TRIM_HORIZON)
	default:
		return errors.New("unknown initial position")
	}

	return worker.NewWorker(NewProcessorFactory(consumer.logger, processorDescription), clientConfig).Start()
}
