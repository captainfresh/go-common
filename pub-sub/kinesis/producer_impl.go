package kinesis

import (
	"context"

	"go.uber.org/zap"

	"github.com/aws/aws-sdk-go/service/kinesis/kinesisiface"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/kinesis"
	"github.com/pkg/errors"
)

type producer struct {
	client kinesisiface.KinesisAPI
	logger *zap.Logger
}

func NewProducer(config *AWSConfig, logger *zap.Logger) (Producer, error) {
	client, err := NewClient(config)
	if err != nil {
		return nil, errors.Wrap(err, "unable to create kinesis producer")
	}

	return &producer{
		client: client,
		logger: logger,
	}, nil
}

func (producer *producer) PublishWithContext(ctx context.Context, message *Message) (*kinesis.PutRecordOutput, error) {
	producer.logger.With(zap.Any("message", message)).Debug("ProducerRepo.PublishWithContext")
	input := &kinesis.PutRecordInput{
		Data:         message.Data,
		PartitionKey: aws.String(message.PartitionKey),
		StreamName:   aws.String(message.Stream),
	}

	res, err := producer.client.PutRecordWithContext(ctx, input)

	if err != nil {
		errMsg := "producer.PublishWithContext: error while publishing to kinesis"
		producer.logger.With(zap.Error(err)).Error(errMsg)

		return nil, err
	}

	return res, nil
}

func (producer *producer) PublishBatchWithContext(ctx context.Context, batch *Batch) (*kinesis.PutRecordsOutput, error) {
	producer.logger.With(zap.Any("message", batch.Messages)).Debug("ProducerRepo.PublishBatchWithContext")
	input := producer.prepareBatch(batch)
	res, err := producer.client.PutRecordsWithContext(ctx, input)
	if err != nil {
		errMsg := "producer.PublishBatchWithContext: error while publishing to kinesis"
		producer.logger.With(zap.Error(err)).Error(errMsg)

		return nil, err
	}

	return res, nil
}

func (producer *producer) prepareBatch(batch *Batch) *kinesis.PutRecordsInput {
	records := make([]*kinesis.PutRecordsRequestEntry, 0, len(batch.Messages))

	for _, m := range batch.Messages {
		record := &kinesis.PutRecordsRequestEntry{
			Data:         m.Data,
			PartitionKey: aws.String(m.PartitionKey),
		}

		records = append(records, record)
	}

	return &kinesis.PutRecordsInput{
		Records:    records,
		StreamName: aws.String(batch.Stream),
	}

}
