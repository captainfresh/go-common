package kinesis

import (
	"github.com/go-playground/validator"
	"github.com/pkg/errors"
	"time"
)

var configValidator *validator.Validate

type Message struct {
	Data         []byte
	PartitionKey string
	Stream       string
}

type Batch struct {
	Messages []*Message
	Stream   string
}

type AWSConfig struct {
	Endpoint string `json:"endpoint" mapstructure:"endpoint" validate:"nonzero"`
	Region   string `json:"region" mapstructure:"region"`
}

type IteratorConfig struct {
	InitialPositionPolicy IteratorType `json:"initialPositionPolicy,omitempty" validate:"required"`
	Timestamp             *time.Time   `json:"timestamp"`
}

// ConsumerConfig is a kinesis consumer configuration.
type ConsumerConfig struct {
	WorkerID       string          `json:"workerId,omitempty"`
	CheckpointDB   string          `json:"checkpointDB,omitempty"`
	Endpoint       string          `json:"endpoint,omitempty"`
	Region         string          `json:"region,omitempty" validate:"required"`
	IteratorConfig *IteratorConfig `json:"iteratorConfig,omitempty" validate:"required"`
	AppName        string          `json:"appName,omitempty" validate:"required"`
	StreamName     string          `json:"streamName,omitempty" validate:"required"`
}

func (config *ConsumerConfig) Validate() error {
	if err := getValidator().Struct(config); err != nil {
		return err
	}

	if config.IteratorConfig.InitialPositionPolicy == AtTimestamp && config.IteratorConfig.Timestamp == nil {
		return errors.New("timestamp is required with InitialPositionPolicy: AtTimestamp")
	}

	return nil
}

func getValidator() *validator.Validate {
	if configValidator == nil {
		configValidator = validator.New()
	}
	return configValidator
}
