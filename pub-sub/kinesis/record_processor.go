package kinesis

import (
	"context"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/kinesis"
	"github.com/vmware/vmware-go-kcl/clientlibrary/interfaces"
	"go.uber.org/zap"
)

type Handle func(ctx context.Context, records []*kinesis.Record)

type ProcessorDescription struct {
	HandlerFunc Handle
}

type processorFactory struct {
	logger               *zap.Logger
	processorDescription *ProcessorDescription
}

func NewProcessorFactory(logger *zap.Logger, description *ProcessorDescription) interfaces.IRecordProcessorFactory {
	return &processorFactory{logger: logger, processorDescription: description}
}

func (factory *processorFactory) CreateProcessor() interfaces.IRecordProcessor {
	// can create different type of processors in future
	return &recordProcessor{logger: factory.logger, handlerFunc: factory.processorDescription.HandlerFunc}
}

type recordProcessor struct {
	logger      *zap.Logger
	handlerFunc Handle
}

func (processor *recordProcessor) Initialize(input *interfaces.InitializationInput) {
	processor.logger.
		With(zap.String("shardID", input.ShardId), zap.Any("sequence", input.ExtendedSequenceNumber)).
		Info("Processor initialized...")

	return
}

func (processor *recordProcessor) ProcessRecords(input *interfaces.ProcessRecordsInput) {
	processor.logger.Debug("recordProcessor.ProcessRecords: ProcessRecords called")
	// don't process empty record
	if len(input.Records) == 0 {
		processor.logger.Debug("recordProcessor.ProcessRecords: no records to process")

		return
	}

	processor.handlerFunc(context.Background(), input.Records)
	// checkpoint it after processing this batch
	lastRecordSequenceNumber := input.Records[len(input.Records)-1].SequenceNumber
	processor.logger.With(
		zap.Any("lastSequenceNumber", lastRecordSequenceNumber),
		zap.Int64("millisBehindLatest", input.MillisBehindLatest)).Debug("recordProcessor.ProcessRecords: checkpoint progress at")
	if err := input.Checkpointer.Checkpoint(lastRecordSequenceNumber); err != nil {
		processor.logger.With(zap.Error(err)).Error("recordProcessor.ProcessRecords: Unable to checkpoint progress")
	}

	processor.logger.Debug("recordProcessor.ProcessRecords: records processed successfully!!!")
}

func (processor *recordProcessor) Shutdown(input *interfaces.ShutdownInput) {
	processor.logger.
		With(zap.String("reason", aws.StringValue(interfaces.ShutdownReasonMessage(input.ShutdownReason)))).
		Info("Processor Shutdown...")

	// When the value of {@link ShutdownInput#getShutdownReason()} is
	// {@link ShutdownReason#TERMINATE} it is required that you
	// checkpoint. Failure to do so will result in an IllegalArgumentException, and the KCL no longer making progress.
	if input.ShutdownReason == interfaces.TERMINATE {
		err := input.Checkpointer.Checkpoint(nil)
		processor.logger.With(zap.Error(err)).Error("recordProcessor.Shutdown: Unable to checkpoint progress")
	}
}
