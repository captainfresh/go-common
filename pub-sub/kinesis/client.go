// client.go

package kinesis

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/kinesis"
	"gopkg.in/validator.v2"
)

func NewClient(config *AWSConfig) (*kinesis.Kinesis, error) {
	err := validator.Validate(config.Endpoint)
	if err != nil {
		return nil, err
	}
	c := aws.NewConfig()
	if config.Endpoint != "" {
		c.WithEndpoint(config.Endpoint)
	}
	if config.Region != "" {
		c.WithRegion(config.Region)
	}

	awsSession, err := session.NewSession(c)
	if err != nil {
		return nil, err
	}

	return kinesis.New(awsSession), nil
}
