package kinesis

import (
	"context"

	"github.com/aws/aws-sdk-go/service/kinesis"
)

type Producer interface {
	PublishWithContext(ctx context.Context, message *Message) (*kinesis.PutRecordOutput, error)
	PublishBatchWithContext(ctx context.Context, batch *Batch) (*kinesis.PutRecordsOutput, error)
}
