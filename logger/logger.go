package logger

import (
	"os"
	"sync"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var (
	// Log is a global logger
	log *zap.Logger
	// to init logger only once
	onceInit sync.Once
)

//const (
//	ANSIC       = "Mon Jan _2 15:04:05 2006"
//	UnixDate    = "Mon Jan _2 15:04:05 MST 2006"
//	RubyDate    = "Mon Jan 02 15:04:05 -0700 2006"
//	RFC822      = "02 Jan 06 15:04 MST"
//	RFC822Z     = "02 Jan 06 15:04 -0700" // RFC822 with numeric zone
//	RFC850      = "Monday, 02-Jan-06 15:04:05 MST"
//	RFC1123     = "Mon, 02 Jan 2006 15:04:05 MST"
//	RFC1123Z    = "Mon, 02 Jan 2006 15:04:05 -0700" // RFC1123 with numeric zone
//	RFC3339     = "2006-01-02T15:04:05Z07:00"
//	RFC3339Nano = "2006-01-02T15:04:05.999999999Z07:00"
//	Kitchen     = "3:04PM"
//	// Handy time stamps.
//	Stamp      = "Jan _2 15:04:05"
//	StampMilli = "Jan _2 15:04:05.000"
//	StampMicro = "Jan _2 15:04:05.000000"
//	StampNano  = "Jan _2 15:04:05.000000000"
//)

//type Level int8
//const (
//	// DebugLevel logs are typically voluminous, and are usually disabled in
//	// production.
//	DebugLevel Level = iota - 1
//	// InfoLevel is the default logging priority.
//	InfoLevel
//	// WarnLevel logs are more important than Info, but don't need individual
//	// human review.
//	WarnLevel
//	// ErrorLevel logs are high-priority. If an application is running smoothly,
//	// it shouldn't generate any error-level logs.
//	ErrorLevel
//	// DPanicLevel logs are particularly important errors. In development the
//	// logger panics after writing the message.
//	DPanicLevel
//	// PanicLevel logs a message, then panics.
//	PanicLevel
//	// FatalLevel logs a message, then calls os.Exit(1).
//	FatalLevel
//)

func customTimeEncoder(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
	loc, _ := time.LoadLocation("Asia/Kolkata")
	enc.AppendString(t.In(loc).Format(time.RFC3339Nano))
}

// Init initializes log by input parameters
// lvl - global log level: Debug(-1), Info(0), Warn(1), Error(2), DPanic(3), Panic(4), Fatal(5)
// timeFormat - custom time format for logger of empty string to use default
func Init(lvl int) (*zap.Logger, error) {
	var err error
	onceInit.Do(func() {
		globalLevel := zapcore.Level(lvl)
		highPriority := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
			return lvl >= zapcore.ErrorLevel
		})
		lowPriority := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
			return lvl >= globalLevel && lvl < zapcore.ErrorLevel
		})
		consoleInfos := zapcore.Lock(os.Stdout)
		consoleErrors := zapcore.Lock(os.Stderr)
		encoderConfig := zap.NewProductionEncoderConfig()
		//customTimeFormat = "2006-01-02T15:04:05.999999999Z07:00"
		encoderConfig.EncodeTime = customTimeEncoder
		consoleEncoder := zapcore.NewJSONEncoder(encoderConfig)
		core := zapcore.NewTee(
			zapcore.NewCore(consoleEncoder, consoleErrors, highPriority),
			zapcore.NewCore(consoleEncoder, consoleInfos, lowPriority),
		)
		log = zap.New(core)
		zap.RedirectStdLog(log)
	})
	return log, err
}

func GetLoggerInstance() *zap.Logger {
	return log
}
