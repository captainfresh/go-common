package rest

import (
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/context"
)

type Handler struct {
	Method      string
	Path        string
	HandlerFunc func(ctx iris.Context)
}

type Router struct {
	Path        string
	Middlewares []context.Handler
	Handlers    []Handler
}

func RegisterRoutersToParty(parentParty iris.Party, routers []Router) {
	for _, router := range routers {
		party := parentParty.Party(router.Path, wrapMiddlewares(router.Middlewares)...)
		for _, handler := range router.Handlers {
			party.Handle(handler.Method, handler.Path, handler.HandlerFunc)
		}
	}
}

func RegisterRouters(app *iris.Application, routers []Router) {
	for _, router := range routers {
		party := app.Party(router.Path, wrapMiddlewares(router.Middlewares)...)
		for _, handler := range router.Handlers {
			party.Handle(handler.Method, handler.Path, handler.HandlerFunc)
		}
	}
}

func wrapMiddlewares(middlewares []context.Handler) (wrappedMiddlewares []context.Handler) {
	for _, middleware := range middlewares {
		wrappedMiddlewares = append(wrappedMiddlewares, wrapMiddleware(middleware))
	}
	return
}

func wrapMiddleware(middleware func(ctx iris.Context)) context.Handler {
	return func(ctx iris.Context) {
		middleware(ctx)
		ctx.Next()
	}
}
