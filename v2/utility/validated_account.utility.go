package utility

import (
	"bitbucket.org/captainfresh/go-common/v2/model"
	"context"
)

func GetValidatedAccount(ctx context.Context) (*model.ValidatedAccount, error) {
	validatedData, err := GetValidatedData(ctx)
	if err != nil {
		return nil, err
	}

	return validatedData.GetAccount()
}

func GetValidatedAccountID(ctx context.Context) (*int64, error) {
	validatedAccount, err := GetValidatedAccount(ctx)
	if err != nil {
		return nil, err
	}

	return validatedAccount.GetAccountID()
}

func GetValidatedAccountName(ctx context.Context) (*string, error) {
	validatedAccount, err := GetValidatedAccount(ctx)
	if err != nil {
		return nil, err
	}

	return validatedAccount.GetAccountName()
}

func GetValidatedAccountMetadata(ctx context.Context) (*model.ValidatedAccountMetadata, error) {
	validatedAccount, err := GetValidatedAccount(ctx)
	if err != nil {
		return nil, err
	}

	return validatedAccount.GetAccountMetadata()
}

func GetValidatedAccountFeature(ctx context.Context) (*model.AccountFeature, error) {
	validatedAccountMetadata, err := GetValidatedAccountMetadata(ctx)
	if err != nil {
		return nil, err
	}

	return validatedAccountMetadata.GetAccountFeature()
}

func IfValidatedAccountHasBuyingFeature(ctx context.Context) (bool, error) {
	feature, err := GetValidatedAccountFeature(ctx)
	if err != nil {
		return false, err
	}

	return feature.Buying, nil
}

func IfValidatedAccountHasSellingFeature(ctx context.Context) (bool, error) {
	feature, err := GetValidatedAccountFeature(ctx)
	if err != nil {
		return false, err
	}

	return feature.Selling, nil
}

func IfValidatedAccountHasTradingFeature(ctx context.Context) (bool, error) {
	feature, err := GetValidatedAccountFeature(ctx)
	if err != nil {
		return false, err
	}

	return feature.Selling && feature.Buying, nil
}

func GetValidatedAccountDefaults(ctx context.Context) (*model.AccountDefaults, error) {
	validatedAccountMetadata, err := GetValidatedAccountMetadata(ctx)
	if err != nil {
		return nil, err
	}

	return validatedAccountMetadata.GetAccountDefaults()
}

func GetValidatedAccountDefaultShippingAddress(ctx context.Context) (*int64, error) {
	validatedAccountDefaults, err := GetValidatedAccountDefaults(ctx)
	if err != nil {
		return nil, err
	}

	return validatedAccountDefaults.GetAccountDefaultShippingAddress()
}

func GetValidatedAccountDefaultBillingAddress(ctx context.Context) (*int64, error) {
	validatedAccountDefaults, err := GetValidatedAccountDefaults(ctx)
	if err != nil {
		return nil, err
	}

	return validatedAccountDefaults.GetAccountDefaultBillingAddress()
}
