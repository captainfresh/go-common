package utility

import (
	"bitbucket.org/captainfresh/go-common/v2/model"
	"context"
)

func GetValidatedNamespace(ctx context.Context) (*model.ValidatedNamespace, error) {
	validatedData, err := GetValidatedData(ctx)
	if err != nil {
		return nil, err
	}

	return validatedData.GetNamespace()
}

func GetValidatedNamespaceID(ctx context.Context) (*int64, error) {
	validatedNamespace, err := GetValidatedNamespace(ctx)
	if err != nil {
		return nil, err
	}

	return validatedNamespace.GetNamespaceID()
}

func GetValidatedNamespaceCode(ctx context.Context) (*string, error) {
	validatedNamespace, err := GetValidatedNamespace(ctx)
	if err != nil {
		return nil, err
	}

	return validatedNamespace.GetNamespaceCode()
}

func GetValidatedNamespaceName(ctx context.Context) (*string, error) {
	validatedNamespace, err := GetValidatedNamespace(ctx)
	if err != nil {
		return nil, err
	}

	return &validatedNamespace.Name, nil
}
