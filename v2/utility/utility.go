package utility

import (
	"bitbucket.org/captainfresh/go-common/v2/constant"
	"bitbucket.org/captainfresh/go-common/v2/errors"
	"context"
	"fmt"
	"log"
	"reflect"
	"strconv"
	"strings"
)

// GetUser returns user-id of string type or errors if user-id is not present in the context
func GetUser(ctx context.Context) (string, error) {
	return get(ctx, constant.CtxUserID)
}

// GetAccountID returns account-id of int64 type or errors. Account-id is fetched from context
// and converted into int64 type. If conversion is not successful, errors is returned by the function
func GetAccountID(ctx context.Context) (accountID int64, err error) {
	accountIDStr, err := GetAccountIDString(ctx)
	if err != nil {
		return
	}
	return parseInt(constant.CtxAccountID, accountIDStr)
}

// GetAccountIDString returns account-id of string type or errors, if account-id is not present in the context
func GetAccountIDString(ctx context.Context) (string, error) {
	return get(ctx, constant.CtxAccountID)
}

// GetOrganisationID returns organisation-id of int64 type or errors. Organisation-id is fetched from context
// and converted into int64 type. If conversion is not successful, errors is returned by the function
func GetOrganisationID(ctx context.Context) (orgID int64, err error) {
	orgIDStr, err := GetOrganisationIDString(ctx)
	if err != nil {
		return
	}
	return parseInt(constant.CtxOrgID, orgIDStr)
}

// GetOrganisationIDString returns organisation-id of string type or errors. It is fetched from context
func GetOrganisationIDString(ctx context.Context) (string, error) {
	return get(ctx, constant.CtxOrgID)
}

// GetNamespace returns namespace of string type. It is fetched from context
func GetNamespace(ctx context.Context) (string, error) {
	return get(ctx, constant.CtxNamespace)
}

// GetNamespaceID returns namespace-id of int64 type. It is fetched from context
func GetNamespaceID(ctx context.Context) (nsID int64, err error) {
	nsID, ok := ctx.Value(constant.CtxNamespaceID).(int64)
	if !ok {
		msg := fmt.Sprintf("missing %s in context", constant.CtxNamespaceID)
		log.Println(msg)
		return 0, errors.NewRequiredHeaderMissingError(msg)
	}
	return
}

// GetNamespaceIDStr returns namespace-id of string type. Namespace-id.(int64) is fetched from context and
// converted to string.
func GetNamespaceIDStr(ctx context.Context) (ns string, err error) {
	nsID, err := GetNamespaceID(ctx)
	if err != nil {
		return
	}
	return strconv.FormatInt(nsID, base10), nil
}

// GetAuthorizationHeader returns authorization header of string type. It is fetched from context
func GetAuthorizationHeader(ctx context.Context) (string, error) {
	return get(ctx, constant.CtxAuthHeader)
}

// GetClientId returns constant.HeaderKeyClientID header of string type. It is fetched from context
func GetClientId(ctx context.Context) (string, error) {
	return get(ctx, constant.HeaderKeyClientID)
}

// GetClientSecret returns constant.HeaderKeyClientSecret header of string type. It is fetched from context
func GetClientSecret(ctx context.Context) (string, error) {
	return get(ctx, constant.HeaderKeyClientSecret)
}

func GetAuthorizationCookie(ctx context.Context) (string, error) {
	return get(ctx, constant.HeaderCookie)
}

func GetSAccessToken(ctx context.Context) (string, error) {
	return get(ctx, constant.SAccessToken)
}

func GetSIdRefreshToken(ctx context.Context) (string, error) {
	return get(ctx, constant.SIdRefreshToken)
}

//--------------------------------------------HELPER FUNCTIONS-----------------------------------------------------//

func get(ctx context.Context, key string) (value string, err error) {
	value, ok := ctx.Value(key).(string)
	if !ok || value == "" {
		msg := fmt.Sprintf("missing %s in context", key)
		//log.Println(msg)
		return "", errors.NewRequiredHeaderMissingError(msg)
	}
	return
}

func parseInt(key, keyStr string) (int64, error) {
	id, err := strconv.ParseInt(keyStr, base10, bitSize64)
	if err != nil {
		msg := fmt.Sprintf("failed to parse %s", key)
		log.Printf("%s, errors: %s", msg, err.Error())
		return id, errors.BadRequestError(err, msg)
	}
	return id, nil
}

// GetCtxValueWithDefaultString retrieves the provided key's value from context and returns default value if not exists.
func GetCtxValueWithDefaultString(ctx context.Context, key string, defaultValue string) string {
	value := ctx.Value(key)
	if value == nil {
		log.Printf("key %s not found in context. Fallback to defaultValue: %s", key, defaultValue)

		return defaultValue
	}

	if reflect.TypeOf(value).Kind() != reflect.String {
		log.Printf("key %s not found in context. Fallback to defaultValue: %s", key, defaultValue)

		return defaultValue
	}

	return value.(string)
}

// GetLocaleFromCtx retrieves the value of Accept-Language header set in context with default value of "en" if not found
func GetLocaleFromCtx(ctx context.Context) string {
	return GetCtxValueWithDefaultString(ctx, constant.CtxHeaderAcceptLanguage, "en")
}

func CookieFromString(name string, cookie string) string {
	cookies := strings.Split(cookie, ";")
	for _, cookieData := range cookies {
		splitCookie := strings.Split(cookieData, "=")
		if splitCookie[0] == name {
			return splitCookie[1]
		}
	}

	return ""
}

func BuildAuthNAuthCookie(sAccessToken string, sIdRefreshToken string) (cookie string) {
	if sAccessToken != "" {
		cookie = fmt.Sprintf("%s=%s;", constant.SAccessToken, sAccessToken)
	}
	if sIdRefreshToken != "" {
		cookie = fmt.Sprintf("%s%s=%s;", cookie, constant.SIdRefreshToken, sIdRefreshToken)
	}

	return cookie
}

func GetRequiredHeaders(cookie string, clientId, clientSecret string) map[string]string {
	res := make(map[string]string)
	res[constant.HeaderCookie] = cookie
	res[constant.HeaderKeyContentType] = constant.HeaderValContentTypeJson
	res[constant.HeaderKeyClientID] = clientId
	res[constant.HeaderKeyClientSecret] = clientSecret
	return res
}
