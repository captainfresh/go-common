package utility

import (
	"bitbucket.org/captainfresh/go-common/v2/constant"
	"bitbucket.org/captainfresh/go-common/v2/errors"
	"bitbucket.org/captainfresh/go-common/v2/model"
	"context"
	"fmt"
)

func GetValidatedData(ctx context.Context) (*model.ValidatedData, error) {
	validatedData, ok := ctx.Value(constant.CtxValidatedData).(*model.ValidatedData)
	if !ok || validatedData == nil {
		return nil, errors.NewRecordNotFoundError(fmt.Sprintf("missing %s in context", constant.CtxValidatedData))
	}

	return validatedData, nil
}
