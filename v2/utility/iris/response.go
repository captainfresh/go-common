package iris

import (
	"bitbucket.org/captainfresh/go-common/v2/model"
	"github.com/kataras/iris/v12"
)

func ErrorResponse(ctx iris.Context, statusCode int, err error) {
	//log.Println(err)
	statusResponse := &model.Status{Code: statusCode, Message: err.Error(), Detail: "error"}
	errorResponse := &model.BaseResponse{Error: statusResponse}

	ctx.StatusCode(statusCode)
	ctx.JSON(errorResponse)
}
