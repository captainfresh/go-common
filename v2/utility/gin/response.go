package gin

import (
	"bitbucket.org/captainfresh/go-common/v2/model"
	"github.com/gin-gonic/gin"
)

func ErrorResponse(ctx *gin.Context, statusCode int, err error) {
	statusResponse := &model.Status{Code: statusCode, Message: err.Error(), Detail: "error"}
	errorResponse := &model.BaseResponse{Error: statusResponse}
	ctx.JSON(statusCode, errorResponse)
}
