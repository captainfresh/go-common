package utility

import (
	"bitbucket.org/captainfresh/go-common/v2/model"
	"context"
)

func GetValidatedUser(ctx context.Context) (*model.ValidatedUser, error) {
	validatedData, err := GetValidatedData(ctx)
	if err != nil {
		return nil, err
	}

	return validatedData.GetUser()
}

func GetValidatedUserID(ctx context.Context) (*string, error) {
	validatedUser, err := GetValidatedUser(ctx)
	if err != nil {
		return nil, err
	}

	return validatedUser.GetUserID()
}
