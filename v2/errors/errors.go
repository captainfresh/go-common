package errors

// InternalServerErr Internal Server Error
type InternalServerErr struct {
	BaseErr
}

// NewInternalServerError constructs and returns custom internal server error, given the error message
func NewInternalServerError(msg string) BaseErr {
	return &InternalServerErr{NewWithErrorCode("InternalServerErr: "+msg, InternalServerErrorCode)}
}

// InternalServerError appends and returns custom internal server error, given the error message
func InternalServerError(err error, msg string) (baseError BaseErr) {
	switch err.(type) {
	case BaseErr:
		baseError = AppendWithErrorCode(err.(BaseErr), msg, InternalServerErrorCode)
	default:
		baseError = NewWithErrorCode("InternalServerErr: "+msg+", "+err.Error(), InternalServerErrorCode)
	}
	return &InternalServerErr{baseError}
}

// RecordNotFoundErr Record Not Found Error
type RecordNotFoundErr struct {
	BaseErr
}

// NewRecordNotFoundError constructs and returns custom record not found error, given the error message
func NewRecordNotFoundError(msg string) BaseErr {
	return &RecordNotFoundErr{NewWithErrorCode("RecordNotFoundErr: "+msg, RecordNotFoundErrorCode)}
}

// RecordNotFoundError appends and returns custom record not found error, given the error message
func RecordNotFoundError(err error, msg string) (baseError BaseErr) {
	switch err.(type) {
	case BaseErr:
		baseError = AppendWithErrorCode(err.(BaseErr), msg, RecordNotFoundErrorCode)
	default:
		baseError = NewWithErrorCode("RecordNotFoundErr: "+msg+", "+err.Error(), RecordNotFoundErrorCode)
	}
	return &RecordNotFoundErr{baseError}
}

// BadRequestErr Bad Request Error
type BadRequestErr struct {
	BaseErr
}

// NewBadRequestError constructs and returns custom bad request error, given the error message
func NewBadRequestError(msg string) BaseErr {
	return &BadRequestErr{NewWithErrorCode("BadRequestErr: "+msg, BadRequestErrorCode)}
}

// BadRequestError appends and returns custom bad request error, given the error message
func BadRequestError(err error, msg string) (baseError BaseErr) {
	switch err.(type) {
	case BaseErr:
		baseError = AppendWithErrorCode(err.(BaseErr), msg, BadRequestErrorCode)
	default:
		baseError = NewWithErrorCode("BadRequestErr: "+msg+", "+err.Error(), BadRequestErrorCode)
	}
	return &BadRequestErr{baseError}
}

// ForbiddenErr Forbidden Error
type ForbiddenErr struct {
	BaseErr
}

// NewForbiddenError constructs and returns custom forbidden error, given the error message
func NewForbiddenError(msg string) BaseErr {
	return &ForbiddenErr{NewWithErrorCode("ForbiddenErr: "+msg, ForbiddenErrorCode)}
}

// ForbiddenError appends and returns custom forbidden error, given the error message
func ForbiddenError(err error, msg string) (baseError BaseErr) {
	switch err.(type) {
	case BaseErr:
		baseError = AppendWithErrorCode(err.(BaseErr), msg, ForbiddenErrorCode)
	default:
		baseError = NewWithErrorCode("ForbiddenErr: "+msg+", "+err.Error(), ForbiddenErrorCode)
	}
	return &ForbiddenErr{baseError}
}

// UnauthenticatedErr Unauthenticated Error
type UnauthenticatedErr struct {
	BaseErr
}

// NewUnauthenticatedError constructs and returns custom unauthenticated error, given the error message
func NewUnauthenticatedError(msg string) BaseErr {
	return &UnauthenticatedErr{NewWithErrorCode("UnauthenticatedErr: "+msg, UnauthenticatedErrorCode)}
}

// UnauthenticatedError appends and returns custom unauthenticated error, given the error message
func UnauthenticatedError(err error, msg string) (baseError BaseErr) {
	switch err.(type) {
	case BaseErr:
		baseError = AppendWithErrorCode(err.(BaseErr), msg, UnauthenticatedErrorCode)
	default:
		baseError = NewWithErrorCode("UnauthenticatedErr: "+msg+", "+err.Error(), UnauthenticatedErrorCode)
	}
	return &UnauthenticatedErr{baseError}
}

// RequiredHeaderMissingErr Required Header Missing Error
type RequiredHeaderMissingErr struct {
	BaseErr
}

// NewRequiredHeaderMissingError constructs and returns custom required header missing error, given the error message
func NewRequiredHeaderMissingError(msg string) BaseErr {
	return &RequiredHeaderMissingErr{NewWithErrorCode("RequiredHeaderMissingErr: "+msg, RequiredHeaderMissingErrorCode)}
}

// RequiredHeaderMissingError appends and returns custom required header missing error, given the error message
func RequiredHeaderMissingError(err error, msg string) (baseError BaseErr) {
	switch err.(type) {
	case BaseErr:
		baseError = AppendWithErrorCode(err.(BaseErr), msg, RequiredHeaderMissingErrorCode)
	default:
		baseError = NewWithErrorCode("RequiredHeaderMissingErr: "+msg+", "+err.Error(), RequiredHeaderMissingErrorCode)
	}
	return &RequiredHeaderMissingErr{baseError}
}
