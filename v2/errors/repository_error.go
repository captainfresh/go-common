package errors

import (
	"github.com/jackc/pgconn"
	"github.com/jackc/pgerrcode"
	"gorm.io/gorm"
)

func RepositoryError(err error) error {
	if err == nil {
		return nil
	}
	if err == gorm.ErrRecordNotFound {
		return NewRecordNotFoundError(err.Error())
	}
	pgErr, ok := err.(*pgconn.PgError)
	if !ok {
		return err
	}
	switch pgErr.Code {
	case pgerrcode.ForeignKeyViolation:
		return NewBadRequestError(err.Error())
	case pgerrcode.UniqueViolation:
		return NewBadRequestError(err.Error())
	default:
		return err
	}
}
