package errors

type ErrorCode string

const (
	InternalServerErrorCode        = "INTERNAL_SERVER_ERROR"
	RecordNotFoundErrorCode        = "RECORD_NOT_FOUND"
	BadRequestErrorCode            = "BAD_REQUEST"
	ForbiddenErrorCode             = "FORBIDDEN"
	UnauthenticatedErrorCode       = "UNAUTHENTICATED"
	RequiredHeaderMissingErrorCode = "REQUIRED_HEADER_MISSING"
)
