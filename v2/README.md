# Building GraphQL servers in GoLang using go-common

#### Step 1: Setup Project
Required - bitbucket repository initialised as Go module.
Create a tools.go file and add gqlgen as a tool dependency for your module.
```
//go:build tools
// +build tools

package tools

import (
_ "github.com/99designs/gqlgen"
)
```

To automatically add the dependency to your go.mod run `go mod tidy`


#### Step 2: Building the server
Create the project skeleton
>go run github.com/99designs/gqlgen init

This will create suggested package layout. Modify these paths in gqlgen.yml as below:
```
# Where are all the schema files located? globs are supported eg  src/**/*.graphqls
schema:
- graph/schema/*.graphqls

# Where should the generated server code go?
exec:
filename: graph/generated/generated.go
package: generated

# Enable federation
federation:
filename: graph/generated/federation.go
package: generated
version: 2

# Where should any generated models go?
model:
filename: graph/generated/model.go
package: generated

# Where should the resolver implementations go?
resolver:
layout: follow-schema
dir: graph/resolver
package: resolver
```

At the top of resolver.go, between package and import, add the following line:
>//go:generate go run github.com/99designs/gqlgen generate

This magic comment tells go generate what command to run when we want to regenerate our code. To run go generate recursively over your entire project, use this command:
`go generate ./...`

#### Step 3: Setup Server
1. Initialize viper config in main method. `config.Init(env)`
2. Execute serve/migrate command from main method. `cmd.Execute(initFunc)`
3. Create initFunc :
1. Create database connection. `database.InitPostgres(&database.PostgresConnectionConfig{DSN: "database.dsn"})`
2. Initialize graphql server
```
gqlConfig := &generated.Config{}
gqlConfig.resolver := &resolver.Resolver{....}
gqlConfig.Directives.VerifyNamespace = graphql.VerifyNamespace()
.....
gqlSchema := generated.NewExecutableSchema(gqlConfig)
gqlServer := handler.NewDefaultServer(gqlSchema)
srv := graphql.EnrichContext(gqlServer)
http.Handle("/", srv)
http.ListenAndServe(":"+port, nil)
```

#### Step 4: Add queries, mutations decorated with verifier directives.
Example:
````
directive @verifyNamespace on FIELD_DEFINITION
GetMyProfile: Profile!  @verifyNamespace
````

### Commands
Regenerate files `go run github.com/99designs/gqlgen generate`

Build `go build server.go`

Run server `go run server.go serve`

Run up migration scripts `go run server.go migrate up`

Run down migration scripts `go run server.go migrate down`


### Code Structure
```
|-- Dockerfile
|-- README.md
|-- bitbucket-pipelines.yml
|-- config
|   `-- default.yaml
|-- crood.yaml
|-- entrypoint.sh
|-- go.mod
|-- go.sum
|-- gqlgen.yml
|-- graph
|   |-- generated
|   |   |-- federation.go
|   |   |-- generated.go
|   |   `-- model.go
|   |-- mapper
|   |   `-- entity_name.go
|   |-- resolver
|   |   |-- entity_name.resolvers.go
|   |   `-- resolver.go
|   |-- schema
|   |   |-- directive.graphqls
|   |   `-- entity_name.graphqls
|   `-- utility
|       `-- utility.go
|-- internal
|   |-- constant
|   |   `-- constant.go
|   |-- dto
|   |   |-- entity_name.go
|   |-- mapper
|   |   `-- entity_name.go
|   |-- model
|   |   `-- entity_name.go
|   |-- repository
|   |   |-- entity_name.go
|   |   `-- entity_name_impl.go
|   |-- server.go
|   |-- service
|   |   |-- entity_name.go
|   |   `-- entity_name_impl.go
|   |-- utility
|   |   `-- utility.go
|-- migrations
|   |-- 000001_entity_name.down.sql
|   `-- 000002_entity_name.up.sql
|-- server.go
`-- tools.go

```