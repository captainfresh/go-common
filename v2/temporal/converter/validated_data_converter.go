package converter

import (
	"bitbucket.org/captainfresh/go-common/v2/model"
	"encoding/json"
	"fmt"
	"github.com/mitchellh/mapstructure"
	"go.temporal.io/sdk/converter"

	commonpb "go.temporal.io/api/common/v1"
)

// ValidatedDataConverter converts to/from model.ValidatedData.
type ValidatedDataConverter struct {
}

// NewValidatedDataConverter creates new instance of ValidatedDataConverter.
func NewValidatedDataConverter() *ValidatedDataConverter {
	return &ValidatedDataConverter{}
}

// ToPayload converts single value to payload.
func (c *ValidatedDataConverter) ToPayload(value interface{}) (*commonpb.Payload, error) {
	data, err := json.Marshal(value)
	if err != nil {
		return nil, fmt.Errorf("%w value: %v, error: %v", converter.ErrUnableToEncode, value, err)
	}
	return &commonpb.Payload{
		Metadata: map[string][]byte{
			converter.MetadataEncoding: []byte(c.Encoding()),
		},
		Data: data,
	}, nil
}

// ToPayloads converts a list of values.
func (c *ValidatedDataConverter) ToPayloads(values ...interface{}) (*commonpb.Payloads, error) {
	result := &commonpb.Payloads{}

	for i, value := range values {
		payload, err := c.ToPayload(value)
		if err != nil {
			return nil, fmt.Errorf("values[%d]: %w", i, err)
		}

		result.Payloads = append(result.Payloads, payload)
	}
	return result, nil
}

// FromPayload converts single value from payload.
func (c *ValidatedDataConverter) FromPayload(payload *commonpb.Payload, valuePtr interface{}) error {
	var validatedData *model.ValidatedData
	err := json.Unmarshal(payload.GetData(), &validatedData)
	if err != nil {
		return fmt.Errorf("%w payload: %s, error: %v", converter.ErrUnableToDecode, c.ToString(payload), err)
	}

	err = mapstructure.Decode(validatedData, valuePtr)
	if err != nil {
		return fmt.Errorf("%w validatedData:%v, error: %v", converter.ErrUnableToDecode, validatedData, err)
	}
	return nil
}

// FromPayloads converts to a list of values of different types.
// Useful for deserializing arguments of function invocations.
func (c *ValidatedDataConverter) FromPayloads(payloads *commonpb.Payloads, valuePtrs ...interface{}) error {
	for i, payload := range payloads.GetPayloads() {
		err := c.FromPayload(payload, valuePtrs[i])

		if err != nil {
			return fmt.Errorf("args[%d]: %w", i, err)
		}
	}

	return nil
}

// ToString converts payload object into human-readable string.
func (c *ValidatedDataConverter) ToString(payload *commonpb.Payload) string {
	return string(payload.GetData())
}

// ToStrings converts payloads object into human-readable strings.
func (c *ValidatedDataConverter) ToStrings(payloads *commonpb.Payloads) []string {
	var result []string
	for _, payload := range payloads.GetPayloads() {
		result = append(result, c.ToString(payload))
	}

	return result
}

// Encoding returns MetadataEncodingJSON.
func (c *ValidatedDataConverter) Encoding() string {
	return MetadataEncodingValidatedData
}

// MetadataEncodingValidatedData is "saas/validated-data"
const MetadataEncodingValidatedData = "saas/validated-data"
