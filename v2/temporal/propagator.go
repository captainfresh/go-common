package temporal

import (
	"context"
	"go.temporal.io/sdk/converter"
	"go.temporal.io/sdk/workflow"
	"log"
)

// propagator implements the custom context propagator
type propagator struct {
	// keyValueDataConverter Map of keys to be propagated in context
	// to converter.DataConverter to be used for serializing/deserializing the value
	keyValueDataConverter map[string]converter.DataConverter
}

// NewContextPropagator returns an instance of workflow.ContextPropagator initialised with keyValueDataConverter
func NewContextPropagator(keyValueDataConverter map[string]converter.DataConverter) workflow.ContextPropagator {
	return &propagator{
		keyValueDataConverter: keyValueDataConverter,
	}
}

// Inject injects values from context into headers for propagation
func (p *propagator) Inject(ctx context.Context, writer workflow.HeaderWriter) error {
	for key, valueConverter := range p.keyValueDataConverter {
		value := ctx.Value(key)
		if value == nil {
			log.Printf("error: key %s not found in context, skipping\n", key)
			continue
		}
		payload, err := valueConverter.ToPayload(value)
		if err != nil {
			return err
		}
		writer.Set(key, payload)
	}
	return nil
}

// Extract extracts values from headers and puts them into context
func (p *propagator) Extract(ctx context.Context, reader workflow.HeaderReader) (context.Context, error) {
	for key, valueConverter := range p.keyValueDataConverter {
		if value, ok := reader.Get(key); ok {
			var values interface{}
			if err := valueConverter.FromPayload(value, &values); err != nil {
				return nil, err
			}
			ctx = context.WithValue(ctx, key, values)
		} else {
			log.Printf("error: key %s not found in context, skipping\n", key)
		}
	}
	return ctx, nil
}

// InjectFromWorkflow injects values from context into headers for propagation
func (p *propagator) InjectFromWorkflow(ctx workflow.Context, writer workflow.HeaderWriter) error {
	for key, valueConverter := range p.keyValueDataConverter {
		value := ctx.Value(key)
		if value == nil {
			log.Printf("error: key %s not found in context, skipping\n", key)
			continue
		}
		payload, err := valueConverter.ToPayload(value)
		if err != nil {
			return err
		}
		writer.Set(key, payload)
	}
	return nil
}

// ExtractToWorkflow extracts values from headers and puts them into workflow context
func (p *propagator) ExtractToWorkflow(ctx workflow.Context, reader workflow.HeaderReader) (workflow.Context, error) {
	for key, valueConverter := range p.keyValueDataConverter {
		if value, ok := reader.Get(key); ok {
			var values interface{}
			if err := valueConverter.FromPayload(value, &values); err != nil {
				return nil, err
			}
			ctx = workflow.WithValue(ctx, key, values)
		} else {
			log.Printf("error: key %s not found in context, skipping\n", key)
		}
	}
	return ctx, nil
}
