package kinesis

import (
	"bitbucket.org/captainfresh/go-common/v2/errors"
	"context"
	"github.com/aws/aws-sdk-go-v2/service/kinesis/types"
	"github.com/vmware/vmware-go-kcl-v2/clientlibrary/interfaces"
)

type Handle func(ctx context.Context, records []types.Record) errors.BaseErr

type ProcessorDescription struct {
	HandlerFunc Handle
}

type processorFactory struct {
	processorDescription *ProcessorDescription
}

func NewProcessorFactory(description *ProcessorDescription) interfaces.IRecordProcessorFactory {
	return &processorFactory{processorDescription: description}
}

func (factory *processorFactory) CreateProcessor() interfaces.IRecordProcessor {
	// can create different type of processors in future
	return &recordProcessor{handlerFunc: factory.processorDescription.HandlerFunc}
}

type recordProcessor struct {
	handlerFunc Handle
}

func (processor *recordProcessor) Initialize(*interfaces.InitializationInput) {
	return
}

func (processor *recordProcessor) ProcessRecords(input *interfaces.ProcessRecordsInput) {
	//processor.logger.Debug("recordProcessor.ProcessRecords: ProcessRecords called")
	// don't process empty record
	if input == nil || len(input.Records) == 0 {
		//processor.logger.Debug("recordProcessor.ProcessRecords: no records to process")

		return
	}

	if err := processor.handlerFunc(context.Background(), input.Records); err != nil {
		// TODO: Handle error
		return
	}
	// checkpoint it after processing this batch
	lastRecordSequenceNumber := input.Records[len(input.Records)-1].SequenceNumber
	//processor.logger.With(
	//	zap.Any("lastSequenceNumber", lastRecordSequenceNumber),
	//	zap.Int64("millisBehindLatest", input.MillisBehindLatest)).Debug("recordProcessor.ProcessRecords: checkpoint progress at")
	if err := input.Checkpointer.Checkpoint(lastRecordSequenceNumber); err != nil {
		//processor.logger.With(zap.Error(err)).Error("recordProcessor.ProcessRecords: Unable to checkpoint progress")
	}

	//processor.logger.Debug("recordProcessor.ProcessRecords: records processed successfully!!!")
}

func (processor *recordProcessor) Shutdown(input *interfaces.ShutdownInput) {
	//processor.logger.
	//	With(zap.String("reason", aws.StringValue(interfaces.ShutdownReasonMessage(input.ShutdownReason)))).
	//	Info("Processor Shutdown...")

	// When the value of {@link ShutdownInput#getShutdownReason()} is
	// {@link ShutdownReason#TERMINATE} it is required that you
	// checkpoint. Failure to do so will result in an IllegalArgumentException, and the KCL no longer making progress.
	if input.ShutdownReason == interfaces.TERMINATE {
		//err := input.Checkpointer.Checkpoint(nil)
		//processor.logger.With(zap.Error(err)).Error("recordProcessor.Shutdown: Unable to checkpoint progress")
	}
}
