package kinesis

import (
	"context"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/go-playground/validator/v10"
	"time"
)

var validate *validator.Validate

func init() {
	validate = validator.New()
}

// IteratorType defines the behaviour of queue iterator
// in case of a new APP or when the iterator checkpoint is missing.
type IteratorType string

var emptyOption = func(options *config.LoadOptions) error {
	return nil
}

const (
	// IteratorTypeLatest will start the iterator from the last record found in queue.
	IteratorTypeLatest = "LATEST"
	// IteratorTypeAtTimestamp will start the iterator from the timestamp provided in config.
	IteratorTypeAtTimestamp = "AT_TIMESTAMP"
	// IteratorTypeTrimHorizon will start the iterator from the oldest available data record.
	IteratorTypeTrimHorizon = "TRIM_HORIZON"
)

// Config contains the minimum required config to maintain a connection.
type Config struct {
	Endpoint *string
	Region   *string
}

// IteratorConfig contains the config for the queue iterator.
// In case of IteratorTypeAtTimestamp, Timestamp is a mandatory field.
type IteratorConfig struct {
	InitialPositionPolicy IteratorType `json:"initialPositionPolicy,omitempty" validate:"required"`
	Timestamp             *time.Time   `json:"timestamp"`
}

// ConsumerConfig is a kinesis consumer configuration.
type ConsumerConfig struct {
	*Config
	// WorkerID used to distinguish different workers/processes of a Kinesis application
	WorkerID       string          `json:"workerId,omitempty"`
	CheckpointDB   string          `json:"checkpointDB,omitempty"`
	IteratorConfig *IteratorConfig `json:"iteratorConfig,omitempty" validate:"required"`
	AppName        string          `json:"appName,omitempty" validate:"required,gt=1"`
	StreamName     string          `json:"streamName,omitempty" validate:"required,gt=1"`
}

func (config *ConsumerConfig) ValidateAndPrepare() error {
	if e := validate.Struct(config); e != nil {
		return e
	}
	if config.IteratorConfig.InitialPositionPolicy == IteratorTypeAtTimestamp && config.IteratorConfig.Timestamp.IsZero() {
		return fmt.Errorf("timestamp is a required parameter in case of IteratorTypeAtTimestamp")
	}
	return nil
}

func (cfg *Config) getEndpointLoadOption() config.LoadOptionsFunc {
	if cfg.Endpoint != nil {
		return config.WithEndpointResolverWithOptions(
			aws.EndpointResolverWithOptionsFunc(func(serviceName, region string, options ...interface{}) (aws.Endpoint, error) {
				return aws.Endpoint{URL: *cfg.Endpoint}, nil
			}))
	}

	return emptyOption
}

func (cfg *Config) getRegionLoadOption() config.LoadOptionsFunc {
	if cfg.Region != nil {
		return config.WithRegion(*cfg.Region)
	}
	return emptyOption
}

func getAWSConfigs(cfg *Config) (aws.Config, error) {
	return config.LoadDefaultConfig(context.TODO(), cfg.getEndpointLoadOption(), cfg.getRegionLoadOption())
}

func GetConfigFromMap(config map[string]interface{}) *Config {
	if config == nil {
		return nil
	}
	cfg := &Config{}
	if endpointRaw, ok := config["endpoint"]; ok {
		endpointStr, _ := endpointRaw.(string)
		cfg.Endpoint = &endpointStr
	}

	if regionRaw, ok := config["region"]; ok {
		regionStr, _ := regionRaw.(string)
		cfg.Region = &regionStr
	}
	return cfg
}

func GetSourceConfigFromMap(config map[string]interface{}) *ConsumerConfig {
	consumerConfig := &ConsumerConfig{}
	if config == nil {
		return nil
	}

	consumerConfig.Config = GetConfigFromMap(config)
	if workerId, ok := config["worker_id"]; ok {
		consumerConfig.WorkerID, _ = workerId.(string)
	}

	if checkpointDB, ok := config["checkpoint_db"]; ok {
		consumerConfig.CheckpointDB, _ = checkpointDB.(string)
	}

	if iteratorType, ok := config["iterator_type"]; ok {
		consumerConfig.IteratorConfig = &IteratorConfig{}
		policy, _ := iteratorType.(string)
		consumerConfig.IteratorConfig.InitialPositionPolicy = IteratorType(policy)
	}

	if timestamp, ok := config["iterator_timestamp"]; ok {
		if consumerConfig.IteratorConfig == nil {
			consumerConfig.IteratorConfig = &IteratorConfig{}
		}
		consumerConfig.IteratorConfig.Timestamp = timestamp.(*time.Time)
	}

	if appName, ok := config["app_name"]; ok {
		consumerConfig.AppName, _ = appName.(string)
	}

	if streamName, ok := config["stream_name"]; ok {
		consumerConfig.StreamName, _ = streamName.(string)
	}

	return consumerConfig
}
