package kinesis

import (
	"bitbucket.org/captainfresh/go-common/v2/errors"
	"bitbucket.org/captainfresh/go-common/v2/pubsub/event"
	"context"
	"encoding/json"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/kinesis"
)

// Publisher uses kinesis to publish records to stream. It is an Implementation of pub_sub.Publisher
type Publisher struct {
	event.FailureHandler
	client *kinesis.Client
}

func NewPublisher(cfg *Config, failureHandler event.FailureHandler) (*Publisher, errors.BaseErr) {
	awsConfigs, err := getAWSConfigs(cfg)
	if err != nil {
		return nil, errors.New(err.Error())
	}

	return &Publisher{FailureHandler: failureHandler, client: kinesis.NewFromConfig(awsConfigs)}, nil
}

func (publisher *Publisher) Publish(ctx context.Context, queueName string, event *event.Event) errors.BaseErr {
	dataBytes, err := json.Marshal(event)
	if err != nil {
		return errors.New(err.Error())
	}
	_, err = publisher.client.PutRecord(ctx, &kinesis.PutRecordInput{
		Data:         dataBytes,
		PartitionKey: aws.String(event.PartitionKey),
		StreamName:   aws.String(queueName),
	})

	if err != nil {
		return publisher.OnFailure(ctx, event, errors.New(err.Error()))
	}

	return nil
}
