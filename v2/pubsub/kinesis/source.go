package kinesis

import (
	"bitbucket.org/captainfresh/go-common/v2/errors"
	"bitbucket.org/captainfresh/go-common/v2/pubsub/event"
	"bitbucket.org/captainfresh/go-common/v2/pubsub/subscriber"
	"context"
	"encoding/json"
	"github.com/aws/aws-sdk-go-v2/service/kinesis/types"
	"github.com/vmware/vmware-go-kcl-v2/clientlibrary/config"
	"github.com/vmware/vmware-go-kcl-v2/clientlibrary/worker"
)

const (
	batchChanBuffSize = 200
)

// Source will source events from a single kinesis stream.
type Source struct {
	config       *ConsumerConfig
	batchChan    chan *event.Batch
	batchAckChan chan event.AcknowledgementType
	acknowledger subscriber.BatchAcknowledger
}

func NewSource(queueConfig *ConsumerConfig, acknowledger subscriber.BatchAcknowledger) *Source {
	return &Source{
		config:       queueConfig,
		batchChan:    make(chan *event.Batch, batchChanBuffSize),
		batchAckChan: make(chan event.AcknowledgementType),
		acknowledger: acknowledger,
	}
}

// SourceEvents is a blocking method that will create a worker and start sourcing events.
// Internally, it is using the  github.com/vmware/vmware-go-kcl-v2 library to connect to kinesis streams.
// The library also takes care of maintaining check-pointers in dynamoDB as well as connecting to all the shards
func (source *Source) SourceEvents() errors.BaseErr {
	if source.config == nil {
		return errors.NewBadRequestError("source config not provided")
	}

	if err := source.config.ValidateAndPrepare(); err != nil {
		return errors.NewBadRequestError(err.Error())
	}
	clientConfig := config.NewKinesisClientLibConfig(source.config.AppName, source.config.StreamName, *source.config.Region, source.config.WorkerID)

	// if CheckpointDB is not set, clientConfig will use the default endpoint for db.
	if len(source.config.CheckpointDB) > 0 {
		clientConfig = clientConfig.WithDynamoDBEndpoint(source.config.CheckpointDB)
	}

	// if Endpoint is not set, clientConfig will use the default endpoint.
	if source.config.Endpoint != nil {
		clientConfig = clientConfig.WithKinesisEndpoint(*source.config.Endpoint)
	}

	switch source.config.IteratorConfig.InitialPositionPolicy {
	case IteratorTypeLatest:
		clientConfig = clientConfig.WithInitialPositionInStream(config.LATEST)
	case IteratorTypeAtTimestamp:
		clientConfig = clientConfig.WithInitialPositionInStream(config.AT_TIMESTAMP).
			WithTimestampAtInitialPositionInStream(source.config.IteratorConfig.Timestamp)
	case IteratorTypeTrimHorizon:
		clientConfig = clientConfig.WithInitialPositionInStream(config.TRIM_HORIZON)
	default:
		return errors.New("unknown initial position")
	}

	err := worker.NewWorker(NewProcessorFactory(&ProcessorDescription{HandlerFunc: source.getHandlerFunc()}), clientConfig).Start()
	if err != nil {
		return errors.New(err.Error())
	}

	return nil
}

func (source *Source) getHandlerFunc() Handle {
	return func(ctx context.Context, records []types.Record) errors.BaseErr {
		var outputs = make([]*event.Record, 0, len(records))
		var output *event.Record
		for _, record := range records {
			output = &event.Record{Raw: record.Data}
			err := json.Unmarshal(record.Data, &output.Event)
			if err != nil {
				output.Err = errors.NewBadRequestError(err.Error())
			}
			outputs = append(outputs, output)
		}
		batch := &event.Batch{Records: outputs, AckChan: source.batchAckChan}
		source.batchChan <- batch
		return source.acknowledger.Acknowledge(batch)
	}
}

func (source *Source) BatchChannel() <-chan *event.Batch {
	return source.batchChan
}
