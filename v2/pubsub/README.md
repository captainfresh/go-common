## Publishing events from publisher interface

# Create publisher builder by passing the config map to build function

```go

publisher, err := pubsub.NewPublisherBuilder().Build(
	pubsub.PublisherTypeKinesis,
	map[string]interface{} {
		"region": "ap-south-1",
	})

if err != nil {
	return err
}

publisher.Publish(context.Background(), "fg-temp-1", &event.Event{
	PartitionKey: "1",
	Topic: "ORDER",
	Type: "UPDATE",
	Data: <data>
})
```


## Subscribing to events from a source

```go
config := map[string]interface{} {
	"region": "ap-south-1",
	"checkpoint_db": "https://localhost:8000",
	"iterator_config": map[string]interface{} {
		"initial_position_policy": "TRIM_HORIZON"
	}
}

subscriberBuilder := pubsub.NewSubscriberBuilder()
_, err := subscriberBuilder.AddSource(pubsub.SourceTypeKinesis, config)
if err != nil {
	return err
}

subscriber := subscriberBuilder.AddHandler(Handler, event.WithNamespaces([]{"cf"}), event.WithTopic([]string{"ACCOUNT"})).
	          AddHandler(Handler2, event.WithTopic([]string{"ORDER"}), event.WithType([]string{"UPDATE"})).
              Build()
return subscriber.Start()
```