package builder

import (
	"bitbucket.org/captainfresh/go-common/v2/errors"
	"bitbucket.org/captainfresh/go-common/v2/pubsub/event"
	"bitbucket.org/captainfresh/go-common/v2/pubsub/kinesis"
	"bitbucket.org/captainfresh/go-common/v2/pubsub/publisher"
)

type PublisherType = int8

const (
	PublisherTypeKinesis PublisherType = iota + 1
)

type PublisherBuilder struct {
	failureHandler event.FailureHandler
}

func NewPublisherBuilder() *PublisherBuilder {
	return &PublisherBuilder{}
}

func (builder *PublisherBuilder) Build(publisherType PublisherType, config map[string]interface{}) (publisher.Publisher, errors.BaseErr) {
	builder.prepare()
	switch publisherType {
	case PublisherTypeKinesis:
		cfg := kinesis.GetConfigFromMap(config)
		return kinesis.NewPublisher(cfg, builder.failureHandler)
	default:
		return nil, errors.NewBadRequestError("unknown publisher type")
	}
}

func (builder *PublisherBuilder) prepare() {
	if builder.failureHandler == nil {
		builder.failureHandler = event.NewFailureHandlerReturnErr()
	}
}

func (builder *PublisherBuilder) WithFailureHandler(failureHandler event.FailureHandler) *PublisherBuilder {
	builder.failureHandler = failureHandler
	return builder
}
