package builder

import (
	"bitbucket.org/captainfresh/go-common/v2/errors"
	"bitbucket.org/captainfresh/go-common/v2/pubsub/event"
	"bitbucket.org/captainfresh/go-common/v2/pubsub/kinesis"
	"bitbucket.org/captainfresh/go-common/v2/pubsub/subscriber"
	"time"
)

type SourceType uint8

const (
	SourceTypeKinesis SourceType = iota + 1

	defaultTimeOutForAcknowledgement = time.Minute
	defaultTimeOutAction             = subscriber.AcknowledgementActionReturnErr
)

type sourceTypeAndConfigMap struct {
	sourceType SourceType
	configMap  map[string]interface{}
}

// SubscriberBuilder is the builder class for subscribers.
// It encapsulates all the necessary bits and pieces required to build a subscriber.
type SubscriberBuilder struct {
	sourceConfigs   []*sourceTypeAndConfigMap
	sources         []subscriber.Source
	acknowledger    subscriber.BatchAcknowledger
	handlerRegistry event.HandlerRegistry
	dispatcher      subscriber.Dispatcher
}

func NewSubscriberBuilder() *SubscriberBuilder {
	return &SubscriberBuilder{
		handlerRegistry: event.NewDefaultHandlerRegistry(),
	}
}

func (builder *SubscriberBuilder) AddSource(sourceType SourceType, config map[string]interface{}) *SubscriberBuilder {
	builder.sourceConfigs = append(builder.sourceConfigs, &sourceTypeAndConfigMap{
		sourceType: sourceType,
		configMap:  config,
	})
	return builder
}

func (builder *SubscriberBuilder) Build() (subscriber.Subscriber, errors.BaseErr) {
	if err := builder.prepare(); err != nil {
		return nil, err
	}
	return subscriber.NewDefaultSubscriber(builder.dispatcher, builder.sources), nil
}

func (builder *SubscriberBuilder) AddHandler(handler event.Handler, criteria ...event.MatchingCriterion) *SubscriberBuilder {
	builder.handlerRegistry.RegisterHandler(handler, criteria...)
	return builder
}

func (builder *SubscriberBuilder) WithAcknowledger(acknowledger subscriber.BatchAcknowledger) *SubscriberBuilder {
	builder.acknowledger = acknowledger
	return builder
}

func (builder *SubscriberBuilder) prepare() errors.BaseErr {
	if builder.acknowledger == nil {
		builder.acknowledger = subscriber.NewTimeoutAcknowledger(defaultTimeOutForAcknowledgement, defaultTimeOutAction)
	}

	builder.dispatcher = subscriber.NewSequentialDispatcher(builder.handlerRegistry)
	for _, sourceConfig := range builder.sourceConfigs {
		switch sourceConfig.sourceType {
		case SourceTypeKinesis:
			kinesisConfig := kinesis.GetSourceConfigFromMap(sourceConfig.configMap)
			builder.sources = append(builder.sources, kinesis.NewSource(kinesisConfig, builder.acknowledger))
		default:
			return errors.NewBadRequestError("unknown source type")
		}
	}
	return nil
}
