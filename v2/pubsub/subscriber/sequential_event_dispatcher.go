package subscriber

import (
	"bitbucket.org/captainfresh/go-common/v2/pubsub/event"
	"context"
	"sync"
)

// SequentialDispatcher implements the Dispatcher Interface to call handler methods synchronously.
// It requires a HandlerRegistry to get the List of Handlers who are interested into the coming event.
type SequentialDispatcher struct {
	handlerFactory event.HandlerRegistry
}

func NewSequentialDispatcher(handlerFactory event.HandlerRegistry) *SequentialDispatcher {
	return &SequentialDispatcher{handlerFactory: handlerFactory}
}

// Subscribe will let the Dispatcher register itself to th event updates provided by the list of sources.
func (s *SequentialDispatcher) Subscribe(sources []Source) {
	for i := range sources {
		go func(idx int) {
			s.listenToEvents(sources[idx])
		}(i)
	}
}

func (s *SequentialDispatcher) listenToEvents(source Source) {
	for batch := range source.BatchChannel() {
		s.passBatchToHandlers(batch)
	}
}

func (s *SequentialDispatcher) passBatchToHandlers(batch *event.Batch) {
	for _, record := range batch.Records {
		if record.Err != nil {
			// TODO: handle err
			continue
		}
		s.passEventsToHandlers(record.Event)
		// TODO: have a policy and based on policy send acknowledgementType.
		batch.AckChan <- event.AcknowledgmentTypeAllProcessed
	}
}

func (s *SequentialDispatcher) passEventsToHandlers(receivedEvent *event.Event) {
	wg := sync.WaitGroup{}
	handlers := s.handlerFactory.GetHandlers(receivedEvent)
	for i := range handlers {
		wg.Add(1)
		go func(handler event.Handler) {
			ctx := context.Background()
			if err := handler.Handle(ctx, receivedEvent); err != nil {
				_ = handler.OnFailure(ctx, receivedEvent, err)
			}
			wg.Done()
		}(handlers[i])
	}
	wg.Wait()
}
