package subscriber

import (
	"bitbucket.org/captainfresh/go-common/v2/errors"
)

type defaultSubscriber struct {
	sources    []Source
	dispatcher Dispatcher
}

func NewDefaultSubscriber(dispatcher Dispatcher, sources []Source) *defaultSubscriber {
	return &defaultSubscriber{
		dispatcher: dispatcher,
		sources:    sources,
	}
}

func (subscriber *defaultSubscriber) Start() errors.BaseErr {
	subscriber.dispatcher.Subscribe(subscriber.sources)
	errChan := make(chan errors.BaseErr)
	for i := range subscriber.sources {
		go func(source Source) {
			// Source until error occurs
			errChan <- source.SourceEvents()
		}(subscriber.sources[i])
	}

	for range subscriber.sources {
		if err := <-errChan; err != nil {
			return err
		}
	}

	select {}
}
