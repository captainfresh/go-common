package subscriber

import (
	"bitbucket.org/captainfresh/go-common/v2/errors"
	"bitbucket.org/captainfresh/go-common/v2/pubsub/event"
)

// Source contains the logic of fetching the messages from queue and pushing it to a channel.
// Subscribers can use the channel to get events and take actions.
type Source interface {
	// SourceEvents is a blocking call that will source events pushed in the queue.
	// On Receiving events, it will passEventsToHandlers to the events channel
	SourceEvents() errors.BaseErr
	// BatchChannel returns the Batch channel that can be used by dispatchers to get event updates.
	BatchChannel() <-chan *event.Batch
}
