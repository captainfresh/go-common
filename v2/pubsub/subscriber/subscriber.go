package subscriber

import "bitbucket.org/captainfresh/go-common/v2/errors"

// Subscriber is the main interface that will be used to get events from sources.
type Subscriber interface {
	// Start is a blocking method that will keep fetching events from the sources.
	Start() errors.BaseErr
}
