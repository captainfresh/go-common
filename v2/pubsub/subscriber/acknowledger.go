package subscriber

import (
	"bitbucket.org/captainfresh/go-common/v2/errors"
	"bitbucket.org/captainfresh/go-common/v2/pubsub/event"
	"time"
)

// AcknowledgementAction is the type that will be used to define the actions that can be taken
type AcknowledgementAction uint8

const (
	// AcknowledgementActionReturnErr means in case of no acknowledgement received, Acknowledger will return err.
	AcknowledgementActionReturnErr AcknowledgementAction = iota
	// AcknowledgementActionSkip means in case of no acknowledgement received, Acknowledger will not return any error.
	AcknowledgementActionSkip
)

type AcknowledgerConfig struct {
}

// BatchAcknowledger handles the acknowledgement of batch processed by dispatcher
type BatchAcknowledger interface {
	// Acknowledge is a blocking call and must handle the acknowledgment sent by dispatchers by source.
	// Clients implementing this interface must return error if they don't want to increase the Checkpoint.
	Acknowledge(batch *event.Batch) errors.BaseErr
}

// TimeoutAcknowledger will wait till the timout duration,
// if no acknowledgement is received within the timeframe defined, Acknowledger will either return err or nil based on action.
type TimeoutAcknowledger struct {
	timeout time.Duration
	action  AcknowledgementAction
}

func NewTimeoutAcknowledger(timeout time.Duration, action AcknowledgementAction) *TimeoutAcknowledger {
	return &TimeoutAcknowledger{
		timeout: timeout,
		action:  action,
	}
}

func (acknowledger *TimeoutAcknowledger) Acknowledge(batch *event.Batch) errors.BaseErr {
	timer := time.NewTimer(acknowledger.timeout)
	select {
	case <-timer.C:
		return acknowledger.handleTimeout(batch)
	case <-batch.AckChan:
		return nil
	}
}

func (acknowledger *TimeoutAcknowledger) handleTimeout(batch *event.Batch) errors.BaseErr {
	switch acknowledger.action {
	case AcknowledgementActionSkip:
		return nil
	default:
		return errors.New("batch timed out")
	}
}
