package subscriber

// Dispatcher can subscribe to a list of listeners and is responsible to pass the events to handlers that are interested in the events.
type Dispatcher interface {
	// Subscribe accepts a list of listeners and subscribe to the channels used by the listeners.
	Subscribe([]Source)
}
