package event

import (
	"bitbucket.org/captainfresh/go-common/v2/errors"
	"context"
)

type MatchEventField uint8

const (
	matchEventFieldNamespace MatchEventField = iota + 1
	matchEventFieldTopic
	matchEventFieldType
	matchEventFieldSubtype
)

// Handler is the interface where actions specific to application will be executed.
type Handler interface {
	FailureHandler
	// Handle method will be called from the dispatcher if the handler is interested in the event.
	Handle(context.Context, *Event) errors.BaseErr
}

// MatchingCriterion returns a MatchEventField type and a list of strings based on which a handler will be selected.
type MatchingCriterion func() (MatchEventField, []string)

// HandlerRegistry must contain all the handlers for events that an application is interested into.
type HandlerRegistry interface {
	// GetHandlers returns a list of handlers that are interested into the event.
	GetHandlers(*Event) []Handler

	// RegisterHandler will be used to register a handler based on criteria list provided.
	RegisterHandler(handler Handler, criteria ...MatchingCriterion)
}

// MatchWithNamespace is an implementation of MatchingCriterion which returns
// matchEventFieldNamespace with namespaces provided in argument
func MatchWithNamespace(namespaces []string) MatchingCriterion {
	return func() (MatchEventField, []string) {
		return matchEventFieldNamespace, namespaces
	}
}

// MatchWithTopic is an implementation of MatchingCriterion which returns
// matchEventFieldTopic with topics provided in argument
func MatchWithTopic(topics []string) MatchingCriterion {
	return func() (MatchEventField, []string) {
		return matchEventFieldTopic, topics
	}
}

// MatchWithType is an implementation of MatchingCriterion which returns
// matchEventFieldType with types provided in argument
func MatchWithType(types []string) MatchingCriterion {
	return func() (MatchEventField, []string) {
		return matchEventFieldType, types
	}
}

// MatchWithSubtype is an implementation of MatchingCriterion which returns
// matchEventFieldSubtype with subtypes provided in argument
func MatchWithSubtype(subtypes []string) MatchingCriterion {
	return func() (MatchEventField, []string) {
		return matchEventFieldSubtype, subtypes
	}
}

// Retrier is optional. Retrier can be used by handlers in case of event failures where the FailurePolicy is Retry.
type Retrier interface {
	Retry(*Event, errors.BaseErr) errors.BaseErr
}
