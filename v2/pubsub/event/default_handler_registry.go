package event

import "strings"

const criteriaMatchAll = "*"

type defaultHandlerRegistry struct {
	handlerGraph *handlerGraph
	handlerMap   map[string][]Handler
}

func NewDefaultHandlerRegistry() *defaultHandlerRegistry {
	return &defaultHandlerRegistry{handlerGraph: &handlerGraph{}, handlerMap: make(map[string][]Handler)}
}

func (registry *defaultHandlerRegistry) GetHandlers(event *Event) (handlers []Handler) {
	var matches = make([]string, 0)
	matches = append(matches, event.Topic, event.Type)
	if event.Metadata == nil || len(event.Metadata.Namespace) == 0 {
		matches = append([]string{criteriaMatchAll}, matches...)
	} else {
		matches = append([]string{event.Metadata.Namespace}, matches...)
	}

	if len(event.Metadata.Subtype) == 0 {
		matches = append(matches, criteriaMatchAll)
	} else {
		matches = append(matches, event.Metadata.Subtype)
	}

	mapKey := getHandlerMapKey(matches)
	if _, ok := registry.handlerMap[mapKey]; !ok {
		registry.handlerMap[mapKey] = registry.handlerGraph.getHandlers(matches)
	}
	return registry.handlerMap[mapKey]
}

// RegisterHandler Registers a handler based on criteria. If No criteria is provided,
// default matching criteria selected will select handler on every event match.
//
// example 1: RegisterHandler(A) will register handler that will be selected on all the events.
//
// example 2: RegisterHandler(A, MatchWithNamespace([]string{"NS1", "NS2"}))
// will select events if the event is either coming from NS1 or NS2
//
// example 3: RegisterHandler(A, MatchWithNamespace([]string{"NS1"}, MatchWithTopic([]string{"ACCOUNT"}))
// will select those events coming from NS1 and with topic ACCOUNT
func (registry *defaultHandlerRegistry) RegisterHandler(handler Handler, criteria ...MatchingCriterion) {
	criteriaMap := registry.getDefaultMatchingCriteriaMap()
	for _, criterion := range criteria {
		field, matches := criterion()
		criteriaMap[field] = matches
	}

	registry.handlerGraph.addHandler(handler, []MatchEventField{matchEventFieldNamespace, matchEventFieldTopic, matchEventFieldType, matchEventFieldSubtype}, criteriaMap)
}

func (registry *defaultHandlerRegistry) getDefaultMatchingCriteriaMap() map[MatchEventField][]string {
	return map[MatchEventField][]string{
		matchEventFieldNamespace: {criteriaMatchAll},
		matchEventFieldTopic:     {criteriaMatchAll},
		matchEventFieldType:      {criteriaMatchAll},
		matchEventFieldSubtype:   {criteriaMatchAll},
	}
}

func getHandlerMapKey(fields []string) string {
	return strings.Join(fields, ":")
}
