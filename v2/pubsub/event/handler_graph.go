package event

type handlerGraph struct {
	subGraph map[string]*handlerGraph
	handlers []Handler
}

func (graph *handlerGraph) addHandler(handler Handler, fieldList []MatchEventField, criteria map[MatchEventField][]string) {
	if len(fieldList) == 1 {
		graph.handlers = append(graph.handlers, handler)
		return
	}

	for _, match := range criteria[fieldList[0]] {
		if graph.subGraph == nil {
			graph.subGraph = make(map[string]*handlerGraph)
		}

		if graph.subGraph[match] == nil {
			graph.subGraph[match] = &handlerGraph{}
		}

		graph.subGraph[match].addHandler(handler, fieldList[1:], criteria)
	}
}

func (graph *handlerGraph) getHandlers(matches []string) (handlers []Handler) {
	if len(matches) == 0 {
		return
	}

	if len(matches) == 1 {
		handlers = append(handlers, graph.handlers...)
	}
	matchKey := matches[0]
	if subgraph, ok := graph.subGraph[matchKey]; ok {
		handlers = append(handlers, subgraph.getHandlers(matches[1:])...)
	}

	if subgraph, ok := graph.subGraph[criteriaMatchAll]; ok {
		handlers = append(handlers, subgraph.getHandlers(matches[1:])...)
	}

	return
}
