package event

import (
	"bitbucket.org/captainfresh/go-common/v2/errors"
	"github.com/go-playground/validator/v10"
	"strings"
	"time"
)

var validate *validator.Validate

func init() {
	validate = validator.New()
}

type Metadata struct {
	// ServiceName is the publisher's name
	ServiceName string `json:"serviceName,omitempty"`
	// Namespace contains the namespace information
	Namespace string `json:"namespace,omitempty"`

	// Subtype of event can be used to put extra information of event details. e.g.
	// In case of price UPDATE, We can use Subtype PRICE_INCREASE/PRICE_DECREASE.
	Subtype string                 `json:"subType,omitempty"`
	Custom  map[string]interface{} `json:"custom,omitempty"`
}

type Event struct {
	// PartitionKey is used to decide in which shard/partition
	// of the queue the event will be pushed to
	PartitionKey string `json:"partitionKey,omitempty" validate:"min=1"`

	// Type of event. Example, CREATE/UPDATE/DELETE
	Type string `json:"type,omitempty" validate:"min=1"`

	// Topic determines to which queue/stream the event will be pushed to
	Topic string `json:"topic,omitempty" validate:"min=1"`

	Timestamp time.Time   `json:"timestamp"`
	Data      interface{} `json:"data,omitempty" validate:"required"`
	Metadata  *Metadata   `json:"metadata,omitempty"`
}

type Events []*Event

// ValidateAndPrepare validates the event and adds extra information such as timestamp.
func (event *Event) ValidateAndPrepare() error {
	event.PartitionKey = strings.TrimSpace(event.PartitionKey)
	event.Topic = strings.TrimSpace(event.Topic)
	event.Type = strings.TrimSpace(event.Type)
	event.Timestamp = time.Now().UTC()

	return validate.Struct(event)
}

// AcknowledgementType is the types of Acknowledgements accepted by Source once the events are processed.
// It is helpful in those sources that uses offsets to read next batch of events.
type AcknowledgementType uint8

const (
	// AcknowledgmentTypeAllProcessed means all the events have been processed successfully by dispatchers
	AcknowledgmentTypeAllProcessed AcknowledgementType = iota + 1
)

type Record struct {
	Raw   []byte
	Event *Event
	Err   errors.BaseErr
}

// Batch is a batch of events received from source in one read.
type Batch struct {
	Records []*Record
	// Sources will use this channel to receive acknowledgments
	AckChan chan AcknowledgementType
}
