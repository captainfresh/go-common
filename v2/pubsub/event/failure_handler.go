package event

import (
	"bitbucket.org/captainfresh/go-common/v2/errors"
	"context"
)

// FailurePolicy will decide how events will be handled in case of error.
type FailurePolicy int8

const (
	// FailurePolicyDoNothing will do nothing and return nil as an error.
	FailurePolicyDoNothing FailurePolicy = iota
	// FailurePolicyReturnErr will return the error as it is.
	FailurePolicyReturnErr
)

// FailureHandler implementation will decide how a failure will be handled in case of Publish/Consume events.
type FailureHandler interface {
	OnFailure(ctx context.Context, event *Event, err errors.BaseErr) errors.BaseErr
}

type FailureHandlerReturnErr struct{}

func NewFailureHandlerReturnErr() *FailureHandlerReturnErr {
	return &FailureHandlerReturnErr{}
}

func (handler *FailureHandlerReturnErr) OnFailure(_ context.Context, _ *Event, err errors.BaseErr) errors.BaseErr {
	return err
}
