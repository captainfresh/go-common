package publisher

import (
	"bitbucket.org/captainfresh/go-common/v2/errors"
	"bitbucket.org/captainfresh/go-common/v2/pubsub/event"
	"context"
)

// Publisher is responsible to publish event to the queue.
// Implementation of Publisher must take care of the queue configuration.
// Implementor must also take care of event failures
type Publisher interface {
	event.FailureHandler
	// Publish will publish the event into the queue specified by the queueName argument. In case of publish failure, OnFailure will be called
	Publish(ctx context.Context, queueName string, event *event.Event) errors.BaseErr
}
