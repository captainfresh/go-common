package config

const (
	envPrefix         = "CF"
	defaultConfigFile = "default"
	configType        = "yaml"
)
