package config

import (
	"fmt"
	"os"
	"path"
	"strings"

	"github.com/spf13/viper"
)

var Viper *viper.Viper

type InitConfigParams struct {
	BaseConfigPath     string
	BaseConfigFile     string
	OverrideConfigPath string
	OverrideConfigFile string
}

/**Sample
--for docker instances
params = InitConfigParams {
BaseConfigPath: "\app\config"
BaseConfigFile: "default"
OverrideConfigPath: "\app"
OverrideConfigFile: "config"
}
 --for test files
params = InitConfigParams {
BaseConfigPath: "\config"
BaseConfigFile: "default"
OverrideConfigPath: "\config"
OverrideConfigFile: "test"
}
*/

// Init function initialises the config. It takes the base config path and env as argument.
// baseConfigPath: where the config files are placed.
// env: file to override default configs with.
func Init(params InitConfigParams) (err error) {
	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvPrefix(envPrefix)
	viper.SetEnvKeyReplacer(replacer)
	viper.AutomaticEnv()
	setConfigFromFile(params.BaseConfigFile, params.BaseConfigPath)
	err = viper.ReadInConfig()
	if err != nil {
		fmt.Println("error in reading config from file: ", err)
		return
	}
	if len(params.OverrideConfigFile) != 0 && len(params.OverrideConfigPath) != 0 {
		if err = override(params.OverrideConfigFile, params.OverrideConfigPath); err != nil {
			fmt.Println("error in overriding config: ", err)
			return
		}
	}
	Viper = viper.GetViper()
	return nil
}

func override(configFileName, configPath string) error {
	fileName := configFileName + "." + configType
	if _, err := os.Stat(path.Join(configPath, fileName)); err != nil {
		return err
	}
	setConfigFromFile(configFileName, configPath)
	return viper.MergeInConfig()
}

func setConfigFromFile(configFileName, configPath string) {
	viper.SetConfigName(configFileName)
	viper.SetConfigType(configType)
	viper.AddConfigPath(configPath)
}
