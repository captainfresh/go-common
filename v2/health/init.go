package health

import (
	"bitbucket.org/captainfresh/go-common/v2/config"
	"database/sql"
	"fmt"
	"github.com/heptiolabs/healthcheck"
	"gopkg.in/yaml.v3"
	"gorm.io/gorm"
	"net/http"
)

// Init will initialize and registers the liveliness and readiness APIs on the port provided in config.
// Init will throw error if the config provided is invalid and will panic in case health-check server
// is unable to start or shutdowns after starting.
func Init(config *Config) (err error) {
	if err = config.ValidateAndPrepare(); err != nil {
		return
	}
	handler := healthcheck.NewHandler()
	for _, checkConfig := range config.ReadinessDependencies {
		switch checkConfig.Type {
		case DependencyTypeWebApp:
			handler.AddReadinessCheck(checkConfig.Name, healthcheck.HTTPGetCheck(checkConfig.URL, checkConfig.Timeout))
		case DependencySQLDBPing:
			handler.AddReadinessCheck(checkConfig.Name, healthcheck.DatabasePingCheck(config.DB, checkConfig.Timeout))
		}
	}

	go func() {
		if err = http.ListenAndServe("0.0.0.0:"+config.Port, handler); err != nil {
			panic(err)
		}
	}()
	return nil
}

// GetConfigFromViper is a helper function that uses viper to read health configs.
func GetConfigFromViper(db *gorm.DB) (*Config, error) {
	cfgBytes, err := yaml.Marshal(config.Viper.Get("health.readiness"))
	if err != nil {
		return nil, err
	}
	var dependencies []*ReadinessDependency
	if err = yaml.Unmarshal(cfgBytes, &dependencies); err != nil {
		return nil, err
	}

	var dbDriver *sql.DB
	if db != nil {
		dbDriver, _ = db.DB()
	}

	return &Config{DB: dbDriver, ReadinessDependencies: dependencies, Port: config.Viper.GetString("health.port")}, nil
}

// InitWithViperConfig initialises health check using GetConfigFromViper for reading configs
func InitWithViperConfig(db *gorm.DB) error {
	if !config.Viper.GetBool("health.enable") {
		fmt.Println("health check apis are disabled")
		return nil
	}

	healthConfig, err := GetConfigFromViper(db)
	if err != nil {
		return err
	}

	return Init(healthConfig)
}
