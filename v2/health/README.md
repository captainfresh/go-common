# health
The package exposes two APIs (`/live` and `/ready`) that can be used by k8s to monitor the health of the application.
The readiness API when hit will check all the dependent services and will wait for their response
## Usage

example:
```go
config := &health.Config{
		DB:   utility.GetDB(),
		ReadinessDependencies: []*health.ReadinessDependency{
			{
				Name: "accounts-service",
				URL:  "http://accounts-manager/live",
			},
			{
				Name: "database",
				Type: health.DependencySQLDBPing,
				Timeout: time.Second,
			},
		},
	}
	err := health.Init(config)
	if err != nil {
		return err
	}

```