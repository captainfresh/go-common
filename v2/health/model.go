package health

import (
	"bitbucket.org/captainfresh/go-common/v2/errors"
	"database/sql"
	"time"
)

// DependencyType is the type of readiness configuration.
type DependencyType string

const (
	DependencyTypeWebApp DependencyType = "WebApp"
	DependencySQLDBPing  DependencyType = "SQLPing"

	defaultTimeout = time.Second * 5
	defaultPort    = "8069"
)

type Config struct {
	DB                    *sql.DB
	Port                  string                 `json:"port" validate:"required,gte=4"`
	ReadinessDependencies []*ReadinessDependency `json:"configs"`
}

type ReadinessDependency struct {
	Name    string         `json:"name" validate:"required"`
	Type    DependencyType `json:"type"`
	URL     string         `json:"url" validate:"required"`
	Timeout time.Duration  `json:"timeout"`
}

func (config *Config) ValidateAndPrepare() error {
	if config == nil {
		return nil
	}

	if len(config.Port) == 0 {
		config.Port = defaultPort
	}

	for _, readinessConfig := range config.ReadinessDependencies {
		if len(readinessConfig.Type) == 0 {
			readinessConfig.Type = DependencyTypeWebApp
		}

		if readinessConfig.Timeout == 0 {
			readinessConfig.Timeout = defaultTimeout
		}

		if readinessConfig.Type == DependencySQLDBPing && config.DB == nil {
			return errors.New("db driver is required for DependencySQLDBPing")
		}
	}
	return nil
}
