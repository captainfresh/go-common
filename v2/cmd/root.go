package cmd

import (
	"github.com/go-gormigrate/gormigrate/v2"
	"github.com/spf13/cobra"
	"log"
)

type Command struct {
	// Use is the command to be run on `go run`
	Use string
	// Short is the short description describing the command
	Short string
	// Description is the description of command in detail
	Description string
	// Run is the function executed when the command 'use' is passed as run argument.
	// If the function returns error, the program will exit
	Run func() error

	// SubCommands are the command args valid for the parent command. For example `go run migrate up/down.
	//'migrate' is the parent command and up and down are the possible args.
	SubCommands []*Command
}

type ExecuteParams struct {
	// Dsn is the postgres database dsn used in migration command
	Dsn string

	// function that contains application initialization logic, eg. create database connection, server...
	// used in serve command
	ServeFunc func() error

	// Complex migrations which include more than only sql queries like fetch data from other service and insert.
	ScriptMigrations []*gormigrate.Migration
	// Commands is used to pass additional run commands apart from the basic commands given out of the box by this package.
	Commands []*Command
}

var executeParams *ExecuteParams

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute(params *ExecuteParams) {
	executeParams = params
	addCustomCommands(rootCmd, params.Commands)
	err := rootCmd.Execute()
	if err != nil {
		log.Fatal(err)
	}
}

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "root command",
	Short: "A brief description of your application",
	Long: `A longer description that spans multiple lines and likely contains
examples and usage of using your application. For example:
`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
}

func init() {
	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.
	// rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file")
	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func addCustomCommands(parent *cobra.Command, commands []*Command) {
	for _, command := range commands {
		cobraCommand := &cobra.Command{
			Use:   command.Use,
			Short: command.Short,
			Long:  command.Description,
			Run: func(_ *cobra.Command, _ []string) {
				if err := command.Run(); err != nil {
					log.Fatal(err)
				}
			},
		}
		addCustomCommands(cobraCommand, command.SubCommands)
		parent.AddCommand(cobraCommand)
	}
}
