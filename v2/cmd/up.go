package cmd

import (
	"bitbucket.org/captainfresh/go-common/v2/database"
	"github.com/spf13/cobra"
	"log"
)

// upCmd represents the up command
var upCmd = &cobra.Command{
	Use:   "up",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:
`,
	Run: func(cmd *cobra.Command, args []string) {
		steps, err := getStepsFromArgs(args)
		if err != nil {
			log.Fatal(err)
		}
		m := &database.Migrate{
			ScriptMigrations: executeParams.ScriptMigrations,
			Steps:            int(steps),
		}
		err = m.Up(executeParams.Dsn)
		if err != nil {
			log.Fatal(err)
		}
	},
}

func init() {
	migrateCmd.AddCommand(upCmd)
	// Here you will define your flags and configuration settings.
	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// upCmd.PersistentFlags().String("foo", "", "A help for foo")
	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// upCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
