package cmd

import (
	"bitbucket.org/captainfresh/go-common/v2/database"
	"fmt"
	"github.com/spf13/cobra"
	"log"
)

// downCmd represents the down command
var downCmd = &cobra.Command{
	Use:   "down",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:
`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("down called")
		steps, err := getStepsFromArgs(args)
		if err != nil {
			log.Fatal(err)
		}
		m := &database.Migrate{Steps: int(steps)}
		err = m.Down(executeParams.Dsn)
		if err != nil {
			log.Fatal(err)
		}
	},
}

func init() {
	migrateCmd.AddCommand(downCmd)
	// Here you will define your flags and configuration settings.
	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// downCmd.PersistentFlags().String("foo", "", "A help for foo")
	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// downCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
