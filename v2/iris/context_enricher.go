package iris

import (
	"bitbucket.org/captainfresh/go-common/v2/constant"
	"context"
	"github.com/kataras/iris/v12"
)

// CreateContext initializes context.Context using the http request headers.
func CreateContext(irisCtx iris.Context) context.Context {
	ctx := irisCtx.Request().Context()
	ctx = context.WithValue(ctx, constant.CtxNamespace, irisCtx.GetHeader(constant.CtxNamespace))
	ctx = context.WithValue(ctx, constant.CtxUserID, irisCtx.GetHeader(constant.CtxUserID))
	ctx = context.WithValue(ctx, constant.CtxAccountID, irisCtx.GetHeader(constant.CtxAccountID))
	ctx = context.WithValue(ctx, constant.CtxAuthHeader, irisCtx.GetHeader(constant.CtxAuthHeader))
	ctx = context.WithValue(ctx, constant.CtxOrgID, irisCtx.GetHeader(constant.CtxOrgID))
	ctx = context.WithValue(ctx, constant.CtxUserKey, constant.CtxUserID)
	ctx = context.WithValue(ctx, constant.HeaderCookie, irisCtx.GetHeader(constant.HeaderCookie))
	ctx = context.WithValue(ctx, constant.HeaderKeyClientID, irisCtx.GetHeader(constant.HeaderKeyClientID))
	ctx = context.WithValue(ctx, constant.HeaderKeyClientSecret, irisCtx.GetHeader(constant.HeaderKeyClientSecret))

	return ctx
}
