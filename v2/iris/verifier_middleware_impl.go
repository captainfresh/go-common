package iris

import (
	"bitbucket.org/captainfresh/go-common/v2/constant"
	"bitbucket.org/captainfresh/go-common/v2/errors"
	"bitbucket.org/captainfresh/go-common/v2/model"
	"context"
	"fmt"
	"github.com/jinzhu/copier"
	"github.com/kataras/iris/v12"
	"log"
)

// VerifyAuthorizationHeader validate the authorization cookie header.
// constant.HeaderCookie must contain sAccessToken and sIdRefreshToken.
// User details are then fetched from the successful realm response and stored in context.
func (m *middleware) VerifyAuthorizationHeader(ctx iris.Context) {
	validateContext(ctx, constant.HeaderCookie, m.authorizer.ValidateAuthorizationHeader)
}

// VerifyNamespaceUser validates user_id and namespace provided in the http request headers.
// It verifies user belongs to that namespace, by checking {user_id: ctx.user_id, type=ctx.namespace}
// mapping exists in user_mappings table in account-manager
func (m *middleware) VerifyNamespaceUser(ctx iris.Context) {
	keys := fmt.Sprintf("%s and %s", constant.CtxNamespace, constant.CtxUserID)
	validateContext(ctx, keys, m.authorizer.ValidateNamespaceUser)
}

// VerifyAccountUser validates user_id and account_id provided in the http request headers.
// It verifies user belongs to that account, by checking {user_id: ctx.user_id, type: ACCOUNT, type_ref: ctx.account_id}
// mapping exists in user_mappings table in account-manager
func (m *middleware) VerifyAccountUser(ctx iris.Context) {
	keys := fmt.Sprintf("%s and %s", constant.CtxAccountID, constant.CtxUserID)
	validateContext(ctx, keys, m.authorizer.ValidateAccountUser)
}

// VerifyAccountManager validates user_id and account_id provided in the context.
// It verifies user is account manager, by checking {user_id: ctx.user_id, type: ACCOUNT, type_ref: ctx.account_id, role: OWNER/ADMIN}
// mapping exists in user_mappings table in account-manager
func (m *middleware) VerifyAccountManager(ctx iris.Context) {
	keys := fmt.Sprintf("%s and %s", constant.CtxAccountID, constant.CtxUserID)
	validateContext(ctx, keys, m.authorizer.ValidateAccountManager)
}

// VerifyNamespaceAndUser validates user_id and namespace provided in the http request headers.
// It verifies {code: ctx.namespace} exists in namespaces table and {user_id: ctx.user_id, status: ACTIVE} mapping
// exists in user_mappings table in account-manager
func (m *middleware) VerifyNamespaceAndUser(ctx iris.Context) {
	keys := fmt.Sprintf("%s and %s", constant.CtxNamespace, constant.CtxUserID)
	validateContext(ctx, keys, m.authorizer.ValidateNamespaceAndUser)
}

// VerifyNamespace validates namespace provided in the http request headers.
// It verifies {code: ctx.namespace} exists in namespaces table in account-manager
func (m *middleware) VerifyNamespace(ctx iris.Context) {
	validateContext(ctx, constant.CtxNamespace, m.authorizer.ValidateNamespace)
}

//--------------------------------------------HELPER FUNCTIONS-----------------------------------------------------//

// validateContext will validate the given context-key and update validated data in context
func validateContext(ctx iris.Context, key string, keyValidator func(ctx context.Context) (*model.ValidatedData, error)) {
	if resp, err := keyValidator(CreateContext(ctx)); err != nil {
		switch err.(type) {
		case errors.RequiredHeaderMissingErr:
			if key == constant.CtxAuthHeader {
				ctx.Problem(iris.NewProblem().Status(iris.StatusUnauthorized).Detail(err.Error()))
			} else {
				ctx.Problem(iris.NewProblem().Status(iris.StatusForbidden).Detail(err.Error()))
			}
		case errors.BadRequestErr:
			ctx.Problem(iris.NewProblem().Status(iris.StatusBadRequest).Detail(err.Error()))
		case errors.ForbiddenErr:
			ctx.Problem(iris.NewProblem().Status(iris.StatusForbidden).Detail(err.Error()))
		case errors.UnauthenticatedErr:
			ctx.Problem(iris.NewProblem().Status(iris.StatusUnauthorized).Detail(err.Error()))
		default:
			ctx.Problem(iris.NewProblem().Status(iris.StatusInternalServerError).Detail(err.Error()))
		}
		log.Printf("verifying %s failed: %s\n", key, err.Error())
		ctx.StopExecution()
		return
	} else {
		updateValidatedData(ctx, resp)
		ctx.Next()
	}
}

// updateValidatedData Update user, namespace and account validated data in context
func updateValidatedData(ctx iris.Context, data *model.ValidatedData) {
	validatedData, _ := ctx.Values().Get(constant.CtxValidatedData).(*model.ValidatedData)

	if validatedData == nil {
		validatedData = model.NewValidatedData()
	}

	if data != nil {
		// safe case - if data.user is nil, copy operation will do nothing.
		options := copier.Option{DeepCopy: true, IgnoreEmpty: true}
		if data.User != nil {
			if err := copier.CopyWithOption(&validatedData.User, &data.User, options); err != nil {
				log.Println("failed to copy validated user, errors: ", err.Error())
			}
		}
		if data.Account != nil {
			if err := copier.CopyWithOption(&validatedData.Account, &data.Account, options); err != nil {
				log.Println("failed to copy validated account, errors: ", err.Error())
			}
		}
		if data.Namespace != nil {
			if err := copier.CopyWithOption(&validatedData.Namespace, &data.Namespace, options); err != nil {
				log.Println("failed to copy validated namespace, errors: ", err.Error())
			}
		}
	}
	ctx.Values().Set(constant.CtxValidatedData, validatedData)
}
