package iris

import (
	"bitbucket.org/captainfresh/go-common/v2/auth"
	"fmt"
	"github.com/kataras/iris/v12"
)

type VerifierMiddleware interface {
	VerifyAuthorizationHeader(ctx iris.Context)
	VerifyNamespaceUser(ctx iris.Context)
	VerifyAccountUser(ctx iris.Context)
	VerifyAccountManager(ctx iris.Context)
	VerifyNamespaceAndUser(ctx iris.Context)
	VerifyNamespace(ctx iris.Context)
}

// Middleware that wraps request authorizer
type middleware struct {
	authorizer auth.Authorizer
}

// NewVerifierMiddleware returns an instance of middleware initialised with authorizer
func NewVerifierMiddleware(amsURL, realmUrl string) (VerifierMiddleware, error) {
	if len(amsURL) == 0 {
		return nil, fmt.Errorf("not provided AMS Url in config")
	}
	if len(realmUrl) == 0 {
		return nil, fmt.Errorf("not provided realm Url in config")
	}
	return &middleware{
		authorizer: auth.NewAuthorizer(amsURL, realmUrl),
	}, nil
}
