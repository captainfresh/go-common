package redis

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-redis/redis/v9"
	"github.com/pkg/errors"
	"time"
)

var client *redis.Client

const KeyNotExistError = redis.Nil

// Init Initialises connection to redis for provided host port and database. Set password to "" if redis server is not
// password protected.
func Init(host string, port int, password string, database int) {
	client = redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%d", host, port),
		Password: password,
		DB:       database,
	})
	if client != nil {
		fmt.Println("Connected to redis")
	}
}

// GetClient returns redis client object.
func GetClient() *redis.Client {
	return client
}

// Set sets the value to key with an expiry time on redis. Set expiry to -1 if the key should not have TTL.
func Set(ctx context.Context, key string, value interface{}, expiry time.Duration) (err error) {
	bytes, err := json.Marshal(value)
	if err != nil {
		return errors.Wrap(err, "Redis Set error: ")
	}

	return errors.Wrap(client.Set(ctx, key, bytes, expiry).Err(), "Redis Set error: ")
}

// Get gets the key from redis and stores its result into target. target must be a pointer.
func Get(ctx context.Context, key string, target interface{}) error {
	value, err := client.Get(ctx, key).Result()
	if err != nil {
		return err
	}

	return json.Unmarshal([]byte(value), &target)
}

// Disconnect closes connection to redis server.
func Disconnect() error {
	return client.Close()
}
