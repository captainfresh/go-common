package database

import (
	"fmt"
	"github.com/go-gormigrate/gormigrate/v2"
	migrate4 "github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"log"
	"os"
	"strings"
)

type Migrate struct {
	Steps            int
	ScriptMigrations []*gormigrate.Migration
}

// GetPath gets the absolute path of migration scripts. This function expects the migration scripts are placed in
// /migrations directory in root folder.
func (m *Migrate) GetPath() (filePath string, err error) {
	myDir, err := os.Getwd()
	if err != nil {
		err = fmt.Errorf("failed to get directory: %s ", err.Error())
		log.Println(err)
		return
	}
	filePath = "file://" + myDir + "/migrations/"
	return
}

// Up applies sql and script migrations. It will apply all pending up migrations.
func (m *Migrate) Up(dsn string) (err error) {
	err = m.MigrateSql(dsn)
	if err != nil {
		fmt.Printf("error running sql migrations: %s", err.Error())

		return err
	}

	// It is not always necessary that the migration contains extra scripts
	if len(m.ScriptMigrations) == 0 {
		fmt.Println("No migration scripts found to run")
		return nil
	}
	err = m.MigrateScripts(dsn)
	if err != nil {
		fmt.Printf("error running script migrations: %s", err.Error())

		return err
	}

	return nil
}

// MigrateSql applies sql migrations placed in ./migrations directory in root folder.
func (m *Migrate) MigrateSql(dsn string) (err error) {
	if len(dsn) == 0 {
		return fmt.Errorf("database DSN not provided")
	}
	myDir, err := m.GetPath()
	if err != nil {
		err = fmt.Errorf("failed to get migration scripts path: %s ", err.Error())
		log.Println(err)
		return
	}
	mig, err := migrate4.New(myDir, dsn)
	if err != nil {
		err = fmt.Errorf("failed to get migrate instance: %s", err.Error())
		log.Println(err)
		return
	}

	if m.Steps < 0 {
		return fmt.Errorf("steps must be a positive number")
	}

	if m.Steps != 0 {
		err = mig.Steps(m.Steps)
	} else {
		err = mig.Up()
	}

	if err != nil {
		if strings.Contains(err.Error(), "no change") {
			fmt.Println("No pending changes to apply on database migration")
			return nil
		}

		err = fmt.Errorf("failed to run migration: %s", err.Error())
		log.Println(err)
		return
	}

	fmt.Println("Database migrations are successful")
	return
}

// MigrateScripts applies script migrations provided while calling cmd.Execute method.
func (m *Migrate) MigrateScripts(dsn string) (err error) {
	if len(dsn) == 0 {
		return fmt.Errorf("database DSN not provided")
	}

	err = InitPostgres(&PostgresConnectionConfig{
		DSN: dsn,
	})
	if err != nil {
		return err
	}

	gormMigrate := gormigrate.New(GetDatabase(), &gormigrate.Options{
		TableName: "script_migrations",
	}, m.ScriptMigrations)
	if err = gormMigrate.Migrate(); err != nil {
		return err
	}
	fmt.Println("Script migrations are successful")
	DisconnectDatabase()

	return nil
}

// Down applies database migrations. It will apply 1 down migration.
// This function expects the migration scripts are placed in ./migrations directory in root folder.
func (m *Migrate) Down(dsn string) (err error) {
	if len(dsn) == 0 {
		return fmt.Errorf("database DSN not provided")
	}
	myDir, err := m.GetPath()
	if err != nil {
		err = fmt.Errorf("failed to get migration scripts path: %s ", err.Error())
		log.Println(err)
		return
	}
	mig, err := migrate4.New(myDir, dsn)
	if err != nil {
		err = fmt.Errorf("failed to get migrate instance: %s", err.Error())
		log.Println(err)
		return
	}
	if m.Steps < 0 {
		return fmt.Errorf("steps must be a positive number")
	}

	if m.Steps != 0 {
		err = mig.Steps(-m.Steps)
	} else {
		err = mig.Down()
	}

	if err != nil {
		err = fmt.Errorf("failed to run migration: %s", err.Error())
		log.Println(err)
		return
	}
	return nil
}
