package database

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"os"
	"time"
)

var db *gorm.DB

type PostgresConnectionConfig struct {
	DSN                string
	MaxIdleConnections int
	MaxOpenConnections int
}

// InitPostgres initializes postgres connection with provided configuration.
func InitPostgres(config *PostgresConnectionConfig) (err error) {
	if len(config.DSN) == 0 {
		return fmt.Errorf("database DSN not provided")
	}
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold:             time.Second, // Slow SQL threshold
			LogLevel:                  logger.Info, // Log level
			IgnoreRecordNotFoundError: false,       // Ignore ErrRecordNotFound errors for logger
			Colorful:                  true,        // Disable color
		},
	)
	db, err = gorm.Open(postgres.Open(config.DSN), &gorm.Config{
		SkipDefaultTransaction: false,
		Logger:                 newLogger,
	})
	if err != nil {
		return fmt.Errorf("Connection : unable to establish connection with postgres: %w ", err)
	}
	sqlDB, err := db.DB()
	if err != nil {
		return fmt.Errorf("SQLDB : unable to establish connection with postgres: %w ", err)
	}
	err = sqlDB.Ping()
	if err != nil {
		return fmt.Errorf("Ping failed : unable to establish connection with postgres: %w ", err)
	}
	if config.MaxIdleConnections > 0 {
		sqlDB.SetMaxIdleConns(config.MaxIdleConnections)
	}
	if config.MaxOpenConnections > 0 {
		sqlDB.SetMaxOpenConns(config.MaxOpenConnections)
	}
	return nil
}

// DisconnectDatabase closes the connection to database.
func DisconnectDatabase() {
	fmt.Println(`Disconnecting from database...`)
	dbInstance, err := GetDatabase().DB()
	if err != nil {
		fmt.Printf(`Error retrieving sql DB: %v`, err.Error())

		return
	}

	err = dbInstance.Close()
	if err != nil {
		fmt.Printf(`Error disconnecting from db: %v`, err.Error())

		return
	}
}

// GetDatabase returns the database instance
func GetDatabase() *gorm.DB {
	return db
}
