package translation

import (
	"errors"
	"strings"
)

func GetTagFromString(tagString string) (*Tag, error) {
	tagSlice := strings.Split(tagString, tagSeparator)
	return getTagFromTagSlice(tagSlice)
}

func getTagFromTagSlice(tagSlice []string) (tag *Tag, err error) {
	tag = &Tag{}
	var hasTranslateTag, hasTypeTag bool
	for _, tagString := range tagSlice {
		if isTranslateTag(tagString) {
			hasTranslateTag = true
			tag.Translate = getBoolValueFromString(tagString)
			continue
		}

		if isMethodTag(tagString) {
			hasTypeTag = true
			tag.Method, err = getMethodValueFromString(tagString)
			if err != nil {
				return
			}
			continue
		}
		return nil, errors.New("invalid tag value '" + tagString + "'")
	}

	if !hasTypeTag {
		tag.Method = defaultTypeTag
	}

	if !hasTranslateTag {
		tag.Translate = defaultTranslateTag
	}

	return
}

func isTranslateTag(tag string) bool {
	if tag == "true" || tag == "false" {
		return true
	}

	return false
}

func getBoolValueFromString(boolStr string) bool {
	return boolStr == "true"
}

func isMethodTag(tagString string) bool {
	return strings.HasPrefix(tagString, methodTag)
}

func getMethodValueFromString(tagString string) (Method, error) {
	tagParts := strings.Split(tagString, tagValueSeparator)
	if len(tagParts) != 2 {
		return "", errors.New("invalid tag value for method: '" + tagString + "'")
	}

	switch Method(tagParts[1]) {
	case MethodExactMatch:
		return MethodExactMatch, nil
	default:
		return "", errors.New("invalid method tag value: '" + tagParts[1] + "'")
	}
}
