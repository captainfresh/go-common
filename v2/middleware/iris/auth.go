package iris

import (
	"bitbucket.org/captainfresh/go-common/v2/constant"
	"github.com/kataras/iris/v12"
)

// Authenticate : Priority - Cookie -> Client Id and Secret -> Api Key .
func Authenticate(realmHost string) iris.Handler {
	return func(ctx iris.Context) {
		clientID := ctx.GetHeader(constant.HeaderKeyClientID)
		if len(clientID) != 0 {
			VerifyClientIDSecret(realmHost)(ctx)
			return
		}
		VerifySupertokenSession(realmHost)(ctx)
	}
}
