package iris

import (
	"bitbucket.org/captainfresh/go-common/v2/clients"
	"bitbucket.org/captainfresh/go-common/v2/constant"
	"bitbucket.org/captainfresh/go-common/v2/errors"
	"bitbucket.org/captainfresh/go-common/v2/model"
	irisutility "bitbucket.org/captainfresh/go-common/v2/utility/iris"
	"fmt"
	"github.com/kataras/iris/v12"
	"log"
	"net/http"
)

func VerifyClientIDSecret(realmHost string) iris.Handler {
	return func(ctx iris.Context) {
		clientID := ctx.GetHeader(constant.HeaderKeyClientID)
		clientSecret := ctx.GetHeader(constant.HeaderKeyClientSecret)

		if len(clientID) == 0 || len(clientSecret) == 0 {
			irisutility.ErrorResponse(ctx, http.StatusForbidden, errors.New("forbidden"))
			return
		}

		realmClient := clients.NewRealmClient(realmHost)
		statusCode, response, err := realmClient.VerifyClientIDSecret(clientID, clientSecret)
		if err != nil {
			irisutility.ErrorResponse(ctx, statusCode, err)
			return
		} else if statusCode != 200 {
			msg := fmt.Sprintf("failed to verify clientID: %s, secret: ***masked***, error: %d\n", clientID, statusCode)
			log.Printf(msg)
			irisutility.ErrorResponse(ctx, statusCode, fmt.Errorf("forbidden"))
			return
		}

		user := ctx.GetHeader(constant.CtxUserID)
		if len(user) == 0 {
			user = clientID
			ctx.Values().Set(constant.CtxUserKey, constant.ClientId)
			ctx.Values().Set(constant.ClientId, response.Data.ClientID)
		}

		validatedData := model.ValidatedData{User: &model.ValidatedUser{ID: user}}
		ctx.Values().Set(constant.CtxValidatedData, validatedData)

		ctx.Next()
	}
}
