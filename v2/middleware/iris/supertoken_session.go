package iris

import (
	"bitbucket.org/captainfresh/go-common/v2/clients"
	"bitbucket.org/captainfresh/go-common/v2/constant"
	"bitbucket.org/captainfresh/go-common/v2/model"
	irisutility "bitbucket.org/captainfresh/go-common/v2/utility/iris"
	"fmt"
	"github.com/kataras/iris/v12"
	"log"
	"net/http"
)

func VerifySupertokenSession(realmHost string) iris.Handler {
	return func(ctx iris.Context) {
		cookie := ctx.GetHeader(constant.HeaderCookie)

		if len(cookie) == 0 {
			irisutility.ErrorResponse(ctx, http.StatusForbidden, fmt.Errorf("forbidden"))
			return
		}

		realmClient := clients.NewRealmClient(realmHost)
		statusCode, response, err := realmClient.SessionInfo(cookie)
		if err != nil {
			irisutility.ErrorResponse(ctx, statusCode, err)
			return
		} else if statusCode != 200 {
			msg := fmt.Sprintf("failed to verify cookie: ***masked***, error: %d\n", statusCode)
			log.Printf(msg)
			irisutility.ErrorResponse(ctx, statusCode, fmt.Errorf("forbidden"))
			return
		}

		validatedData := model.ValidatedData{
			User: &model.ValidatedUser{
				ID:      response.Data.UserID,
				Name:    response.Data.Name,
				Email:   response.Data.Email,
				Picture: "",
			},
		}
		ctx.Values().Set(constant.CtxValidatedData, validatedData)

		ctx.Values().Set(constant.CtxUserKey, constant.UserID)
		ctx.Values().Set(constant.CtxUserID, response.Data.UserID)
		ctx.Values().Set(constant.HeaderKeySessionHandle, response.Data.SessionHandle)
		ctx.Next()
	}
}
