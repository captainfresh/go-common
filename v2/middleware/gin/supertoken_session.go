package gin

import (
	"bitbucket.org/captainfresh/go-common/v2/clients"
	"bitbucket.org/captainfresh/go-common/v2/constant"
	ginutility "bitbucket.org/captainfresh/go-common/v2/utility/gin"
	"github.com/gin-gonic/gin"
)

func VerifySupertokenSession(realmHost string) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		cookie := ctx.GetHeader(constant.HeaderCookie)

		realmClient := clients.NewRealmClient(realmHost)
		code, response, err := realmClient.SessionInfo(cookie)
		if err != nil {
			ginutility.ErrorResponse(ctx, code, err)
			ctx.Abort()

			return
		}

		ctx.Set(constant.CtxUserKey, constant.UserID)
		ctx.Set(constant.UserID, response.Data.UserID)
		ctx.Set(constant.HeaderKeySessionHandle, response.Data.SessionHandle)
		ctx.Next()
	}
}
