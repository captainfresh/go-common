package gin

import (
	"bitbucket.org/captainfresh/go-common/v2/clients"
	"bitbucket.org/captainfresh/go-common/v2/constant"
	gin2 "bitbucket.org/captainfresh/go-common/v2/utility/gin"
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
)

func VerifyClientIDSecret(realmHost string) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		clientID := ctx.GetHeader(constant.HeaderKeyClientID)
		clientSecret := ctx.GetHeader(constant.HeaderKeyClientSecret)

		if clientID == "" || clientSecret == "" {
			gin2.ErrorResponse(ctx, http.StatusForbidden, errors.New("forbidden"))
			ctx.Abort()

			return
		}

		realmClient := clients.NewRealmClient(realmHost)
		statusCode, response, err := realmClient.VerifyClientIDSecret(clientID, clientSecret)
		if err != nil {
			gin2.ErrorResponse(ctx, statusCode, err)
			ctx.Abort()

			return
		}

		ctx.Set(constant.CtxUserKey, constant.ClientId)
		ctx.Set(constant.ClientId, response.Data.ClientID)

		ctx.Next()
	}
}
