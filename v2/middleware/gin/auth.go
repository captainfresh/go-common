package gin

import (
	"bitbucket.org/captainfresh/go-common/v2/constant"
	"github.com/gin-gonic/gin"
)

// Authenticate : Priority - Cookie -> Client Id and Secret -> Api Key .
func Authenticate(realmHost string) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		_, err := ctx.Cookie(constant.SAccessToken)
		if err != nil {
			VerifyClientIDSecret(realmHost)(ctx)

			return
		}

		VerifySupertokenSession(realmHost)(ctx)
	}
}
