package auth

import (
	"bitbucket.org/captainfresh/go-common/v2/model"
	"context"
	"net/http"
	"time"
)

// Authorizer wraps validations for entities like authorization token, namespace, user, account
type Authorizer interface {
	ValidateNamespaceUser(ctx context.Context) (*model.ValidatedData, error)
	ValidateAccountUser(ctx context.Context) (*model.ValidatedData, error)
	ValidateAccountManager(ctx context.Context) (*model.ValidatedData, error)
	ValidateNamespaceAndUser(ctx context.Context) (*model.ValidatedData, error)
	ValidateNamespace(ctx context.Context) (*model.ValidatedData, error)
	ValidateAuthorizationHeader(ctx context.Context) (*model.ValidatedData, error)
}

type authorizer struct {
	client   http.Client
	amsUrl   string
	realmUrl string
}

func NewAuthorizer(amsUrl, realmUrl string) Authorizer {
	return &authorizer{
		client:   http.Client{Timeout: time.Second * httpClientTimeout},
		amsUrl:   amsUrl,
		realmUrl: realmUrl,
	}
}
