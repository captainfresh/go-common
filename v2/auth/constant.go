package auth

import "time"

const (
	validateNamespaceUserEndpoint    = "/auth/verify-ns-user"
	validateAccountUserEndpoint      = "/auth/verify-account-user"
	validateAccountManagerEndpoint   = "/auth/verify-account-manager"
	validateNamespaceAndUserEndpoint = "/auth/v2/verify-namespace-and-user"
	validateNamespaceEndpoint        = "/auth/v2/verify-namespace"

	verifierAutoRefreshInterval = time.Hour * 12
	httpClientTimeout           = 5
)

// auth claims
const (
	nameKey    = "name"
	emailKey   = "email"
	pictureKey = "picture"
	userIdKey  = "sub"
)
