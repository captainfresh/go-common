package auth

import (
	"bitbucket.org/captainfresh/go-common/v2/clients"
	"bitbucket.org/captainfresh/go-common/v2/constant"
	"bitbucket.org/captainfresh/go-common/v2/errors"
	"bitbucket.org/captainfresh/go-common/v2/model"
	"bitbucket.org/captainfresh/go-common/v2/utility"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"path"
)

// ValidateAuthorizationHeader will call realm to verify the Authorization cookie passed in context.
// cookie must contain sAccessToken and sIdRefreshToken. It returns the user id received from realm.
func (auth *authorizer) ValidateAuthorizationHeader(ctx context.Context) (*model.ValidatedData, error) {
	realmClient := clients.NewRealmClient(auth.realmUrl)

	clientID, err := utility.GetClientId(ctx)
	if err == nil && len(clientID) != 0 {
		clientSecret, err := utility.GetClientSecret(ctx)
		if err != nil {
			return nil, errors.NewUnauthenticatedError(err.Error())
		}

		statusCode, _, err := realmClient.VerifyClientIDSecret(clientID, clientSecret)
		if err != nil {
			return nil, errors.NewUnauthenticatedError(err.Error())
		} else if statusCode != 200 {
			msg := fmt.Sprintf("failed to verify clientID: %s, secret: ***masked***, error: %d\n", clientID, statusCode)
			log.Printf(msg)
			return nil, errors.NewUnauthenticatedError("Forbidden")
		}

		user, err := utility.GetUser(ctx)
		if err != nil {
			user = clientID
		}

		return &model.ValidatedData{User: &model.ValidatedUser{ID: user}}, nil
	}

	cookie, err := utility.GetAuthorizationCookie(ctx)
	if err != nil {
		return nil, err
	}

	_, response, err := realmClient.SessionInfo(cookie)
	if err != nil {
		return nil, errors.NewUnauthenticatedError(err.Error())
	}

	validatedUser := model.ValidatedUser{
		ID: response.Data.UserID,
	}
	return &model.ValidatedData{User: &validatedUser}, nil
}

// ValidateNamespaceUser validates user_id and namespace provided in the context.
// It verifies user belongs to that namespace, by checking {user_id: ctx.user_id, type=ctx.namespace}
// mapping exists in user_mappings table in account-manager
func (auth *authorizer) ValidateNamespaceUser(ctx context.Context) (*model.ValidatedData, error) {
	keys := []string{constant.CtxUserID, constant.CtxNamespace, constant.HeaderCookie, constant.HeaderKeyClientID, constant.HeaderKeyClientSecret}
	return auth.validateContext(ctx, validateNamespaceUserEndpoint, keys)
}

// ValidateAccountUser validates user_id and account_id provided in the context.
// It verifies user belongs to that account, by checking {user_id: ctx.user_id, type: ACCOUNT, type_ref: ctx.account_id}
// mapping exists in user_mappings table in account-manager
func (auth *authorizer) ValidateAccountUser(ctx context.Context) (*model.ValidatedData, error) {
	keys := []string{constant.CtxUserID, constant.CtxNamespace, constant.CtxAccountID, constant.HeaderCookie, constant.HeaderKeyClientID, constant.HeaderKeyClientSecret}
	return auth.validateContext(ctx, validateAccountUserEndpoint, keys)
}

// ValidateAccountManager validates user_id and account_id provided in the context.
// It verifies user is account manager, by checking {user_id: ctx.user_id, type: ACCOUNT, type_ref: ctx.account_id, role: OWNER/ADMIN}
// mapping exists in user_mappings table in account-manager
func (auth *authorizer) ValidateAccountManager(ctx context.Context) (*model.ValidatedData, error) {
	keys := []string{constant.CtxUserID, constant.CtxNamespace, constant.CtxAccountID, constant.CtxAuthHeader, constant.HeaderKeyClientID, constant.HeaderKeyClientSecret}
	return auth.validateContext(ctx, validateAccountManagerEndpoint, keys)
}

// ValidateNamespaceAndUser validates user_id and namespace provided in the context.
// It verifies {code: ctx.namespace} exists in namespaces table and {user_id: ctx.user_id, status: ACTIVE} mapping
// exists in user_mappings table in account-manager
func (auth *authorizer) ValidateNamespaceAndUser(ctx context.Context) (*model.ValidatedData, error) {
	keys := []string{constant.CtxUserID, constant.CtxNamespace, constant.HeaderCookie, constant.HeaderKeyClientID, constant.HeaderKeyClientSecret}
	return auth.validateContext(ctx, validateNamespaceAndUserEndpoint, keys)
}

// ValidateNamespace validates namespace provided in the context.
// It verifies {code: ctx.namespace} exists in namespaces table in account-manager
func (auth *authorizer) ValidateNamespace(ctx context.Context) (*model.ValidatedData, error) {
	keys := []string{constant.CtxNamespace, constant.HeaderCookie, constant.HeaderKeyClientID, constant.HeaderKeyClientSecret}
	return auth.validateContext(ctx, validateNamespaceEndpoint, keys)
}

//--------------------------------------------HELPER FUNCTIONS-----------------------------------------------------//

func (auth *authorizer) validateContext(ctx context.Context, endpoint string, keys []string) (resp *model.ValidatedData, err error) {
	headers := make(map[string]string)
	for _, key := range keys {
		headers[key], _ = auth.getStrValueFromCtx(ctx, key)
	}

	if err = auth.makeRequest(endpoint, headers, &resp); err != nil {
		log.Printf("http request to %s failed with error: %s\n", endpoint, err.Error())
		return
	}
	return
}

// getValidatedUser get user details from parsed token and return it
func (*authorizer) getValidatedUser(claims map[string]interface{}) *model.ValidatedUser {
	user := &model.ValidatedUser{}
	if name, ok := claims[nameKey]; !ok {
		log.Println("failed to get name key from parsed token")
	} else {
		user.Name = name.(string)
	}
	if userId, ok := claims[userIdKey]; !ok {
		log.Println("failed to get userId key from parsed token")
	} else {
		user.ID = userId.(string)
	}
	if email, ok := claims[emailKey]; ok {
		user.Email = email.(string)
	}
	if picture, ok := claims[pictureKey]; ok {
		user.Picture = picture.(string)
	}
	return user
}

// getStrValueFromCtx - get key from context in string variable
func (*authorizer) getStrValueFromCtx(ctx context.Context, key string) (string, error) {
	switch key {
	case constant.CtxNamespace:
		return utility.GetNamespace(ctx)
	case constant.CtxUserID:
		return utility.GetUser(ctx)
	case constant.CtxAccountID:
		return utility.GetAccountIDString(ctx)
	case constant.CtxAuthHeader:
		return utility.GetAuthorizationHeader(ctx)
	case constant.HeaderCookie:
		return utility.GetAuthorizationCookie(ctx)
	case constant.HeaderKeyClientID:
		return utility.GetClientId(ctx)
	case constant.HeaderKeyClientSecret:
		return utility.GetClientSecret(ctx)
	case constant.SAccessToken:
		return utility.GetSAccessToken(ctx)
	case constant.SIdRefreshToken:
		return utility.GetSIdRefreshToken(ctx)
	default:
		err := fmt.Errorf("error in AuthorizerImpl::getStrValueFromCtx, case: %s not handled", key)
		log.Println(err)
		return "", err
	}
}

// makeRequest - make http Get request
func (auth *authorizer) makeRequest(urlEndpoint string, headers map[string]string, resp interface{}) (err error) {
	u, err := url.Parse(auth.amsUrl)
	if err != nil {
		log.Printf("parsing url failed with error: %s\n", err.Error())
		return
	}
	u.Path = path.Join(u.Path, urlEndpoint)
	request, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		log.Printf("http request failed with error: %s\n", err.Error())
		return
	}
	for k, v := range headers {
		request.Header.Set(k, v)
	}
	response, err := auth.client.Do(request)
	if err != nil {
		//TODO: Retry on failure
		msg := fmt.Sprintf("%s request failed with error: %s\n", urlEndpoint, err.Error())
		log.Println(msg)
		return err
	}

	defer func() { _ = response.Body.Close() }()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Printf("read response failed with error: %s\n", err.Error())
		return
	}

	if !is2xxCode(response.StatusCode) {
		err := fmt.Errorf("authorization failed with status code %d", response.StatusCode)
		log.Println(err)
		return errors.ForbiddenError(err, string(body))
	}

	return json.Unmarshal(body, &resp)
}

// is2xxCode check if success status code
func is2xxCode(statusCode int) bool {
	return statusCode >= 200 && statusCode < 300
}
