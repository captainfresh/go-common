package egress

import (
	"bytes"
	"encoding/json"
	"github.com/pkg/errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"path"
	"time"
)

const defaultTimeout = time.Second * 5

type RequestParams struct {
	Path        string
	Headers     map[string]string
	QueryParams map[string]string
	Body        interface{}
}

type Client interface {
	Get(req *RequestParams, res interface{}) (int, error)
	Post(req *RequestParams, res interface{}) (int, error)
	Put(req *RequestParams, res interface{}) (int, error)
	Patch(req *RequestParams, res interface{}) (int, error)
	Delete(req *RequestParams, res interface{}) (int, error)
}

type HTTPClient struct {
	c    *http.Client
	host string
}

func NewHTTPClient(host string, timeout time.Duration) Client {
	if timeout == 0 {
		timeout = defaultTimeout
	}

	return &HTTPClient{
		c:    &http.Client{Timeout: timeout},
		host: host,
	}
}

func (client *HTTPClient) Get(req *RequestParams, res interface{}) (int, error) {
	return client.makeRequest(http.MethodGet, req, res)
}

func (client *HTTPClient) Post(req *RequestParams, res interface{}) (int, error) {
	return client.makeRequest(http.MethodPost, req, res)
}

func (client *HTTPClient) Put(req *RequestParams, res interface{}) (int, error) {
	return client.makeRequest(http.MethodPut, req, res)
}

func (client *HTTPClient) Patch(req *RequestParams, res interface{}) (int, error) {
	return client.makeRequest(http.MethodPatch, req, res)
}

func (client *HTTPClient) Delete(req *RequestParams, res interface{}) (int, error) {
	return client.makeRequest(http.MethodDelete, req, res)
}

func (client *HTTPClient) makeRequest(method string, req *RequestParams, res interface{}) (code int, err error) {
	request, err := client.request(method, req)
	if err != nil {
		return
	}

	resp, err := client.c.Do(request)
	if err != nil {
		return
	}

	code = resp.StatusCode

	defer func() { _ = resp.Body.Close() }()

	buffer, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}

	if !Is2xxCode(code) {
		return code, errors.New(string(buffer))
	}

	if res == nil {
		return
	}
	err = json.Unmarshal(buffer, &res)
	if err != nil {
		return
	}

	return
}

func (client *HTTPClient) request(method string, req *RequestParams) (request *http.Request, err error) {
	u, err := url.Parse(client.host)
	if err != nil {
		return
	}

	u.Path = path.Join(u.Path, req.Path)
	buf, err := json.Marshal(req.Body)
	if err != nil {
		return nil, err
	}

	request, err = http.NewRequest(method, u.String(), bytes.NewBuffer(buf))
	if err != nil {
		return
	}

	q := request.URL.Query()
	for k, v := range req.QueryParams {
		q.Set(k, v)
	}

	request.URL.RawQuery = q.Encode()
	for k, v := range req.Headers {
		request.Header.Set(k, v)
	}

	return
}

func Is2xxCode(code int) bool {
	return code >= 200 && code <= 299
}
