package graphql

import (
	"bitbucket.org/captainfresh/go-common/v2/errors"
	"context"
	"github.com/99designs/gqlgen/graphql"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/vektah/gqlparser/v2/gqlerror"
)

func PresentGqlError(srv *handler.Server) {
	srv.SetErrorPresenter(func(ctx context.Context, e error) *gqlerror.Error {
		err := graphql.DefaultErrorPresenter(ctx, e)

		myOriginalError := err.Unwrap()
		var baseErr errors.BaseErr
		switch myOriginalError.(type) {
		case errors.BaseErr:
			baseErr = myOriginalError.(errors.BaseErr)
		default:
			baseErr = errors.NewInternalServerError(e.Error())
		}

		err.Extensions = map[string]interface{}{
			"code":    baseErr.Code(),
			"message": baseErr.Error(),
			// TODO: fix Stacked Trace
			//"details": myErr.StackedTrace(),
		}
		err.Message = ""
		return err
	})
}
