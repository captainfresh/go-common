package tracer

import (
	"context"
	"fmt"
	"strconv"

	"github.com/99designs/gqlgen/graphql"
	"github.com/99designs/gqlgen/graphql/handler/extension"
	"github.com/newrelic/go-agent/v3/integrations/nrpkgerrors"
	"github.com/newrelic/go-agent/v3/newrelic"
)

type (
	NewRelicTracer struct {
		ComplexityExtensionName string
	}
)

func NewRelic() NewRelicTracer {
	return NewRelicTracer{}
}

var _ interface {
	graphql.HandlerExtension
	graphql.OperationInterceptor
	graphql.FieldInterceptor
	graphql.RootFieldInterceptor
} = NewRelicTracer{}

func (a NewRelicTracer) ExtensionName() string {
	return "NewRelic"
}

func (a NewRelicTracer) Validate(schema graphql.ExecutableSchema) error {
	return nil
}

func (a NewRelicTracer) InterceptRootField(ctx context.Context, next graphql.RootResolver) graphql.Marshaler {
	name := fmt.Sprintf("POST /graphql/%s/%s", operationType(ctx), operationName(ctx))

	tx := newrelic.FromContext(ctx)
	tx.SetName(name)

	return next(ctx)
}

func (a NewRelicTracer) InterceptOperation(ctx context.Context, next graphql.OperationHandler) graphql.ResponseHandler {
	tx := newrelic.FromContext(ctx)

	span := tx.StartSegment(operationName(ctx))
	defer span.End()

	oc := graphql.GetOperationContext(ctx)

	span.AddAttribute("request.query", oc.RawQuery)

	complexityExtension := a.ComplexityExtensionName
	if complexityExtension == "" {
		complexityExtension = "ComplexityLimit" // from extension package
	}

	complexityStats, ok := oc.Stats.GetExtension(complexityExtension).(*extension.ComplexityStats)
	if !ok {
		// complexity extension is not used
		complexityStats = &extension.ComplexityStats{}
	}

	if complexityStats.ComplexityLimit > 0 {
		span.AddAttribute("request.complexityLimit", fmt.Sprintf("%d", complexityStats.ComplexityLimit))
		span.AddAttribute("request.operationComplexity", fmt.Sprintf("%d", complexityStats.Complexity))
	}

	for key, val := range oc.Variables {
		span.AddAttribute(fmt.Sprintf("request.variables.%s", key), fmt.Sprintf("%+v", val))
	}

	return next(ctx)
}

func (a NewRelicTracer) InterceptField(ctx context.Context, next graphql.Resolver) (interface{}, error) {
	fc := graphql.GetFieldContext(ctx)
	tx := newrelic.FromContext(ctx)

	span := tx.StartSegment(fc.Field.ObjectDefinition.Name + "/" + fc.Field.Name)
	defer span.End()

	span.AddAttribute("resolver.path", fc.Path().String())
	span.AddAttribute("resolver.object", fc.Field.ObjectDefinition.Name)
	span.AddAttribute("resolver.field", fc.Field.Name)
	span.AddAttribute("resolver.alias", fc.Field.Alias)

	for _, arg := range fc.Field.Arguments {
		if arg.Value != nil {
			span.AddAttribute(fmt.Sprintf("resolver.args.%s", arg.Name), arg.Value.String())
		}
	}

	resp, err := next(ctx)

	errList := graphql.GetFieldErrors(ctx, fc)

	if len(errList) != 0 {
		tx.NoticeError(nrpkgerrors.Wrap(errList))

		span.AddAttribute("resolver.hasError", strconv.FormatBool(true))
		span.AddAttribute("resolver.errorCount", fmt.Sprintf("%d", len(errList)))

		for idx, err := range errList {
			span.AddAttribute(fmt.Sprintf("resolver.error.%d.message", idx), err.Error())
			span.AddAttribute(fmt.Sprintf("resolver.error.%d.kind", idx), fmt.Sprintf("%T", err))
		}
	}

	return resp, err
}

func operationType(ctx context.Context) string {
	return string(graphql.GetOperationContext(ctx).Operation.Operation)
}

func operationName(ctx context.Context) string {
	requestContext := graphql.GetOperationContext(ctx)

	requestName := "nameless-operation"

	if requestContext.Doc != nil && len(requestContext.Doc.Operations) != 0 {
		op := requestContext.Doc.Operations[0]
		if op.Name != "" {
			requestName = op.Name
		}
	}
	return requestName
}
