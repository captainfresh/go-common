package graphql

import (
	"bitbucket.org/captainfresh/go-common/v2/constant"
	"context"
	"net/http"
)

/*
EnrichContext returns a custom middleware that acts before every application handler.
This middleware initializes context.Context using the http request headers.

This is how to use this middleware:

 handler := graphql.EnrichContext(srv)

where handler is of type http.Handler and srv is of type handler.Server
*/
func EnrichContext(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		for k, v := range r.Header {
			ctx = context.WithValue(ctx, k, v[0])
		}
		ctx = context.WithValue(ctx, constant.CtxNamespace, r.Header.Get(headerNamespace))
		ctx = context.WithValue(ctx, constant.CtxUserID, r.Header.Get(headerUserID))
		ctx = context.WithValue(ctx, constant.CtxAccountID, r.Header.Get(headerAccountID))
		ctx = context.WithValue(ctx, constant.CtxUserKey, constant.CtxUserID)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
