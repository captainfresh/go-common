package graphql

import (
	"bitbucket.org/captainfresh/go-common/v2/auth"
	"context"
	"fmt"
	"github.com/99designs/gqlgen/graphql"
)

type Directive interface {
	VerifyAuthorizationToken() func(ctx context.Context, obj interface{}, next graphql.Resolver) (res interface{}, err error)
	VerifyNamespaceUser() func(ctx context.Context, obj interface{}, next graphql.Resolver) (res interface{}, err error)
	VerifyAccountUser() func(ctx context.Context, obj interface{}, next graphql.Resolver) (res interface{}, err error)
	VerifyAccountManager() func(ctx context.Context, obj interface{}, next graphql.Resolver) (res interface{}, err error)
	VerifyNamespaceAndUser() func(ctx context.Context, obj interface{}, next graphql.Resolver) (res interface{}, err error)
	VerifyNamespace() func(ctx context.Context, obj interface{}, next graphql.Resolver) (res interface{}, err error)
}

type directive struct {
	authorizer auth.Authorizer
}

// NewVerifierDirective returns an instance of directive initialised with authorizer
func NewVerifierDirective(amsURL, realmUrl string) (Directive, error) {
	if len(amsURL) == 0 {
		return nil, fmt.Errorf("not provided AMS Url in config")
	}
	if len(realmUrl) == 0 {
		return nil, fmt.Errorf("not provided realm Url in config")
	}
	return &directive{
		authorizer: auth.NewAuthorizer(amsURL, realmUrl),
	}, nil
}
