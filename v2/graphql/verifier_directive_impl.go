package graphql

import (
	"bitbucket.org/captainfresh/go-common/v2/constant"
	"bitbucket.org/captainfresh/go-common/v2/model"
	"context"
	"fmt"
	"github.com/99designs/gqlgen/graphql"
	"github.com/jinzhu/copier"
	"log"
)

/*
VerifyAuthorizationToken implements a custom operation directive. It will validate the authorization
cookie passed in context.Context.

This is how to use this directive:

The directive function is assigned to the Config object before registering the GraphQL handler.
 gqlConfig.Directives.VerifyAuthorizationToken = graphql.VerifyAuthorizationToken()

To decorate an SDL with token validation rules, directive is preceded by the @ character, like so:
 @verifyAuthorizationToken
This directive can be applied to any mutation or query in your schema.
*/

func (d *directive) VerifyAuthorizationToken() func(ctx context.Context, obj interface{}, next graphql.Resolver) (res interface{}, err error) {
	return validateContext(constant.HeaderCookie, d.authorizer.ValidateAuthorizationHeader)
}

/*
VerifyNamespaceUser implements a custom operation directive. It validates user_id and namespace
provided in context.Context. It verifies user belongs to that namespace, by checking
{user_id: ctx.user_id, type=ctx.namespace} mapping exists in user_mappings table in account-manager

This is how to use this directive:

The directive function is assigned to the Config object before registering the GraphQL handler.
 gqlConfig.Directives.VerifyNamespaceUser = graphql.VerifyNamespaceUser(<ams_url>)

To decorate an SDL with namespace-user validation rules, directive is preceded by the @ character, like so:
 @verifyNamespaceUser
This directive can be applied to any mutation or query in your schema.
*/
func (d *directive) VerifyNamespaceUser() func(ctx context.Context, obj interface{}, next graphql.Resolver) (res interface{}, err error) {
	keys := fmt.Sprintf("%s and %s", constant.CtxNamespace, constant.CtxUserID)
	return validateContext(keys, d.authorizer.ValidateNamespaceUser)
}

/*
VerifyAccountUser implements a custom operation directive. It validates user_id and account_id
provided in context.Context. It verifies user belongs to that account, by checking
{user_id: ctx.user_id, type: ACCOUNT, type_ref: ctx.account_id} mapping exists in user_mappings
table in account-manager

This is how to use this directive:

The directive function is assigned to the Config object before registering the GraphQL handler.
 gqlConfig.Directives.VerifyAccountUser = graphql.VerifyAccountUser(<ams_url>)

To decorate an SDL with account-user validation rules, directive is preceded by the @ character, like so:
 @verifyAccountUser
This directive can be applied to any mutation or query in your schema.
*/
func (d *directive) VerifyAccountUser() func(ctx context.Context, obj interface{}, next graphql.Resolver) (res interface{}, err error) {
	keys := fmt.Sprintf("%s and %s", constant.CtxAccountID, constant.CtxUserID)
	return validateContext(keys, d.authorizer.ValidateAccountUser)
}

/*
VerifyAccountManager implements a custom operation directive. It validates user_id and account_id
provided in context.Context. It verifies user is account manager, by checking {user_id: ctx.user_id, type: ACCOUNT,
type_ref: ctx.account_id, role: OWNER/ADMIN} mapping exists in user_mappings table in account-manager service

This is how to use this directive:

The directive function is assigned to the Config object before registering the GraphQL handler.
 gqlConfig.Directives.VerifyAccountManager = graphql.VerifyAccountManager(<ams_url>)

To decorate an SDL with account-manager validation rules, directive is preceded by the @ character, like so:
 @verifyAccountManager
This directive can be applied to any mutation or query in your schema.
*/
func (d *directive) VerifyAccountManager() func(ctx context.Context, obj interface{}, next graphql.Resolver) (res interface{}, err error) {
	keys := fmt.Sprintf("%s and %s", constant.CtxAccountID, constant.CtxUserID)
	return validateContext(keys, d.authorizer.ValidateAccountManager)
}

/*
VerifyNamespaceAndUser implements a custom operation directive. It validates user_id and namespace
provided in context.Context. It verifies {code: ctx.namespace} exists in namespaces table
and {user_id: ctx.user_id, status: ACTIVE} mapping exists in user_mappings table in account-manager

This is how to use this directive:

The directive function is assigned to the Config object before registering the GraphQL handler.
 gqlConfig.Directives.VerifyNamespaceAndUser = graphql.VerifyNamespaceAndUser(<ams_url>)

To decorate an SDL with namespace and user validation rules, directive is preceded by the @ character, like so:
 @verifyNamespaceAndUser
This directive can be applied to any mutation or query in your schema.
*/
func (d *directive) VerifyNamespaceAndUser() func(ctx context.Context, obj interface{}, next graphql.Resolver) (res interface{}, err error) {
	keys := fmt.Sprintf("%s and %s", constant.CtxNamespace, constant.CtxUserID)
	return validateContext(keys, d.authorizer.ValidateNamespaceAndUser)
}

/*
VerifyNamespace implements a custom operation directive. It validates namespace provided in the http
request headers. It verifies {code: ctx.namespace} exists in namespaces table in account-manager

This is how to use this directive:

The directive function is assigned to the Config object before registering the GraphQL handler.
 gqlConfig.Directives.VerifyNamespace = graphql.VerifyNamespace(<ams_url>)

To decorate an SDL with namespace validation rules, directive is preceded by the @ character, like so:
 @verifyNamespace
This directive can be applied to any mutation or query in your schema.
*/
func (d *directive) VerifyNamespace() func(ctx context.Context, obj interface{}, next graphql.Resolver) (res interface{}, err error) {
	return validateContext(constant.CtxNamespace, d.authorizer.ValidateNamespace)
}

//--------------------------------------------HELPER FUNCTIONS-----------------------------------------------------//

// validateContext will validate the given context-key and update validated data in context
func validateContext(key string, keyValidator func(ctx context.Context) (*model.ValidatedData, error)) func(ctx context.Context, obj interface{}, next graphql.Resolver) (res interface{}, err error) {
	return func(ctx context.Context, obj interface{}, next graphql.Resolver) (res interface{}, err error) {
		resp, err := keyValidator(ctx)
		if err != nil {
			log.Println(fmt.Errorf("%s verification failed, err: %s", key, err.Error()))
			return
		}
		ctx = updateValidatedData(ctx, resp)
		return next(ctx)
	}
}

// updateValidatedData updates validated data in the context
func updateValidatedData(ctx context.Context, data *model.ValidatedData) context.Context {
	validatedData, _ := ctx.Value(constant.CtxValidatedData).(*model.ValidatedData)
	if validatedData == nil {
		validatedData = model.NewValidatedData()
	}
	if data != nil {
		// safe case - if data.user is nil, copy operation will do nothing.
		options := copier.Option{DeepCopy: true, IgnoreEmpty: true}
		if data.User != nil {
			if err := copier.CopyWithOption(&validatedData.User, &data.User, options); err != nil {
				log.Println("failed to copy validated user, errors: ", err.Error())
			}
		}
		if data.Account != nil {
			if err := copier.CopyWithOption(&validatedData.Account, &data.Account, options); err != nil {
				log.Println("failed to copy validated account, errors: ", err.Error())
			}
		}
		if data.Namespace != nil {
			if err := copier.CopyWithOption(&validatedData.Namespace, &data.Namespace, options); err != nil {
				log.Println("failed to copy validated namespace, errors: ", err.Error())
			}
		}
	}
	return context.WithValue(ctx, constant.CtxValidatedData, validatedData)
}
