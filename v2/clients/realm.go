package clients

import (
	"bitbucket.org/captainfresh/go-common/v2/egress"
	"bitbucket.org/captainfresh/go-common/v2/model"
	"bitbucket.org/captainfresh/go-common/v2/utility"
	"time"

	"github.com/pkg/errors"
)

const (
	realmSessionInfoEndpoint   = "/sessioninfo"
	realmClientsVerifyEndpoint = "/api/v1/clients/verify"
)

type (
	RealmClient interface {
		SessionInfo(cookie string) (int, model.SessionInfoResponse, error)
		VerifyClientIDSecret(
			clientID string,
			clientSecret string,
		) (int, model.VerifyClientIDSecretResponse, error)
	}
	realmClient struct {
		HTTPClient   egress.Client
		ClientId     string
		ClientSecret string
	}
)

func NewRealmClient(host string) RealmClient {
	return realmClient{
		HTTPClient: egress.NewHTTPClient(host, 2*time.Second),
	}
}

func (c realmClient) SessionInfo(cookie string) (code int, res model.SessionInfoResponse, err error) {
	req := &egress.RequestParams{
		Path:    realmSessionInfoEndpoint,
		Headers: utility.GetRequiredHeaders(cookie, c.ClientId, c.ClientSecret),
	}
	code, err = c.HTTPClient.Get(req, &res)

	return code, res, err
}

func (c realmClient) VerifyClientIDSecret(
	clientID string,
	clientSecret string,
) (code int, res model.VerifyClientIDSecretResponse, err error) {
	req := &egress.RequestParams{
		Path:    realmClientsVerifyEndpoint,
		Headers: utility.GetRequiredHeaders("", clientID, clientSecret),
		QueryParams: map[string]string{
			"clientId":     clientID,
			"clientSecret": clientSecret,
		},
	}
	code, err = c.HTTPClient.Get(req, &res)
	if err == nil && res.Data.ClientID == "" {
		err = errors.New("forbidden: client not found")
	}

	return code, res, err
}
