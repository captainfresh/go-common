package i18n

import (
	"bitbucket.org/captainfresh/go-common/v2/utility"
	"context"
	"encoding/json"
	"fmt"
	"github.com/nicksnyder/go-i18n/v2/i18n"
	"go.uber.org/zap"
	"golang.org/x/text/language"
	"os"
)

var bundle *i18n.Bundle
var logger *zap.Logger

// Before localising any strings, it is essential to initialise the i18n by invoking the Initi18n method at application
// startup. Not doing so will lead to nil pointer references.

// Initi18n initialises go-i18n bundle, loads json translations files from the provided path and sets default language
// to english. It panics if there is an issue in loading the json file or a non json file is encountered.
func Initi18n(zapLogger *zap.Logger, translationsDirPath string) {
	bundle = i18n.NewBundle(language.English)
	bundle.RegisterUnmarshalFunc("json", json.Unmarshal)
	translationFiles, err := os.ReadDir(translationsDirPath)
	if err != nil {
		panic(err)
	}
	for _, translationFile := range translationFiles {
		bundle.MustLoadMessageFile(translationsDirPath + translationFile.Name())
	}
	logger = zapLogger
}

// localise: Fetches the key from translation files in the provided locale. Data is used to fill the variable data
// mentioned in the translation string. PluralCount is used for managing plurals of the string eg "one", "two", "other"
// etc.
func localise(key TranslationKey, locale string, data map[string]interface{}, pluralCount int) (localizedString string) {
	keyStr := string(key)
	localizer := i18n.NewLocalizer(bundle, locale)
	config := i18n.LocalizeConfig{
		MessageID: keyStr,
		DefaultMessage: &i18n.Message{
			ID:    keyStr,
			Other: "Not found",
		},
		TemplateData: data,
	}
	if pluralCount != 0 {
		config.PluralCount = pluralCount
	}

	localizedString, err := localizer.Localize(&config)
	if err != nil {
		logger.With(zap.Error(err)).Error(fmt.Sprintf("unable to localise string with key %s for locale %s", key, locale))

		return localizedString // default locale string
	}

	return localizedString
}

// Localise wrapper around localise and is used to translate static non-plural strings to provided locale
func Localise(key TranslationKey, locale string) (localizedString string) {
	return localise(key, locale, nil, 0)
}

// LocalisePlural wrapper around localise and is used to translate static plural strings to provided locale
func LocalisePlural(key TranslationKey, locale string, pluralCount int) (localizedString string) {
	return localise(key, locale, nil, pluralCount)
}

// LocaliseTemplate wrapper around localise and is used to translate variable based non-plural strings to provided
// locale
func LocaliseTemplate(
	key TranslationKey,
	locale string,
	data map[string]interface{},
) (localizedString string) {
	return localise(key, locale, data, 0)
}

// LocalisePluralTemplate wrapper around localise and is used to translate variable based plural strings to provided
// locale
func LocalisePluralTemplate(
	key TranslationKey,
	locale string,
	data map[string]interface{},
	pluralCount int,
) (localizedString string) {
	return localise(key, locale, data, pluralCount)
}

// LocaliseWithCtx wrapper around localise and is used to translate static non-plural strings to provided locale by fetching
// locale from context "Accept-Language" key.
func LocaliseWithCtx(ctx context.Context, key TranslationKey) (localizedString string) {
	locale := utility.GetLocaleFromCtx(ctx)
	return localise(key, locale, nil, 0)
}

// LocalisePluralWithCtx wrapper around localise and is used to translate static plural strings to provided locale by fetching
// locale from context "Accept-Language" key.
func LocalisePluralWithCtx(ctx context.Context, key TranslationKey, pluralCount int) (localizedString string) {
	locale := utility.GetLocaleFromCtx(ctx)
	return localise(key, locale, nil, pluralCount)
}

// LocaliseTemplateWithCtx wrapper around localise and is used to translate variable based non-plural strings to provided
// locale by fetching locale from context "Accept-Language" key.
func LocaliseTemplateWithCtx(
	ctx context.Context,
	key TranslationKey,
	data map[string]interface{},
) (localizedString string) {
	locale := utility.GetLocaleFromCtx(ctx)
	return localise(key, locale, data, 0)
}

// LocalisePluralTemplateWithCtx wrapper around localise and is used to translate variable based plural strings to provided
// locale by fetching locale from context "Accept-Language" key.
func LocalisePluralTemplateWithCtx(
	ctx context.Context,
	key TranslationKey,
	data map[string]interface{},
	pluralCount int,
) (localizedString string) {
	locale := utility.GetLocaleFromCtx(ctx)
	return localise(key, locale, data, pluralCount)
}
