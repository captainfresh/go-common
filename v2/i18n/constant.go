package i18n

// TranslationKey enum to define translation string key constants.
type TranslationKey string
