package transaction

import "context"

// Func is the function that will be executed atomically.
type Func func(ctx context.Context) error

// Executor can be used in repositories that uses databases with Transaction supports.
type Executor interface {

	// DoInTransaction ensures that the transaction passed as an argument will be executed atomically.
	// It also accepts an arbitrary number of options that can be used to provide configurations for the transaction.
	DoInTransaction(ctx context.Context, transaction Func, opt ...interface{}) error
}
