# Transaction Executor

## How to use

To understand how to use the transaction executor, Lets create a simple transfer function that will transfer money from one account to another account. 

### Model
Our model contains a balance field. There is a check in database to make sure the balance is always >= 0. if any operation results in -ve value, database will throw error.
```go
// Account model contains account id and the balance.
// Any operation that will make balance <0 will result in error
type Account struct {
	ID      int64
	Balance int64
}
```

### Repository
AccountRepository extends the `transaction.Executor` interface. GormAccountRepository is extending the `transaction.GormExecutor`
```go
type AccountRepository interface {
	transaction.Executor
	UpdateBalance(ctx context.Context, account *model.Account, amount int64) error
}

type GormAccountRepository struct {
	*transaction.GormExecutor
}

func (repository *GormAccountRepository) UpdateBalance(ctx context.Context, account *model.Account, amount int64) error {
	return repository.GetDB(ctx).Model(&account).
		Where("id = ?", account.ID).
		UpdateColumn("balance", gorm.Expr("balance - ?", amount)).Error
}
```

### Service
In the service, we can define a transfer flow where we will put the logic to transfer money from one account to another account. And then we will call it inside the `DoInTransaction` method. to make the transfer flow atomic.

**NOTE: ALWAYS USE THE CONTEXT PASSED AS ARGUMENT INSIDE `transaction.Func`.**
```go
type AccountService struct {
	repository repository.AccountRepository
}

func (service *AccountService) Transfer(ctx context.Context, sender, receiver *model.Account, amount int64) error {
	return service.repository.DoInTransaction(ctx, func(tCtx context.Context) error {
		return service.TransferFlow(tCtx, sender, receiver, amount)
	})
}

func (service *AccountService) TransferFlow(ctx context.Context, sender, receiver *model.Account, amount int64) (err error) {
	err = service.repository.UpdateBalance(ctx, receiver, amount)
	if err != nil {
		return err
	}

	return service.repository.UpdateBalance(ctx, receiver, -amount)
}

```