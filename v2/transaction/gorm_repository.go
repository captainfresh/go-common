package transaction

import (
	"context"
	"database/sql"
	"fmt"
	"gorm.io/gorm"
)

const (
	dbKey = "gorm-db"
)

// GormExecutor is the Gorm implementation of Executor.
type GormExecutor struct {
	db *gorm.DB
}

func NewGormExecutor(db *gorm.DB) *GormExecutor {
	return &GormExecutor{db: db}
}

// DoInTransaction is the GormExecutor's implementation of Executor interface.
// It takes a context and a transaction function and transaction options. After starting the transaction,
// it sets the context with *gorm.DB so that it can be used in the repository layer for performing operations.
func (repository *GormExecutor) DoInTransaction(ctx context.Context, tFunc Func, opts ...interface{}) error {
	if ctx == nil {
		ctx = context.Background()
	}
	var sqlOpts = make([]*sql.TxOptions, 0, len(opts))
	for _, opt := range opts {
		sqlOpt, ok := opt.(*sql.TxOptions)
		if !ok {
			return fmt.Errorf("invalid option %v expected option to be of type *sql.TxOptions", opt)
		}
		sqlOpts = append(sqlOpts, sqlOpt)
	}
	return repository.GetDB(ctx).Transaction(func(tx *gorm.DB) error {
		ctxWithDB := context.WithValue(ctx, dbKey, tx)
		return tFunc(ctxWithDB)
	}, sqlOpts...)
}

// GetDB is the getter for db definition. It takes a context and if the context already
// have a db definition (set by any transaction), it will be returned.
// Else the GormExecutor's db will be returned
func (repository *GormExecutor) GetDB(ctx context.Context) *gorm.DB {
	value := ctx.Value(dbKey)
	if value == nil {
		return repository.db.WithContext(ctx)
	}

	db, _ := value.(*gorm.DB)
	return db
}
