package s3

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"log"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

type Link struct {
	Url    string    `json:"url,omitempty" gorm:"-"`
	Expiry time.Time `json:"expiry,omitempty" gorm:"-"`
}

func NewLink(url string, e time.Time) *Link {
	return &Link{
		Url:    url,
		Expiry: e,
	}
}

func AwsS3(region string) (*s3.S3, error) {
	// region := config.ViperConfig.GetString(S3_REGION)
	awsConfig := &aws.Config{}
	if region != "" {
		awsConfig.Region = aws.String(region)
	}

	sess, err := session.NewSession(awsConfig)
	if err != nil {
		fmt.Println(err.Error())
		return nil, errors.New("unable to create aws sessions " + err.Error())
	}

	svc := s3.New(sess)
	return svc, nil
}

func GetUploadLink(region string, bucket string, key string, dur time.Duration) (*Link, error) {
	svc, err := AwsS3(region)
	if err != nil {
		return nil, err
	}

	req, _ := svc.PutObjectRequest(&s3.PutObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	})

	expiry := time.Now().Add(dur)
	urlStr, err := req.Presign(dur)

	if err != nil {
		log.Println("Failed to sign request", err)
		return nil, err
	}

	return NewLink(urlStr, expiry), nil
}

func CheckFileExists(region string, bucket string, key string) (bool, error) {
	svc, err := AwsS3(region)
	if err != nil {
		return false, err
	}

	_, err = svc.HeadObject(&s3.HeadObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	})

	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case "NotFound": // s3.ErrCodeNoSuchKey does not work, aws is missing this error code so we hardwire a string
				return false, nil
			default:
				return false, err
			}
		}
	}
	return true, nil
}

func GetDownloadLink(region string, bucket string, key string, filename string, dur time.Duration, download bool) (*Link, error) {
	svc, err := AwsS3(region)
	if err != nil {
		return nil, err
	}

	exists, err := CheckFileExists(region, bucket, key)
	if err != nil {
		return nil, err
	}

	if !exists {
		return nil, errors.New("no file available in s3")
	}
	reqInp := &s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	}

	if download {
		reqInp.ResponseContentDisposition = aws.String(fmt.Sprintf("attachment;filename=%s", filename))
	} else {
		reqInp.ResponseContentDisposition = aws.String(fmt.Sprintf("inline;filename=%s", filename))
	}

	req, _ := svc.GetObjectRequest(reqInp)

	expiry := time.Now().Add(dur)
	urlStr, err := req.Presign(dur)

	if err != nil {
		log.Println("Failed to sign request", err)
		return nil, err
	}

	return NewLink(urlStr, expiry), nil
}

// GetFileStream This method returns file content as byte stream from s3 storage.
func GetFileStream(region string, bucket string, key string) ([]byte, error) {
	svc, err := AwsS3(region)
	if err != nil {
		return nil, err
	}
	result, err := svc.GetObject(&s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	})
	if err != nil {
		return []byte{}, err
	}

	var buf bytes.Buffer
	_, err = io.Copy(&buf, result.Body)
	if err != nil {
		return []byte{}, err
	}
	return buf.Bytes(), nil
}
