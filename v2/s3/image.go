package s3

import (
	"errors"
	"fmt"
	"time"

	"github.com/segmentio/ksuid"
)

type S3Image struct {
	S3Folder string `json:"s3Folder"` //S3 folder is a bucket with folder <bucket></folder>
	Key      string `json:"key"`
	Url      string `json:"url"`
	CDN      string `json:"cdn"` //In future use cdnType to derive the Host and
}

type S3ImageUpload struct {
	*S3Image
	UploadLink *Link `json:"uploadLink"`
}

type CDNS3Folder struct {
	CDNType        string        `json:"cdnType"`
	CDNHost        string        `json:"cdnHost"`
	CDNPathPrefix  string        `json:"cdnPathPrefix"`
	S3Bucket       string        `json:"s3Bucket"`
	S3Region       string        `json:"s3Region"`
	S3Folder       string        `json:"s3Folder"`
	S3UploadExpiry time.Duration `json:"s3UploadExpiry"`
}

func (c *CDNS3Folder) CDN() string {
	return fmt.Sprintf("%s:%s%s", c.CDNType, c.CDNHost, c.CDNPathPrefix)
}

func (c *CDNS3Folder) S3Path(key string) string {
	return fmt.Sprintf("%s/%s", c.S3Folder, key)
}

func (c *CDNS3Folder) DownloadUrl(key string) string {
	return fmt.Sprintf("%s%s/%s", c.CDNHost, c.CDNPathPrefix, key)
}

func CreateS3Image(conf CDNS3Folder, key string, ext string) (S3ImageUpload, error) {
	uid, err := ksuid.NewRandom()
	if err != nil {
		return S3ImageUpload{}, err
	}
	key = key + "/" + uid.String()
	if 0 < len(ext) {
		key = key + "." + ext
	}
	path := conf.S3Path(key)
	exist, err := CheckFileExists(conf.S3Region, conf.S3Bucket, path)
	if err != nil {
		return S3ImageUpload{}, err
	}
	if exist {
		return S3ImageUpload{}, errors.New("this supposed to not happen, try again")
	}
	link, err := GetUploadLink(conf.S3Region, conf.S3Bucket, path, conf.S3UploadExpiry)
	if err != nil {
		return S3ImageUpload{}, err
	}

	imag := S3ImageUpload{
		S3Image: &S3Image{
			S3Folder: conf.S3Bucket + conf.S3Folder,
			Key:      key,
			CDN:      conf.CDN(),
			Url:      conf.DownloadUrl(key),
		},
		UploadLink: link,
	}

	return imag, nil
}
