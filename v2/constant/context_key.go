package constant

const (
	CtxAuthHeader = "Authorization"
	CtxNamespace  = "saas-namespace"
	CtxUserID     = "saas-user-id"
	CtxAccountID  = "saas-account-id"
	CtxOrgID      = "saas-organisation-id"

	CtxNamespaceID = "saas-namespace-id"

	CtxUserKey = "CtxUserKey"

	CtxValidatedData        = "validated-data"
	CtxHeaderAcceptLanguage = "Accept-Language"
)
