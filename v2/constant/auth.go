package constant

const (
	SAccessToken    = "sAccessToken"
	SIdRefreshToken = "sIdRefreshToken"
	HeaderCookie    = "Cookie"
	UserID          = "User-Id"
	ClientId        = "Client-Id"
)
