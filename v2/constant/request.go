package constant

const (
	HeaderKeyClientID        = "Client-Id"
	HeaderKeyClientSecret    = "Client-Secret"
	HeaderKeySessionHandle   = "Session-Handle"
	HeaderKeyContentType     = "Content-Type"
	HeaderValContentTypeJson = "application/json"
)
