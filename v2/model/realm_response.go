package model

type SessionInfoResponse struct {
	Status *Status         `json:"status"`
	Data   SessionInfoData `json:"data"`
}

type SessionInfoData struct {
	UserID        string `json:"userId"`
	Name          string `json:"name"`
	Email         string `json:"email"`
	SessionHandle string `json:"sessionHandle"`
}

type VerifyClientIDSecretResponse struct {
	Status *Status                  `json:"status"`
	Data   VerifyClientIDSecretData `json:"data"`
}

type VerifyClientIDSecretData struct {
	ClientID string `json:"clientId"`
	Name     string `json:"name"`
}
