package model

// ValidatedNamespace wraps details of a validated namespace like id, code, name
type ValidatedNamespace struct {
	ID   int64  `json:"id,omitempty"`
	Code string `json:"code,omitempty"`
	Name string `json:"name,omitempty"`
}

func (vn *ValidatedNamespace) GetNamespaceID() (*int64, error) {
	// namespace ID can not be zero in valid namespace
	if vn.ID == 0 {
		return nil, validationError("ValidatedNamespace::ID")
	}

	return &vn.ID, nil
}

func (vn *ValidatedNamespace) GetNamespaceCode() (*string, error) {
	// namespace code can not be empty string in valid namespace
	if len(vn.Code) == 0 {
		return nil, validationError("ValidatedNamespace::Code")
	}

	return &vn.Code, nil
}
