package model

import (
	"bitbucket.org/captainfresh/go-common/v2/constant"
	"bitbucket.org/captainfresh/go-common/v2/errors"
	"fmt"
)

// ValidatedData wraps validated entities like user, account, namespace
type ValidatedData struct {
	User      *ValidatedUser      `json:"user,omitempty"`
	Namespace *ValidatedNamespace `json:"namespace,omitempty"`
	Account   *ValidatedAccount   `json:"account,omitempty"`
}

func (vd *ValidatedData) GetAccount() (*ValidatedAccount, error) {
	if vd.Account == nil {
		return nil, validationError("ValidatedAccount")
	}

	return vd.Account, nil
}

func (vd *ValidatedData) GetNamespace() (*ValidatedNamespace, error) {
	if vd.Namespace == nil {
		return nil, validationError("ValidatedNamespace")
	}

	return vd.Namespace, nil
}

func (vd *ValidatedData) GetUser() (*ValidatedUser, error) {
	if vd.User == nil {
		return nil, validationError("ValidatedUser")
	}

	return vd.User, nil
}

// NewValidatedData returns an empty instance of ValidatedData struct
func NewValidatedData() *ValidatedData {
	return &ValidatedData{
		Namespace: &ValidatedNamespace{},
		User:      &ValidatedUser{},
		Account: &ValidatedAccount{
			Metadata: &ValidatedAccountMetadata{
				FeatureConfig:   &AccountFeature{},
				AccountDefaults: &AccountDefaults{},
			},
		},
	}
}

// ---------------------------- HELPER FUNCTIONS ------------------------------//

func validationError(key string) error {
	return errors.NewRecordNotFoundError(fmt.Sprintf(
		"missing %s in context::%s", key, constant.CtxValidatedData))
}
