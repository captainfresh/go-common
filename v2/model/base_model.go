package model

import (
	"bitbucket.org/captainfresh/go-common/v2/constant"
	"gorm.io/gorm"
	"time"
)

type BaseModel struct {
	CreatedAt time.Time      `gorm:"column:created_at" json:"createdAt"`
	UpdatedAt time.Time      `gorm:"column:updated_at" json:"updatedAt"`
	DeletedAt gorm.DeletedAt `gorm:"column:deleted_at" json:"-"`
	CreatedBy string         `gorm:"column:created_by" json:"createdBy"`
	UpdatedBy string         `gorm:"column:updated_by" json:"updatedBy"`
	DeletedBy string         `gorm:"column:deleted_by" json:"deletedBy"`
	IsActive  *bool          `gorm:"column:is_active;default:true" json:"isActive"`
}

func (u *BaseModel) BeforeCreate(tx *gorm.DB) (err error) {
	ctx := tx.Statement.Context
	ctxUserId := ctx.Value(constant.CtxUserKey)
	if ctxUserId == nil {
		return
	}
	key := ctxUserId.(string)
	if key == "" {
		return
	}
	user := ctx.Value(key).(string)
	if user != "" {
		u.CreatedBy = user
	}
	return
}

func (u *BaseModel) BeforeUpdate(tx *gorm.DB) (err error) {
	ctx := tx.Statement.Context
	ctxUserId := ctx.Value(constant.CtxUserKey)
	if ctxUserId == nil {
		return
	}
	key := ctxUserId.(string)
	if key == "" {
		return
	}
	user := ctx.Value(key).(string)
	if user != "" {
		u.UpdatedBy = user
	}
	return
}
func (u *BaseModel) BeforeDelete(tx *gorm.DB) (err error) {
	ctx := tx.Statement.Context
	ctxUserId := ctx.Value(constant.CtxUserKey)
	if ctxUserId == nil {
		return
	}
	key := ctxUserId.(string)
	if key == "" {
		return
	}
	user := ctx.Value(key).(string)
	if user != "" {
		u.DeletedBy = user
	}
	return
}
