package model

type Status struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	Detail  string `json:"detail"`
}

type BaseResponse struct {
	Error *Status     `json:"error"`
	Data  interface{} `json:"data"`
}
