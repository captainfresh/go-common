package model

// ValidatedAccount wraps details of a validated account like id, name, feature details
type ValidatedAccount struct {
	ID       int64                     `json:"id,omitempty"`
	Name     string                    `json:"name,omitempty"`
	Metadata *ValidatedAccountMetadata `json:"metadata,omitempty"`
}

func (va *ValidatedAccount) GetAccountID() (*int64, error) {
	// account ID can not be zero in valid account
	if va.ID == 0 {
		return nil, validationError("ValidatedAccount::ID")
	}

	return &va.ID, nil
}

func (va *ValidatedAccount) GetAccountName() (*string, error) {
	// account name can not be empty string in valid account
	if len(va.Name) == 0 {
		return nil, validationError("ValidatedAccount::Name")
	}

	return &va.Name, nil
}

func (va *ValidatedAccount) GetAccountMetadata() (*ValidatedAccountMetadata, error) {
	// account metadata can not be empty in valid and active account
	if va.Metadata == nil {
		return nil, validationError("ValidatedAccount::Metadata")
	}

	return va.Metadata, nil
}

// ValidatedAccountMetadata wraps metadata of a validated account like feature details
type ValidatedAccountMetadata struct {
	FeatureConfig   *AccountFeature  `json:"featureConfig,omitempty"`
	AccountDefaults *AccountDefaults `json:"accountDefault,omitempty"`
}

func (vam *ValidatedAccountMetadata) GetAccountFeature() (*AccountFeature, error) {
	if vam.FeatureConfig == nil {
		return nil, validationError("ValidatedAccount::Metadata::AccountFeature")
	}

	// check to ensure account is active and has at least one feature enabled
	if vam.FeatureConfig.Buying == false && vam.FeatureConfig.Selling == false {
		return nil, validationError("ValidatedAccount::Metadata::AccountFeature")
	}

	return vam.FeatureConfig, nil
}

func (vam *ValidatedAccountMetadata) GetAccountDefaults() (*AccountDefaults, error) {
	if vam.AccountDefaults == nil {
		return nil, validationError("ValidatedAccount::Metadata::AccountDefaults")
	}

	return vam.AccountDefaults, nil
}

// AccountFeature wraps features available for an account like buying, selling
type AccountFeature struct {
	Buying  bool `json:"buying,omitempty"`
	Selling bool `json:"selling,omitempty"`
}

// AccountDefaults wraps defaults set for an account, currently account can have default shipping and billing address
type AccountDefaults struct {
	BillingAddress  *int64 `json:"billingAddress,omitempty"`
	ShippingAddress *int64 `json:"shippingAddress,omitempty"`
}

func (vam *AccountDefaults) GetAccountDefaultShippingAddress() (*int64, error) {
	if vam.ShippingAddress == nil {
		return nil, validationError("ValidatedAccount::Metadata::AccountDefaults::ShippingAddress")
	}

	return vam.ShippingAddress, nil
}

func (vam *AccountDefaults) GetAccountDefaultBillingAddress() (*int64, error) {
	if vam.BillingAddress == nil {
		return nil, validationError("ValidatedAccount::Metadata::AccountDefaults::BillingAddress")
	}

	return vam.BillingAddress, nil
}
