package model

// ValidatedUser wraps details of a validated user like id, name, email, picture
type ValidatedUser struct {
	ID      string `json:"id,omitempty"`
	Name    string `json:"name,omitempty"`
	Email   string `json:"email,omitempty"`
	Picture string `json:"picture,omitempty"`
}

func (vu *ValidatedUser) GetUserID() (*string, error) {
	if len(vu.ID) == 0 {
		return nil, validationError("ValidatedUser::ID")
	}

	return &vu.ID, nil
}
