package account_permission

import "bitbucket.org/captainfresh/go-common/dto"

type AccountPermissionPolicyInterface interface {
	Validate(permissionsAvailable *dto.AccountPermissions) bool
}
