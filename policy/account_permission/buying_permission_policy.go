package account_permission

import (
	"bitbucket.org/captainfresh/go-common/constant"
	"bitbucket.org/captainfresh/go-common/dto"
	"sync"
)

type BuyingPermissionPolicy struct {
	AccountPermissionPolicy
}

var buyingPermissionPolicyOnce sync.Once

var buyingPermissionPolicyInstance *BuyingPermissionPolicy

func NewBuyingPermissionPolicy() AccountPermissionPolicyInterface {
	buyingPermissionPolicyOnce.Do(func() {
		buyingPermissionPolicyInstance = &BuyingPermissionPolicy{
			AccountPermissionPolicy: AccountPermissionPolicy{},
		}
	})
	return buyingPermissionPolicyInstance
}

func (m *BuyingPermissionPolicy) Validate(permissionsAvailable *dto.AccountPermissions) bool {
	return permissionsAvailable.Permissions[constant.Buying]
}
