package account_permission

import (
	"bitbucket.org/captainfresh/go-common/constant"
	"bitbucket.org/captainfresh/go-common/dto"
	"sync"
)

type SellingPermissionPolicy struct {
	AccountPermissionPolicy
}

var sellingPermissionPolicyOnce sync.Once

var sellingPermissionPolicyInstance *SellingPermissionPolicy

func NewSellingPermissionPolicy() AccountPermissionPolicyInterface {
	sellingPermissionPolicyOnce.Do(func() {
		sellingPermissionPolicyInstance = &SellingPermissionPolicy{
			AccountPermissionPolicy: AccountPermissionPolicy{},
		}
	})
	return sellingPermissionPolicyInstance
}

func (m *SellingPermissionPolicy) Validate(permissionsAvailable *dto.AccountPermissions) bool {
	return permissionsAvailable.Permissions[constant.Selling]
}
