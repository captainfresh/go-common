package account_permission

import (
	"bitbucket.org/captainfresh/go-common/constant"
	"bitbucket.org/captainfresh/go-common/dto"
	"sync"
)

type BuyingOrSellingPermissionPolicy struct {
	AccountPermissionPolicy
}

var buyingOrSellingPermissionPolicyOnce sync.Once

var buyingOrSellingPermissionPolicyInstance *BuyingOrSellingPermissionPolicy

func NewBuyingOrSellingPermissionPolicy() AccountPermissionPolicyInterface {
	buyingOrSellingPermissionPolicyOnce.Do(func() {
		buyingOrSellingPermissionPolicyInstance = &BuyingOrSellingPermissionPolicy{
			AccountPermissionPolicy: AccountPermissionPolicy{},
		}
	})
	return buyingOrSellingPermissionPolicyInstance
}

func (m *BuyingOrSellingPermissionPolicy) Validate(permissionsAvailable *dto.AccountPermissions) bool {
	return permissionsAvailable.Permissions[constant.Buying] || permissionsAvailable.Permissions[constant.Selling]
}
