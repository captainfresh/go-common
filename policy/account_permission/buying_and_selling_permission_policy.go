package account_permission

import (
	"bitbucket.org/captainfresh/go-common/constant"
	"bitbucket.org/captainfresh/go-common/dto"
	"sync"
)

type BuyingAndSellingPermissionPolicy struct {
	AccountPermissionPolicy
}

var buyingAndSellingPermissionPolicyOnce sync.Once

var buyingAndSellingPermissionPolicyInstance *BuyingAndSellingPermissionPolicy

func NewBuyingAndSellingPermissionPolicy() AccountPermissionPolicyInterface {
	buyingAndSellingPermissionPolicyOnce.Do(func() {
		buyingAndSellingPermissionPolicyInstance = &BuyingAndSellingPermissionPolicy{
			AccountPermissionPolicy: AccountPermissionPolicy{},
		}
	})
	return buyingAndSellingPermissionPolicyInstance
}

func (m *BuyingAndSellingPermissionPolicy) Validate(permissionsAvailable *dto.AccountPermissions) bool {
	return permissionsAvailable.Permissions[constant.Buying] && permissionsAvailable.Permissions[constant.Selling]
}
