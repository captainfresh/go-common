package account_permission

import (
	"bitbucket.org/captainfresh/go-common/constant/account_permission"
	"bitbucket.org/captainfresh/go-common/errors"
)

func GetAccountPermissionPolicy(permissionType account_permission.AccountPermissionRequired) (AccountPermissionPolicyInterface, errors.BaseErr) {
	switch permissionType {
	case account_permission.Buying:
		return NewBuyingPermissionPolicy(), nil
	case account_permission.Selling:
		return NewSellingPermissionPolicy(), nil
	case account_permission.BuyingAndSelling:
		return NewBuyingAndSellingPermissionPolicy(), nil
	case account_permission.BuyingOrSelling:
		return NewBuyingOrSellingPermissionPolicy(), nil
	}
	return nil, errors.New("Wrong permission type passed")
}
