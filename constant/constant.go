package constant

type PermissionType string

const (
	Buying  PermissionType = "buying"
	Selling PermissionType = "selling"
)
