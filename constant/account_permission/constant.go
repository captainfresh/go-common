package account_permission

type AccountPermissionRequired string

const (
	Buying           AccountPermissionRequired = "Buying"
	Selling          AccountPermissionRequired = "Selling"
	BuyingOrSelling  AccountPermissionRequired = "BuyingOrSelling"
	BuyingAndSelling AccountPermissionRequired = "BuyingAndSelling"
)
