package error_handling

import (
	"bitbucket.org/captainfresh/go-common/errors"
	"context"
	goError "errors"
	"github.com/99designs/gqlgen/graphql"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/vektah/gqlparser/v2/gqlerror"
)

func PresentGqlError(srv *handler.Server) {

	srv.SetErrorPresenter(func(ctx context.Context, e error) *gqlerror.Error {
		err := graphql.DefaultErrorPresenter(ctx, e)

		var myErr errors.BaseErr
		if goError.As(e, &myErr) {
			switch myErr.(type) {
			case NoRecordFoundErr:
				err.Message = "Incorrect query request parameters or nothing found in db"
				err.Extensions = map[string]interface{}{
					"Status":  400,
					"Title":   "No record found!",
					"Details": err.Error(),
				}
			case NoRowsAffectedErr:
				err.Message = "Error in mutation request"
				err.Extensions = map[string]interface{}{
					"Status":  400,
					"Title":   "Incorrect mutation parameters",
					"Details": err.Error(),
				}
			case ValidationErr:
				err.Message = "Incorrect bearer token/account_id/password"
				err.Extensions = map[string]interface{}{
					"Status":  401,
					"Title":   "Error in validation of credentials",
					"Details": err.Error(),
				}
			default:
				err.Message = "We couldn't process your request this moment. Sorry for the inconvenience!"
				err.Extensions = map[string]interface{}{
					"Status":  500,
					"Title":   "Service error!",
					"Details": err.Error(),
				}
			}

		} else {
			switch myErr.(type) {
			case NoRecordFoundErr:
				err.Message = "Incorrect query request parameters or nothing found in db"
				err.Extensions = map[string]interface{}{
					"Status":  400,
					"Title":   "No record found!",
					"Details": err.Error(),
				}
			case NoRowsAffectedErr:
				err.Message = "Incorrect mutation parameters"
				err.Extensions = map[string]interface{}{
					"Status":  400,
					"Title":   "Error in mutation request",
					"Details": err.Error(),
				}
			case ValidationErr:
				err.Message = "Incorrect bearer token/account_id/password" + err.Error()
				err.Extensions = map[string]interface{}{
					"Status":  401,
					"Title":   "Error in validation of credentials",
					"Details": err.Error(),
				}
			default:
				err.Message = "We couldn't process your request this moment. Sorry for the inconvenience! " + err.Error()
				err.Extensions = map[string]interface{}{
					"Status":  500,
					"Title":   "Service error!",
					"Details": err.Error(),
				}
			}
		}

		return err
	})
}

func GqlPanicHandler(srv *handler.Server) {
	srv.SetRecoverFunc(func(ctx context.Context, err interface{}) error {
		//TODO thread pool executor
		graphql.AddError(context.Background(), &gqlerror.Error{
			Message: err.(string),
		})
		return gqlerror.Errorf("Internal server error!")
	})
}
