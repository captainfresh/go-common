package error_handling

import "bitbucket.org/captainfresh/go-common/errors"

type NoRecordFoundErr struct {
	errors.BaseErr
}

type ValidationErr struct {
	errors.BaseErr
}

type NoRowsAffectedErr struct {
	errors.BaseErr
}

type UnauthorizedErr struct {
	errors.BaseErr
}

func NewNoRowsAffectedErr(err errors.BaseErr, msg string) errors.BaseErr {
	return &NoRowsAffectedErr{errors.Append(err, "NoRowsAffectedErr: "+msg)}
}

func NewNoRecordFoundErr(err errors.BaseErr, msg string) errors.BaseErr {
	return &NoRecordFoundErr{errors.Append(err, "RecordNotFoundErr: "+msg)}
}

func NewValidationErr(err errors.BaseErr, msg string) errors.BaseErr {
	return &ValidationErr{errors.Append(err, "ValidationErr: "+msg)}
}

func NewUnauthorizedErr(err errors.BaseErr, msg string) errors.BaseErr {
	return &UnauthorizedErr{errors.Append(err, "UnauthorizedErr: "+msg)}
}

type RecordAlreadyExistsErr struct {
	errors.BaseErr
}

func NewRecordAlreadyExistsErr(err errors.BaseErr, msg string) errors.BaseErr {
	return &RecordAlreadyExistsErr{errors.Append(err, "RecordAlreadyExistsErr: "+msg)}
}
