package graph

import "bitbucket.org/captainfresh/go-common/auth"

func FillValidatedData(data *auth.ValidatedData, user *auth.ValidatedUser, namespace *auth.ValidatedNamespace, account *auth.ValidatedAccount) *auth.ValidatedData {
	if data == nil {
		data = &auth.ValidatedData{}
	}

	if user != nil {
		fillUser(data, user)
	}

	if namespace != nil {
		fillNamespace(data, namespace)
	}

	if account != nil {
		fillAccount(data, account)
	}

	return data
}

func fillUser(data *auth.ValidatedData, user *auth.ValidatedUser) {
	if data.User == nil {
		data.User = &auth.ValidatedUser{}
	}

	if user.ID != "" {
		data.User.ID = user.ID
	}

	if user.Email != "" {
		data.User.Email = user.Email
	}

	if user.Name != "" {
		data.User.Name = user.Name
	}

	if user.Picture != "" {
		data.User.Picture = user.Picture
	}
}

func fillNamespace(data *auth.ValidatedData, namespace *auth.ValidatedNamespace) {
	if data.Namespace == nil {
		data.Namespace = &auth.ValidatedNamespace{}
	}

	if namespace.ID != 0 {
		data.Namespace.ID = namespace.ID
	}

	if namespace.Name != "" {
		data.Namespace.Name = namespace.Name
	}
	if namespace.Code != "" {
		data.Namespace.Code = namespace.Code
	}
}

func fillAccount(data *auth.ValidatedData, account *auth.ValidatedAccount) {
	if data.Account == nil {
		data.Account = &auth.ValidatedAccount{}
	}

	if account.ID != 0 {
		data.Account.ID = account.ID
	}

	if account.Name != "" {
		data.Account.Name = account.Name
	}

	data.Account.Metadata = account.Metadata
}
