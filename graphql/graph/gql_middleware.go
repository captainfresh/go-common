package graph

import (
	"bitbucket.org/captainfresh/go-common/auth"
	"context"
	"github.com/99designs/gqlgen/graphql"
)

const (
	CtxValidatedData = "validated-data"
)

type Middleware struct {
	auth auth.RequestAuthorizer
}

func NewGqlMiddleware(apiURL string) *Middleware {
	return &Middleware{auth: auth.NewRequestAuthorizer(apiURL)}
}

func (m *Middleware) VerifyNamespaceUser(ctx context.Context) {
	if resp, err := m.auth.ValidateNamespaceUser(ctx); err != nil {
		graphql.AddError(ctx, err)
	} else {
		m.UpdateValidatedData(ctx, resp)
	}

}

func (m *Middleware) VerifyAccountUser(ctx context.Context) {
	if resp, err := m.auth.ValidateAccountUser(ctx); err != nil {
		graphql.AddError(ctx, err)
	} else {
		m.UpdateValidatedData(ctx, resp)
	}
}

func (m *Middleware) VerifyNamespaceAndUser(ctx context.Context) {
	if resp, err := m.auth.ValidateNamespaceAndUser(ctx); err != nil {
		graphql.AddError(ctx, err)
	} else {
		m.UpdateValidatedData(ctx, resp)
	}

}

func (m *Middleware) UpdateValidatedData(ctx context.Context, data *auth.ValidatedData) {
	validatedData, _ := ctx.Value(CtxValidatedData).(*auth.ValidatedData)
	validatedData = FillValidatedData(validatedData, data.User, data.Namespace, data.Account)
	ctx = context.WithValue(ctx, CtxValidatedData, validatedData)
}
