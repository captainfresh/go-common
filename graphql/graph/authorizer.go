package graph

import (
	"context"
	"crypto/rsa"
	"github.com/99designs/gqlgen/graphql"
	"github.com/lestrrat-go/jwx/jwk"
	"github.com/lestrrat-go/jwx/jwt"
	"github.com/spf13/cast"
	"github.com/vektah/gqlparser/v2/gqlerror"
	"strings"
)

type TokenVerifierMiddleware struct {
	auth *VerifierClient
}

func NewTokenVerifierMiddleware(apiURL string) *TokenVerifierMiddleware {
	return &TokenVerifierMiddleware{auth: NewVerifierClient(apiURL)}
}

type VerifierClient struct {
	publicKey    *rsa.PublicKey
	keySetURL    string
	parseOptions []jwt.ParseOption
	ar           *jwk.AutoRefresh
}

func NewVerifierClient(keySetURL string) *VerifierClient {
	ar := jwk.NewAutoRefresh(context.Background())
	ar.Configure(keySetURL, jwk.WithMinRefreshInterval(43200000000000))

	verifier := &VerifierClient{
		publicKey: nil,
		keySetURL: keySetURL,
		ar:        ar,
	}

	verifier.parseOptions = []jwt.ParseOption{
		jwt.WithValidate(true),
		jwt.WithKeySetProvider(jwt.KeySetProviderFunc(func(jwt.Token) (jwk.Set, error) {
			return verifier.ar.Fetch(context.Background(), verifier.keySetURL)
		})),
	}

	return verifier
}

func (m *TokenVerifierMiddleware) Verify(ctx context.Context) bool {
	authToken := ctx.Value("Authorization")
	authTokenString := cast.ToString(authToken)
	tokenParts := strings.Split(authTokenString, " ")
	// tokenParts must contain a prefix such as Jwt/Bearer and then signed string
	if len(tokenParts) < 2 {
		//_, _ = ctx.Problem(iris.NewProblem().Status(iris.StatusUnauthorized).Detail("invalid token"))
		return false
	}

	_, err := m.auth.Verify(ctx, tokenParts[1])
	if err != nil {
		return false
	}

	return true
}

func (auth *VerifierClient) Verify(ctx context.Context, signedString string) (map[string]interface{}, error) {
	token, err := jwt.Parse([]byte(signedString), auth.parseOptions...)
	if err != nil {
		graphql.AddError(ctx, &gqlerror.Error{
			Path:    graphql.GetPath(ctx),
			Message: "User is unauthorized",
			Extensions: map[string]interface{}{
				"code":             "401",
				"possible reasons": "invalid auth token or other credentials",
			},
		})
		return nil, err
	}
	return token.PrivateClaims(), nil
}
