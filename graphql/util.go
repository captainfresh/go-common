package graphql

import (
	"bitbucket.org/captainfresh/go-common/config"
	"bitbucket.org/captainfresh/go-common/graphql/constant"
	"bitbucket.org/captainfresh/go-common/graphql/graph"
	"context"
	"github.com/99designs/gqlgen/graphql"
	"github.com/vektah/gqlparser/v2/gqlerror"
	"net/http"
	"strings"
)

func AddContext(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		for k, v := range r.Header {
			ctx = context.WithValue(ctx, k, v[0])
		}
		ctx = context.WithValue(ctx, constant.CtxNamespace, ctx.Value(constant.CtxNamespaceInput))
		ctx = context.WithValue(ctx, constant.CtxNamespaceId, ctx.Value(constant.CtxNamespaceInput))
		ctx = context.WithValue(ctx, constant.CtxUserId, ctx.Value(constant.CtxUserIdInput))
		ctx = context.WithValue(ctx, constant.CtxAccount, ctx.Value(constant.CtxAccountInput))
		AuthUrl := config.ViperConfig.GetString("authentication.auth0_key_set_url")

		if !strings.HasPrefix(r.URL.Path, config.ViperConfig.GetString("graphql.schema_endpoint")) {
			tokenVerifierMiddleware := graph.NewTokenVerifierMiddleware(AuthUrl)
			if tokenVerifierMiddleware.Verify(ctx) {
				next.ServeHTTP(w, r.WithContext(ctx))
			} else {
				graphql.AddError(ctx, &gqlerror.Error{
					Message: "Unauthorized",
					Extensions: map[string]interface{}{
						"Error Code": 401,
					},
				})
				w.WriteHeader(http.StatusUnauthorized)
			}
		} else {
			next.ServeHTTP(w, r.WithContext(ctx))
		}
	})
}
