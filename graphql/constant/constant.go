package constant

const (
	CtxNamespace        = "saas-namespace"
	CtxUserId           = "saas-user-id"
	CtxAccount          = "saas-account-id"
	CtxNamespaceId      = "namespaceId"
	CtxNamespaceInput   = "Saas-Namespace"
	CtxUserIdInput      = "Saas-User-Id"
	CtxAccountInput     = "Saas-Account-Id"
	CtxNamespaceIdInput = "NamespaceId"
)
