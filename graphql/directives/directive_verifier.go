package directives

import (
	"bitbucket.org/captainfresh/go-common/auth"
	"bitbucket.org/captainfresh/go-common/config"
	"bitbucket.org/captainfresh/go-common/graphql/model"
	"context"
	"fmt"
	"github.com/99designs/gqlgen/graphql"
)

func VerifyDirectives() func(ctx context.Context, obj interface{}, next graphql.Resolver, params model.Param) (res interface{}, err error) {
	authorizer := auth.NewRequestAuthorizer(config.ViperConfig.GetString("services.ams"))
	return func(ctx context.Context, obj interface{}, next graphql.Resolver, params model.Param) (res interface{}, err error) {
		switch params {
		case model.ParamNamespace:
			_, err := authorizer.ValidateNamespaceUser(ctx)
			if err != nil {
				return nil, fmt.Errorf("namespace validation failed")
			}
		case model.ParamAccountuser:
			_, err := authorizer.ValidateAccountUser(ctx)
			if err != nil {
				return nil, fmt.Errorf("namespace validation failed")
			}
		case model.ParamNamespaceuser:
			_, err := authorizer.ValidateNamespaceAndUser(ctx)
			if err != nil {
				return nil, fmt.Errorf("namespace and user validation failed")
			}
		}
		return next(ctx)
	}
}
