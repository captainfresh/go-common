package model

import (
	"fmt"
	"io"
	"strconv"
)

type Param string

const (
	ParamNamespace        Param = "NAMESPACE"
	ParamNamespaceuser    Param = "NAMESPACEUSER"
	ParamAccountuser      Param = "ACCOUNTUSER"
	ParamOrganisationuser Param = "ORGANISATIONUSER"
)

var AllParam = []Param{
	ParamNamespace,
	ParamNamespaceuser,
	ParamAccountuser,
	ParamOrganisationuser,
}

func (e Param) IsValid() bool {
	switch e {
	case ParamNamespace, ParamNamespaceuser, ParamAccountuser, ParamOrganisationuser:
		return true
	}
	return false
}

func (e Param) String() string {
	return string(e)
}

func (e *Param) UnmarshalGQL(v interface{}) error {
	str, ok := v.(string)
	if !ok {
		return fmt.Errorf("enums must be strings")
	}

	*e = Param(str)
	if !e.IsValid() {
		return fmt.Errorf("%s is not a valid Param", str)
	}
	return nil
}

func (e Param) MarshalGQL(w io.Writer) {
	fmt.Fprint(w, strconv.Quote(e.String()))
}
